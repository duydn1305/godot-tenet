extends GdUnitTestSuite

## Asserts: https://mikeschulze.github.io/gdUnit4/asserts/index/

func before() -> void:
	print("Before any test is run")

func after() -> void:
	print("After any test was run")

func before_test() -> void:
	print("Before a test")

func after_test() -> void:
	print("After a test\n")

func test_fail() -> void:
	print("test_fail")
	assert_bool(true).is_false()
	assert_array([""])\
		.override_failure_message("Failed assert does not stop testcase so this assert also run")\
		.is_empty()

func test_use_node() -> void:
	print("test_use_node")
	# This one use the singleton node Helper and test it's node name
	assert_str(str(Helper.name))\
		.is_equal("Helper")

## Run this test atmost 100 times to find a failure, will stop iter if found any.
## Specify a seed to make sure the same result when re-run
## https://mikeschulze.github.io/gdUnit4/advanced_testing/fuzzing/
func test_fuzzer(fuzzer := Fuzzers.rangei(1, 2), fuzzer_iterations := 100, fuzzer_seed := 2023) -> void:
	var n: int = fuzzer.next_value()
	print("test_fuzzer, n = %d" % n)
	assert_int(n%2).is_even()

## Specify a set of testcases for this test. Testcase must match the input args
## https://mikeschulze.github.io/gdUnit4/advanced_testing/paramerized_tests/
func test_parameterized_int_values(a: int, b: int, c: int, expected: int,
	test_parameters := [
		[1, 2, 3, 6],
		[3, 4, 5, 12],
		[6, 7, 8, 22]
	]) -> void:
	print("test_parameterized_int_values")
	assert_int(a+b+c).is_equal(expected)
