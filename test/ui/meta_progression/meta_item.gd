extends GdUnitTestSuite

var data_node: Node
#var store_progression: SaveMetaProgression
func before_test() -> void:
#	store_progression = (Store.meta_progression as SaveMetaProgression).duplicate(true)
	pass


func after_test() -> void:
#	Store.meta_progression = store_progression
#	Store.meta_progression.save()
	pass


func test_selected_meta_progression_item(item_key: String, res_name: String, test_parameters = [
	["meta_1", "1"],
	["meta_2", "2"],
	["meta_3", "3"],
	["meta_4", "4"]
	]) -> void:
	var runner := scene_runner("res://scenes/ui/meta_progression/meta_progression.tscn")
	data_node = runner.find_child("Data")
	
	await await_millis(500)
	var meta_items := data_node.get_children().filter(func(item): return item.current_data.key == item_key)
	
	assert_int(meta_items.size())\
		.override_failure_message("Array item size is not 1")\
		.is_equal(1)
	var meta_item := meta_items[0] as MetaProgresssionItemRow
	var item_data := load("res://resources/meta_progression/data/%s.tres" % res_name) as MetaProgResource
	var button := meta_item.find_child("Button") as Button
	button.pressed.emit()
	
	await await_idle_frame()
	
	var meta_detail := runner.find_child("MetaProgressionItemDetail") as MetaProgressionItemDetail
	var name_node := meta_detail.get_node("%Name") as RichTextLabel
	
	assert_str(name_node.text).is_equal(meta_detail.default_format.format([item_data.name]))


func test_meta_progression_item_increased(item_key: String, res_name: String, current_level: int, coin: int, test_parameters = [
	["meta_1", "1", 5, 1000],
	["meta_1", "1", 5, 0],
	["meta_2", "2", 0, 5],
	["meta_3", "3", 0, 10],
	["meta_3", "3", 3, 200],
	["meta_5", "5", 0, 50],
	]) -> void:
		
	Store.meta_progression.set_value(item_key, current_level)
	Store.general.coin = coin
	
	var runner := scene_runner("res://scenes/ui/meta_progression/meta_progression.tscn")
	data_node = runner.find_child("Data")
	
	await await_millis(500)
	var meta_items := data_node.get_children().filter(func(item): return item.current_data.key == item_key)
	
	assert_int(meta_items.size())\
		.override_failure_message("Array item size is not 1")\
		.is_equal(1)
	var meta_item := meta_items[0] as MetaProgresssionItemRow
	
	var plus := meta_item.find_child("Plus") as Button
	plus.pressed.emit()
	
	await await_idle_frame()
	var item_data := load("res://resources/meta_progression/data/%s.tres" % res_name) as MetaProgResource
	var max_level := item_data.description.level_texts.size()
	var expected_level : int
	var expected_coin : int
	
	if current_level == max_level or coin < item_data.require.coin[current_level]:
		expected_level = current_level
		expected_coin = coin
	else:
		expected_level = current_level + 1
		expected_coin = coin - item_data.require.coin[current_level]
			
	assert_int(Store.meta_progression.get_value(item_key)).is_equal(expected_level)
	assert_str(meta_item.get_node("%Level").text).is_equal("%d / %d" % [expected_level, max_level])

func test_meta_progression_item_decreased(item_key: String, res_name: String, current_level: int, coin: int, test_parameters = [
	["meta_1", "1", 5, 1000],
	["meta_2", "2", 0, 5],
	["meta_3", "3", 1, 10],
	["meta_5", "5", 3, 0],
	]) -> void:
	Store.meta_progression.set_value(item_key, current_level)
	Store.general.coin = coin
	
	var runner := scene_runner("res://scenes/ui/meta_progression/meta_progression.tscn")
	data_node = runner.find_child("Data")
	await await_millis(500)
	var meta_items := data_node.get_children().filter(func(item): return item.current_data.key == item_key)
	
	assert_int(meta_items.size())\
		.override_failure_message("Array item size is not 1")\
		.is_equal(1)
	var meta_item := meta_items[0] as MetaProgresssionItemRow
	
	var minus := meta_item.find_child("Minus") as Button
	minus.pressed.emit()
	
	await await_idle_frame()
	var item_data := load("res://resources/meta_progression/data/%s.tres" % res_name) as MetaProgResource
	var max_level := item_data.description.level_texts.size()
	var expected_level := max(0, current_level - 1) as int
	var expected_coin := coin + item_data.require.coin[current_level - 1] if current_level > 0 else 0
	
		
	assert_int(Store.meta_progression.get_value(item_key)).is_equal(expected_level)
	assert_str(meta_item.get_node("%Level").text).is_equal("%d / %d" % [expected_level, max_level])
