extends GdUnitTestSuite

var sound_node: Node

func before_test() -> void:
	var runner := scene_runner("res://scenes/ui/settings/settings.tscn")
	sound_node = runner.find_child("Sound")
	(sound_node.get_parent() as TabContainer).current_tab = 1

func test_change_volume_master(fuzzer_volume := Fuzzers.rangei(0, 100), fuzzer_iterations := 50,
		fuzzer_seed := 1) -> void:
	var label := sound_node.get_node("%VMasterLabel") as Label
	var slider := sound_node.get_node("%MasterVolume") as HSlider
	
	var value: int = fuzzer_volume.next_value()
	var bus_idx := GameSettings.sound.master_idx

	slider.value = -value
	slider.value = value
	await await_idle_frame()

	assert_str(label.text).is_equal("%d" % value)
	assert_float(AudioServer.get_bus_volume_db(bus_idx)).is_equal_approx(linear_to_db(value*0.01), 0.0001)

func test_change_volume_music(fuzzer_volume := Fuzzers.rangei(0, 100), fuzzer_iterations := 50,
		fuzzer_seed := 2) -> void:
	var label := sound_node.get_node("%VMusicLabel") as Label
	var slider := sound_node.get_node("%MusicVolume") as HSlider
	
	var value: int = fuzzer_volume.next_value()
	var bus_idx := GameSettings.sound.music_idx

	slider.value = -value
	slider.value = value
	await await_idle_frame()

	assert_str(label.text).is_equal("%d" % value)
	assert_float(AudioServer.get_bus_volume_db(bus_idx)).is_equal_approx(linear_to_db(value*0.01), 0.0001)

func test_change_volume_sound(fuzzer_volume := Fuzzers.rangei(0, 100), fuzzer_iterations := 50,
		fuzzer_seed := 3) -> void:
	var label := sound_node.get_node("%VSoundLabel") as Label
	var slider := sound_node.get_node("%SoundVolume") as HSlider

	var value: int = fuzzer_volume.next_value()
	var bus_idx := GameSettings.sound.sound_idx

	slider.value = -value
	slider.value = value
	await await_idle_frame()

	assert_str(label.text).is_equal("%d" % value)
	assert_float(AudioServer.get_bus_volume_db(bus_idx)).is_equal_approx(linear_to_db(value*0.01), 0.0001)
