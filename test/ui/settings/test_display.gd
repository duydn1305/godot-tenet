extends GdUnitTestSuite

var display_node: Node

func before_test() -> void:
	var runner := scene_runner("res://scenes/ui/settings/settings.tscn")
	display_node = runner.find_child("Display")

func test_change_display_mode(mode_id: DisplayServer.WindowMode, expected_text: String, test_parameters = [
	[DisplayServer.WINDOW_MODE_EXCLUSIVE_FULLSCREEN, "Fullscreen"],
	[DisplayServer.WINDOW_MODE_FULLSCREEN, "Fullscreen (Borderless)"],
	[DisplayServer.WINDOW_MODE_WINDOWED, "Windowed"]
	]) -> void:
	const map := {
		DisplayServer.WINDOW_MODE_EXCLUSIVE_FULLSCREEN: 0,
		DisplayServer.WINDOW_MODE_FULLSCREEN: 1,
		DisplayServer.WINDOW_MODE_WINDOWED: 2
	}

	var options := display_node.get_node("%DisplayMode") as OptionButton

	options.select(map[mode_id])
	options.item_selected.emit(map[mode_id])
	await await_idle_frame()

	assert_str(options.get_item_text(options.selected)).is_equal(expected_text)
	assert_int(DisplayServer.window_get_mode()).is_equal(mode_id)

func test_change_fps_limit(option_id: int, expected_text: String, test_parameters = [
	[0, "45"],
	[1, "60"],
	[2, "90"],
	[3, "144"],
	[4, "Unlimited"]
	]) -> void:
	var options := display_node.get_node("%FPSLimit") as OptionButton

	options.select(option_id)
	options.item_selected.emit(option_id)
	await await_idle_frame()
	
	assert_str(options.get_item_text(options.selected)).is_equal(expected_text)
	assert_int(Engine.max_fps).is_equal(
		0 if expected_text == "Unlimited" else
		expected_text.to_int())

func test_change_vsync(state: bool, test_parameters = [[false], [true]]) -> void:
	const map := {
		false: [DisplayServer.VSYNC_DISABLED],
		true: [DisplayServer.VSYNC_ENABLED, DisplayServer.VSYNC_ADAPTIVE]
	}

	var check_btn := display_node.get_node("%VSync") as CheckButton
	
	check_btn.button_pressed = state
	await await_idle_frame()

	assert_array(map[state]).contains([DisplayServer.window_get_vsync_mode()])

func test_change_color_brightness(value: float, test_parameters = [[0.4], [1.0], [6.0], [2.8]]) -> void:
	var label := display_node.get_node("%BrightnessLabel") as Label
	var slider := display_node.get_node("%Brightness") as HSlider

	slider.value = -value
	slider.value = value
	await await_idle_frame()

	assert_str(label.text).is_equal("%.1f" % value)
	assert_float(GameSettings.environment.adjustment_brightness).is_equal_approx(value, 0.0001)

func test_change_color_contrast(value: float, test_parameters = [[0.2], [1.0], [6.0], [4.4]]) -> void:
	var label := display_node.get_node("%ContrastLabel") as Label
	var slider := display_node.get_node("%Contrast") as HSlider

	slider.value = -value
	slider.value = value
	await await_idle_frame()

	assert_str(label.text).is_equal("%.1f" % value)
	assert_float(GameSettings.environment.adjustment_contrast).is_equal_approx(value, 0.0001)

func test_change_color_saturation(value: float, test_parameters = [[0.6], [1.0], [6.0], [5.8]]) -> void:
	var label := display_node.get_node("%SaturationLabel") as Label
	var slider := display_node.get_node("%Saturation") as HSlider

	slider.value = -value
	slider.value = value
	await await_idle_frame()

	assert_str(label.text).is_equal("%.1f" % value)
	assert_float(GameSettings.environment.adjustment_saturation).is_equal_approx(value, 0.0001)
