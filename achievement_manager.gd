extends MarginContainer
var item_screen = preload("res://scenes/ui/achievement/achie_item.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	var async_loader := Helper.load_resources("res://resources/achievements/data/", self) as FolderAsyncLoader
	var data := await async_loader.finished as Array[Resource]
	for item in data:
		item = item as Achievement
		var screen = item_screen.instantiate()
		screen.set_title(item.title)
		screen.set_decs(item.description)
		screen.set_progress(Store.achievements.get_value(item.key), item.total_require)
		%AchieItems.add_child.call_deferred(screen)
