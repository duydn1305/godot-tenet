extends CharacterBody2D

@onready var visuals = $Visuals
@onready var velocity_component : VelocityComponent = $VelocityComponent
var basic_data : BasicStats
var strategy : VelocityComponent.PlayerRankType = VelocityComponent.PlayerRankType.Nearest

func _physics_process(delta):
	if !multiplayer.is_server():
		return
	velocity_component.find_player_by_rank_type(strategy)
	velocity_component.accelebrate_to_player()
	velocity_component.move(self)
	
	if $AnimationPlayer.current_animation in ["attack"]:
		return
	
	#animation: walk/ idle
	if velocity_component.velocity != Vector2.ZERO:
		$AnimationPlayer.play("walk")
	else:
		$AnimationPlayer.play("idle")
	
	var move_sign = sign(velocity.x)
	if move_sign != 0:
		visuals.scale = Vector2(move_sign, 1)
		
	
func change_strategy(new_value : VelocityComponent.PlayerRankType):
	strategy = new_value
