extends Node2D
class_name ShootEnemyAttack

@onready var attack_interval_timer = $AttackIntervalTimer
@export var anim_player : AnimationPlayer
@export var hitbox_component : BaseHitbox
@export var velocity_component : VelocityComponent
@export var shootable_component : ShootableComponent

var damage = 2.3 :
	set(new_value):
		damage = new_value
		if hitbox_component != null:
			hitbox_component.damage = damage
			
var attack_reload = 3.0
var player_count = 0
var attack_speed = 1 :
	set(new_value):
		attack_speed = new_value
		attack_reload = 1/attack_speed

func _ready():
	if !multiplayer.is_server():
		return
	$RangeToAttack.connect("area_entered", _player_enter_atk_area_entered)
	$RangeToAttack.connect("area_exited", _player_enter_atk_area_exited)
	$AttackIntervalTimer.connect("timeout", _handle_base_attack)
	if owner.basic_data != null:
		attack_speed = (owner.basic_data as BasicStats).ATTACK_SPEED
		damage = (owner.basic_data as BasicStats).DMG


# there is some player enter then start timer and perform if timer previously stopped
func _player_enter_atk_area_entered(area):
	player_count += 1
	if attack_interval_timer.is_stopped():
		# emit to perform atk
		attack_interval_timer.emit_signal("timeout")
		# reset wait time and old remaining time
		attack_interval_timer.start(attack_reload)

# there is no player in attack range 
func _player_enter_atk_area_exited(area):
	player_count -= 1
	
# when timeout, perform attack if there is player in range
# if no one in ranged, stop timer
func _handle_base_attack():
	if player_count <= 0:
		attack_interval_timer.stop()
		return
	begin_attack()
	anim_player.play("attack")
	await anim_player.animation_finished
	perform_attack()
	end_attack()
	
# call by animation
func perform_attack():
	shootable_component.damage = damage
	shootable_component.shoot_projectiles(hitbox_component.global_position, velocity_component.player_node.global_position - hitbox_component.global_position, 1, deg_to_rad(90), 0.05)

# virtual function
func begin_attack():
	pass
	
# virtual function
func end_attack():
	pass
