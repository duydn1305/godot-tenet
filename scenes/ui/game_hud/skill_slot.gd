extends MarginContainer
class_name UISkillSlot

@export var skill_id : int
@export var skill_type : Const.ActiveSkill = Const.ActiveSkill.Q
@onready var cooldown_bar = $Panel/CooldownBar
@onready var counter = $Panel/Border/Counter
@onready var skill_cell_init_size = cooldown_bar.size
#
#	if time_remain <= 0:
#		skill_node.get_node("Counter").text = ""
#		skill_node.get_node("CooldownBar").visible = false
#		return
#	skill_node.get_node("CooldownBar").visible = true	
#	skill_node.get_node("Counter").text = str(time_remain)
#	skill_node.get_node("CooldownBar").size = Vector2(skill_cell_init_size.x, skill_cell_init_size.y*time_remain/total_time)

func update_label(t : Const.ActiveSkill):
	counter.text = ""
	match t:
		Const.ActiveSkill.Q:
			$Panel/Border/Label.text = "Q"
		Const.ActiveSkill.E:
			$Panel/Border/Label.text = "E"
		Const.ActiveSkill.R:
			$Panel/Border/Label.text = "R"
		Const.ActiveSkill.Space:
			$Panel/Border/Label.text = "S"

func _ready():
	update_label(skill_type)


func update_skill_cooldown(total_time : float, time_remain : float):
	if time_remain <= 0:
		counter.text = ""
		cooldown_bar.visible = false
		return
	cooldown_bar.visible = true
	counter.text = str(time_remain)
	cooldown_bar.size = Vector2(skill_cell_init_size.x, skill_cell_init_size.y*time_remain/total_time)


# todo
func update_skill_image(icon : Texture2D):
	$Panel/SkillImageContainer/SkillImage.texture = icon
