extends CanvasLayer

@onready var container := $ImageContainer

var current_page := 0

func _ready():
	$AnimationPlayer.play("wasd")

func _process(delta):
	if Input.is_key_pressed(KEY_ESCAPE):
		SceneTransition.start_transition()
		await SceneTransition.transition_halfway
		get_tree().change_scene_to_file("res://scenes/ui/main_menu/main_menu_2.tscn")

func _on_previous_pressed():
	var size := container.get_child_count()
	current_page = (current_page - 1) % size
	if (current_page < 0): current_page += size
	set_visible_page()

func _on_next_pressed():
	var size := container.get_child_count()
	current_page = (current_page + 1) % size
	if (current_page < 0): current_page += size
	set_visible_page()

func set_visible_page():
	for child in container.get_children():
		(child as Control).visible = false
	(container.get_node("%d" % current_page) as Control).visible = true

func _on_back_pressed():
	SceneTransition.start_transition()
	await SceneTransition.transition_halfway
	get_tree().change_scene_to_file("res://scenes/ui/main_menu/main_menu_2.tscn")
