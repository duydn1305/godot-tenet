extends CanvasLayer

@rpc("any_peer", "call_local")
func flash():
	$AnimationPlayer.play("red_flash")
