extends ColorRect

@export var item_scene: PackedScene

func _ready():
	%Loader.visible = true
	var async_loader := Helper.load_resources("res://resources/meta_progression/data/", self) as FolderAsyncLoader
	var data := await async_loader.finished as Array[Resource]
	update_coin(null)
	
	var data_view := %Data
	data.sort_custom(sort_ascending)
	for item in data:
		item = item as MetaProgResource
		var row := item_scene.instantiate() as MetaProgresssionItemRow
		row.changed.connect(update_coin)
		row.pressed.connect(_on_meta_item_pressed)
		data_view.add_child.call_deferred(row)
		row.set_data(item)
	data_view.visible = true
	%Loader.visible = false
	

func update_coin(current_data: MetaProgResource) -> void:
	%Coin.text = str(Store.general.coin)
	if current_data != null:
		set_metaprogression_detail(current_data)


func sort_ascending(a, b):
	return int(a.key.get_slice("_", 1)) < int(b.key.get_slice("_", 1))
	

func _on_meta_item_pressed(current_data: MetaProgResource) -> void:
	set_metaprogression_detail(current_data)
	


func set_metaprogression_detail(data: MetaProgResource)-> void:
	%SelectLabel.visible = false
	var meta_item_detail := $HBoxContainer/PanelContainer2/MetaProgressionItemDetail 
	meta_item_detail.visible = true
	meta_item_detail.set_data(data)
