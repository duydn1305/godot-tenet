class_name MetaProgressionItemDetail
extends PanelContainer

var current_data: MetaProgResource
var default_format: String 

func _ready() -> void:
	default_format = %Name.text
	
	
func set_data(data: MetaProgResource) -> void:
	current_data = data
	_update_data()
	
	
func _update_data() -> void:
	var key := current_data.key
	var name := %Name as RichTextLabel
	var current_level := %CurrentLevel as Label
	var next_level := %NextLevel as Label
	var current_description := %CurrentLevelDescription as RichTextLabel
	var next_description := %NextLevelDescription as RichTextLabel
	var next_coin := %NextLevelCoin as Label
	var save_mp := Store.meta_progression as SaveMetaProgression
	
#	set current level data of selected meta item for view
	name.text = default_format.format([current_data.name])
	$VBoxContainer2/HBoxContainer2/PanelContainer/Icon.texture = current_data.icon
	
	var index := save_mp.get_value(key);
	current_level.text = "%d" % index
	
	if index >= 5:
#		current level
		current_description.text = current_data.description.base_text.format(current_data.description.level_texts[index-1].array)
#		next level
		next_coin.text = "MAX"
		next_description.text = current_data.description.base_text.format(current_data.description.level_texts[index-1].array)
		next_level.text = "MAX"
	else:
		if index == 0:
			current_description.text = current_data.description.base_text.format(["0", "0", "0"])
		else:
	#		current level
			current_description.text = current_data.description.base_text.format(current_data.description.level_texts[index-1].array)
#		next level
		next_coin.text = str(current_data.require.coin[index])
		next_description.text = current_data.description.base_text.format(current_data.description.level_texts[index].array)
		next_level.text = "%d" % (index+1)
