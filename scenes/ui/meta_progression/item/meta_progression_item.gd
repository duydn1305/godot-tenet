class_name MetaProgresssionItemRow
extends PanelContainer

signal changed(data: MetaProgResource)
signal pressed(data: MetaProgResource)
var current_data: MetaProgResource

func set_data(data: MetaProgResource) -> void:
	current_data = data
	_update_data()

func _update_data() -> void:
	var icon := %Icon as TextureRect
	var level := %Level as Label
	var name := %Name as Label
	var description := %Description as RichTextLabel
	var coin := %Coin as Label
	var key := current_data.key
	var save_mp := Store.meta_progression as SaveMetaProgression
	
	icon.texture = current_data.icon
	level.text = "%d / %d" % [save_mp.get_value(key), current_data.description.level_texts.size()]
	name.text = current_data.name
	var coin_key_index := save_mp.get_value(key);
	if (coin_key_index == 5):
		coin.text = "MAX"
		description.text = current_data.description.base_text.format(current_data.description.level_texts[save_mp.get_value(key)-1].array)
	else:
		coin.text = str(current_data.require.coin[coin_key_index])
		if coin_key_index == 0:
			description.text = current_data.description.base_text.format(["0", "0", "0"])
		else:
			description.text = current_data.description.base_text.format(current_data.description.level_texts[save_mp.get_value(key)-1].array)
		

func _on_button_pressed() -> void:
	pressed.emit(current_data)


func _on_plus_pressed():
	var level := Store.meta_progression.get_value(current_data.key)
	
	#check max level
	if (level == current_data.description.level_texts.size()):
		Helper.toasts.add_toast("You have reached the highest level", 2)
		return
		
	if (Store.general.coin - current_data.require.coin[level] >= 0):
		Store.general.coin -= current_data.require.coin[level]
		Store.general.save()
		Store.meta_progression.set_value(current_data.key, level+1)
		Store.meta_progression.save()
		_update_data()
		changed.emit(current_data)
	else:
		Helper.toasts.add_toast("You don't have enough coin to upgrade", 2)
		return


func _on_minus_pressed():
	var level := Store.meta_progression.get_value(current_data.key)
	
	#check max level
	if (level == 0):
		Helper.toasts.add_toast("Current level is minimum", 2)
		return
		
	Store.general.coin += current_data.require.coin[level-1]
	Store.general.save()
	Store.meta_progression.set_value(current_data.key, level-1)
	Store.meta_progression.save()
	_update_data()
	changed.emit(current_data)
