class_name BossHealthbarUI
extends CanvasLayer

func _ready():
	if !multiplayer.is_server():
		return
	GameEvents.boss_spawned.connect(handle_boss_spawned)
	GameEvents.boss_died.connect(handle_boss_died)
	GameEvents.boss_hp_changed.connect(handle_boss_hp_change)

func handle_boss_spawned(boss_id, boss_name):
	show_boss_hp_bar.rpc(true)
	set_boss_name.rpc(boss_name)
func handle_boss_hp_change(current_hp, amount, ratio):
	set_health_bar.rpc(ratio)

func handle_boss_died(boss_id):
	show_boss_hp_bar.rpc(false)	
	
@rpc("any_peer","call_local")
func show_boss_hp_bar(value : bool):
	visible = value
	set_health_bar(1)
	if value == true: 	BgMusic.play_type(BgMusic.Type.BOSS)
	else: BgMusic.play_type(BgMusic.Type.COMBAT)

@rpc("any_peer","call_local")
func set_health_bar(percent: float) -> void:
	var health_bar = %BossHealthBar as TextureProgressBar
	health_bar.value = percent
	
	var boss_icon = %BossIcon as TextureRect
	if percent > .70:
		boss_icon.texture = preload("res://assets/ui/boss/boss_icon_1.png")
	elif percent > .50: 
		boss_icon.texture = preload("res://assets/ui/boss/boss_icon_2.png")
	elif percent > .20:
		boss_icon.texture = preload("res://assets/ui/boss/boss_icon_3.png")
	else:
		boss_icon.texture = preload("res://assets/ui/boss/boss_icon_4.png")
			
	
@rpc("any_peer","call_local")
func set_boss_name(name: String) -> void:
	var boss_name = %BossName as Label
	boss_name.text = name
