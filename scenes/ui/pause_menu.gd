extends CanvasLayer

@onready var pause_menu_board = $MarginContainer/MenuBackground/Data
var pause_menu_open = false
@onready var stats_manager = $"../../StatsManager" as StatsManager

#var stats_board = preload("res://scenes/ui/player_stats.tscn")
#var abilities_board = preload("res://scenes/ui/abilities.tscn")
@onready var stats_board = $MarginContainer/MenuBackground/Data/Stats
@onready var list_abilities_board = $MarginContainer/MenuBackground/Data/ListAbilities
@onready var settings = $MarginContainer/MenuBackground/Settings

#var player_id : int

func _ready():
#	player_id = multiplayer.get_unique_id()
	%StatsBtn.pressed.connect(_on_stats_pressed)
	%Skils.pressed.connect(_on_skills_pressed)
	%Options.pressed.connect(_on_options_pressed)
	%Quit.pressed.connect(_on_quit_pressed)
	GameEvents.end_game.connect(handle_end_game)

func handle_end_game(s, msg):
	set_process_unhandled_input(false)

func _unhandled_input(event):
	if !$TimeDelay.is_stopped():
		return
	if event.is_action_pressed("escape"):
		get_tree().root.set_input_as_handled()
		request_show_pause_menu.rpc_id(1)

@rpc("any_peer", "call_local")
func request_show_pause_menu():
	show_pause_menu.rpc()

@rpc("any_peer", "call_local")
func show_pause_menu():
	$TimeDelay.start(1)
	visible = !pause_menu_open
	get_tree().paused = !pause_menu_open
	pause_menu_open = !pause_menu_open
	if visible:
		hide_all_board()
		_on_stats_pressed()


func _process(delta):
	pass

# when button quest pressed, show quest board
func _on_skills_pressed():
	hide_all_board()
#	var newtab = abilities_board.instantiate()
	list_abilities_board.visible = true
	if stats_manager == null:
		return
	if !list_abilities_board.is_data_loaded:
		list_abilities_board.basic_stats = stats_manager.raw_stats
		list_abilities_board.preload_skill_data()
#	pause_menu_board.add_child(newtab)


# when button stats pressed, show stats board
func _on_stats_pressed():
	hide_all_board()
	if stats_manager == null:
		return
	stats_board.stats = stats_manager.my_stats
	stats_board.stats['extra_data'] = stats_manager.my_extra_data
	stats_board.visible = true
#	var m2 = " .hp: " + str(stats_manager.my_stats[Const.Stats.HP])

func _on_options_pressed():
	hide_all_board()	
	settings.visible = true	
	
func _on_quit_pressed():
	var confirm_modal = preload("res://scenes/ui/common/confirm_modal/confirm_modal.tscn").instantiate() as ConfirmModal
	add_child(confirm_modal)
	confirm_modal.set_header_text("Exit dungeon")
	confirm_modal.set_body_text("Current progress won't be saved!")
	confirm_modal.pressed.connect(confirm_quit)

func confirm_quit(ok):
	if !ok:
		return
	
	multiplayer.multiplayer_peer = null
	get_tree().change_scene_to_file("res://scenes/lodging/house/house.tscn")
	
func _exit_tree():
	multiplayer.multiplayer_peer = null

#	GameEvents.emit_player_disconnected(player_id)
#	multiplayer.multiplayer_peer = null
#

# remove current board
func hide_all_board():
	stats_board.visible = false
	list_abilities_board.visible = false
	settings.visible = false
