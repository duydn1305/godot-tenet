extends CanvasLayer

@export var shop_character_item: PackedScene
@onready var shop_item_detail:= %ShopCharacterItemDetail as ShopCharacterItemDetail

func _ready():
	var async_loader = Helper.load_resources("res://resources/characters/0_data/", self) as FolderAsyncLoader
	var data := await async_loader.finished as Array[Resource]
	
	Callable(func() -> void:
		shop_item_detail.unlock_pressed.connect(_on_unlocked_press)
		_set_characters.call_deferred(data)
		update_coin.call_deferred()
	).call_deferred()

func _set_characters(data: Array[Resource]) -> void:
	var item_container := %ItemContainer
	var is_first_render := true
	for item in data:
		if item.id == 1: continue
		item = item as BasicStats
		var shop_item := shop_character_item.instantiate() as ShopCharacterItem
		shop_item.pressed.connect(_on_item_pressed)
		shop_item.name = str(item.id)
		item_container.add_child(shop_item)
		shop_item.set_data(item)
#		if is_first_render: 
#			shop_item.pressed.emit(item)
#			is_first_render = false

func _on_item_pressed(data: BasicStats) -> void:
	shop_item_detail.set_data.call_deferred(data)
	shop_item_detail.visible = true

func _on_unlocked_press(data: BasicStats) -> void:
	var item := %ItemContainer.get_node(str(data.id)) as ShopCharacterItem
	item.set_data(data)
	update_coin.call_deferred()

func update_coin() -> void:
	%Coin.text = str(Store.general.coin)
