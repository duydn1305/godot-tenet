class_name ShopCharacterItemDetail
extends MarginContainer

signal unlock_pressed(data: BasicStats)

var current_data: BasicStats
@onready var unlock := %Unlock as Button
@onready var icon := %Icon as TextureRect
@onready var char_name := %Name as Label
@onready var HP := %HP as Label
@onready var armor := %Armor as Label
@onready var mana := %Mana as Label
@onready var crit_DMG := %CritDMG as Label
@onready var attack_rate := %AttackRate as Label
@onready var crit_rate := %CritRate as Label
@onready var DMG := %DMG as Label
@onready var select_btn := %Select as Button

func set_data(data: BasicStats) -> void:
	current_data = data
	_set_character_state.call_deferred(data)
	

func _set_character_state(data: BasicStats) -> void:
	icon.texture = data.thumbnail
	char_name.text = data.object_name
	HP.text = str(data.HP)
	armor.text = str(data.AMMOR)
	mana.text = str(data.MP)
	crit_DMG.text = str(data.CRIT_DMG)
	attack_rate.text = str(data.ATTACK_SPEED)
	crit_rate.text = str(data.CRIT_RATE)
	DMG.text = str(data.DMG)
	
	var save_shop := Store.shop as SaveShop
	var unlocked := save_shop.characters.get_value(str(current_data.id))
	var last_choose := Store.general.last_character_id
	if unlocked:
		unlock.visible = false
		select_btn.visible = true
		if last_choose == str(current_data.id):
			select_btn.text = "Selected"
			select_btn.disabled = true
		else:
			select_btn.text = "Select"
			select_btn.disabled = false
	else:
		unlock.visible = true
		select_btn.visible = false

func _on_unlock_pressed():
	var player_coin := Store.general.coin as int
	var require_coin := current_data.PRICE as int
	
	if player_coin >= require_coin:
#		minus coin when buy successful
		Store.general.coin -= require_coin
		Store.general.save()
		
		Store.shop.characters.set_value(str(current_data.id), true)
		Store.shop.save()
		unlock_pressed.emit(current_data)
		set_data(current_data)
	else:
		Helper.toasts.add_toast("You don't have enough coin to buy this character", 2)

func _on_select_pressed():
	Store.general.last_character_id = str(current_data.id)
	Store.general.save()
	set_data(current_data)
