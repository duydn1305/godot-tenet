class_name ShopCharacterItem
extends MarginContainer

signal pressed(data: BasicStats)
var current_data: BasicStats

func set_data(data: BasicStats) -> void:
	current_data = data
	var save_shop := Store.shop as SaveShop
	var unlocked := save_shop.characters.get_value(str(data.id))
	print(unlocked)
	%Name.text = data.object_name
	%Icon.texture = data.icon
	var lock := %Lock as HBoxContainer
	var unlocked_ui := %Unlocked as TextureRect
	if unlocked:
		unlocked_ui.visible = true
		lock.visible = false
	else:
		%Coin.text = str(data.PRICE)
		lock.visible = true
		unlocked_ui.visible = false
	


func _on_button_pressed():
	pressed.emit(current_data)
