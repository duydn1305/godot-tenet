extends CanvasLayer

@onready var exit_button = $ExitButton

signal exit_page()
var exiting = false

func _ready():
	exit_button.connect("pressed", exit_meta_upgrade_page)

func _unhandled_input(event):
	if event.is_action_pressed("escape"):
		get_tree().root.set_input_as_handled()
		exit_meta_upgrade_page()



func exit_meta_upgrade_page():
	if exiting:
		return
	exit_button.disabled = true
	exiting = true
	exit_page.emit()
	SceneTransition.start_transition()
	await SceneTransition.transition_halfway
	call_deferred("queue_free")
