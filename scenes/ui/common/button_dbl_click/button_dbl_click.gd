class_name ButtonDblClick
extends Button

signal double_clicked

func _gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		if event.double_click and event.button_index == MOUSE_BUTTON_LEFT:
			double_clicked.emit()
