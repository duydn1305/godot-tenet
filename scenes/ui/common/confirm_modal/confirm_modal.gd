class_name ConfirmModal
extends	ColorRect

signal pressed(ok: bool)

var is_inform := false

@onready var header_label: RichTextLabel = %Header
@onready var body_label: RichTextLabel = %Body

func _ready() -> void:
	%Cancel.visible = !is_inform
	%OK.grab_focus()
	pass

func set_header_text(text: String, center := false) -> void:
	if center:
		text = "[center]%s[/center]" % text
	header_label.text = text

func set_body_text(text: String, center := false) -> void:
	if center:
		text = "[center]%s[/center]" % text
	body_label.text = text

func _on_ok_pressed() -> void:
	pressed.emit(true)
	queue_free()

func _on_cancel_pressed() -> void:
	pressed.emit(false)
	queue_free()
