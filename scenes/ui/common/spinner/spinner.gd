@tool
class_name Spinner
extends ColorRect

@export_range(0.0, 360.0) var hole_angle := 60.0:
	set(value):
		hole_angle = value
		_update_hole()

@export_range(0.5, 3.0) var time := 1.0
@export_range(0.05, 2.0) var delay_time := 0.4

var _wait_time := 0.0;
var _progress := 0.0;

func _ready() -> void:
	_update_hole()
	_update_progress()

func _process(delta: float) -> void:
	if _wait_time > 0.0:
		_wait_time -= delta
		return

	pivot_offset = size * 0.5;
	var offset := delta / time

	_progress += offset * 2.0;
	if _progress >= 2.0:
		_wait_time = delay_time
		_progress = 0.0;
		rotation_degrees = fmod(rotation_degrees + 360.0 - hole_angle, 360.0)
	else:
		rotation += offset * TAU;
		if rotation > TAU: rotation -= TAU
	_update_progress()

func _update_hole() -> void:
	var mat := material as ShaderMaterial
	mat.set_shader_parameter("hole_angle", hole_angle)

func _update_progress() -> void:
	var mat := material as ShaderMaterial
	mat.set_shader_parameter("progress", _progress)
