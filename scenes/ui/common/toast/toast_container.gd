class_name ToastContainer
extends CanvasLayer

@export var toast_scene: PackedScene

@onready var container: Control = $Container

@rpc("any_peer", "call_local")
func add_toast(message: String, time: float) -> void:
	var toast := toast_scene.instantiate() as Toast
	toast.modulate = Color.TRANSPARENT
	container.add_child(toast)
	toast.set_message(message)

	var tween := Helper.new_tween(self) as Tween
	tween.tween_property(toast, "modulate", Color.WHITE, 0.5)
	tween.parallel().tween_property(toast.container, "position", Vector2.ZERO, 0.5).from(Vector2.RIGHT * 100)
	tween.tween_interval(time)
	tween.tween_property(toast, "modulate", Color.TRANSPARENT, 0.5)
	tween.parallel().tween_property(toast.container, "position", Vector2.LEFT * 50, 0.5)
	tween.tween_callback(func() -> void:
		toast.queue_free()
	)
	tween.play()

func clear() -> void:
	Helper.clear_children(container)
