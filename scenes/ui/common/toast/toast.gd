class_name Toast
extends MarginContainer

@onready var container: Control = $Container
@onready var _msg_label := %Message as Label

func set_message(message: String) -> void:
	_msg_label.text = message
