extends TabBar

@onready var display_option: OptionButton = %DisplayMode
@onready var fps_option: OptionButton = %FPSLimit
@onready var vsync_checkbox: CheckButton = %VSync
@onready var brightness_label: Label = %BrightnessLabel
@onready var brightness_slider: HSlider = %Brightness
@onready var contrast_label: Label = %ContrastLabel
@onready var contrast_slider: HSlider = %Contrast
@onready var saturation_label: Label = %SaturationLabel
@onready var saturation_slider: HSlider = %Saturation

func _ready() -> void:
	update()

func update() -> void:
	set_display_mode(GameSettings.display.get_mode())
	set_fps_limit(GameSettings.display.get_fps_limit())
	set_vsync(GameSettings.display.get_vsync())
	set_color_brightness(GameSettings.display.get_color_brightness())
	set_color_contrast(GameSettings.display.get_color_contrast())
	set_color_saturation(GameSettings.display.get_color_saturation())

func set_display_mode(mode: DisplayServer.WindowMode) -> void:
	display_option.select((
			0 if mode == DisplayServer.WINDOW_MODE_EXCLUSIVE_FULLSCREEN else
			1 if mode == DisplayServer.WINDOW_MODE_FULLSCREEN else
			2))
	display_option.item_selected.emit(display_option.selected)

func set_fps_limit(fps_limit: int) -> void:
	fps_option.select((
			0 if fps_limit == 45 else
			1 if fps_limit == 60 else
			2 if fps_limit == 90 else
			3 if fps_limit == 144 else
			4))
	fps_option.item_selected.emit(fps_option.selected)

func set_vsync(mode: DisplayServer.VSyncMode) -> void:
	vsync_checkbox.button_pressed = (
			true if mode == DisplayServer.VSYNC_ADAPTIVE else
			false
	)

func set_color_brightness(value: float) -> void:
	brightness_slider.value = value

func set_color_contrast(value: float) -> void:
	contrast_slider.value = value
	
func set_color_saturation(value: float) -> void:
	saturation_slider.value = value

func _on_save_pressed() -> void:
	GameSettings.display.save_mode(
			DisplayServer.WINDOW_MODE_EXCLUSIVE_FULLSCREEN if display_option.selected == 0 else
			DisplayServer.WINDOW_MODE_FULLSCREEN if display_option.selected == 1 else
			DisplayServer.WINDOW_MODE_WINDOWED)
	GameSettings.display.save_fps_limit(
			0 if fps_option.selected == fps_option.item_count else
			fps_option.get_item_text(fps_option.selected).to_int())
	GameSettings.display.save_vsync(
			DisplayServer.VSYNC_ADAPTIVE if vsync_checkbox.button_pressed else
			DisplayServer.VSYNC_DISABLED)
	GameSettings.display.save_color_brightness(brightness_slider.value)
	GameSettings.display.save_color_contrast(contrast_slider.value)
	GameSettings.display.save_color_saturation(saturation_slider.value)
	GameSettings.save()

func _on_cancel_pressed() -> void:
	update()

func _on_display_mode_changed(index: int) -> void:
	GameSettings.display.set_mode(
			DisplayServer.WINDOW_MODE_EXCLUSIVE_FULLSCREEN if index == 0 else
			DisplayServer.WINDOW_MODE_FULLSCREEN if index == 1 else
			DisplayServer.WINDOW_MODE_WINDOWED)

func _on_fps_limit_changed(index: int) -> void:
	GameSettings.display.set_fps_limit(
			0 if index == fps_option.item_count else
			fps_option.get_item_text(index).to_int())

func _on_vsync_changed(button_pressed: bool) -> void:
	GameSettings.display.set_vsync(
			DisplayServer.VSYNC_ADAPTIVE if button_pressed else
			DisplayServer.VSYNC_DISABLED)

func _on_brightness_changed(value: float) -> void:
	brightness_label.text = "%.1f" % value
	GameSettings.display.set_color_brightness(value)

func _on_contrast_changed(value: float) -> void:
	contrast_label.text = "%.1f" % value
	GameSettings.display.set_color_contrast(value)

func _on_saturation_changed(value: float) -> void:
	saturation_label.text = "%.1f" % value
	GameSettings.display.set_color_saturation(value)
