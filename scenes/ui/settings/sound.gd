extends TabBar

@onready var master_label: Label = %VMasterLabel
@onready var master_slider: HSlider = %MasterVolume
@onready var music_label: Label = %VMusicLabel
@onready var music_slider: HSlider = %MusicVolume
@onready var sound_label: Label = %VSoundLabel
@onready var sound_slider: HSlider = %SoundVolume

func _ready() -> void:
	update()

func update() -> void:
	set_volume_master(GameSettings.sound.get_volume(GameSettings.sound.master_idx)*100)
	set_volume_music(GameSettings.sound.get_volume(GameSettings.sound.music_idx)*100)
	set_volume_sound(GameSettings.sound.get_volume(GameSettings.sound.sound_idx)*100)

func set_volume_master(value: float) -> void:
	master_slider.value = value

func set_volume_music(value: float) -> void:
	music_slider.value = value

func set_volume_sound(value: float) -> void:
	sound_slider.value = value

func _on_save_pressed() -> void:
	GameSettings.sound.save_volume(GameSettings.sound.master_idx, master_slider.value*0.01)
	GameSettings.sound.save_volume(GameSettings.sound.music_idx, music_slider.value*0.01)
	GameSettings.sound.save_volume(GameSettings.sound.sound_idx, sound_slider.value*0.01)
	GameSettings.save()

func _on_cancel_pressed() -> void:
	update()

func _on_master_volume_value_changed(value: float) -> void:
	master_label.text = str(int(value))
	GameSettings.sound.set_volume(GameSettings.sound.master_idx, value*0.01)

func _on_music_volume_value_changed(value: float) -> void:
	music_label.text = str(int(value))
	GameSettings.sound.set_volume(GameSettings.sound.music_idx, value*0.01)

func _on_sound_volume_value_changed(value: float) -> void:
	sound_label.text = str(int(value))
	GameSettings.sound.set_volume(GameSettings.sound.sound_idx, value*0.01)
