extends HBoxContainer


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func set_decs(s):
	%Desc.text = s
	
func set_progress(current : float , goal : float):
	if current >= goal:
		%Progress.text = "Completed"
		%Progress.add_theme_color_override("font_color",Color(0, 128, 0))
	else:
		%Progress.text = str(current)+ "/" + str(goal)
	var rat = current/goal
	if rat > 1: rat = 1
	$PanelContainer/Panel/ProgressBar.value = rat
func set_title(s):
	%Title.text = s
