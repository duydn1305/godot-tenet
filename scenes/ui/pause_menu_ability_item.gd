extends PanelContainer

func setup_data(skill_name, skill_desc, icon : Texture2D, skill_level_require : int = 0):
	if skill_level_require > 0:
		$HBoxContainer/VBoxContainer/HBoxContainer/PanelContainer2/Level.text = "lv." + str(skill_level_require)
		pass
	if icon != null: $HBoxContainer/PanelContainer/Icon.texture = icon
	$HBoxContainer/VBoxContainer/HBoxContainer/PanelContainer/Name.text = skill_name
	$HBoxContainer/VBoxContainer/PanelContainer/Description.text = skill_desc
