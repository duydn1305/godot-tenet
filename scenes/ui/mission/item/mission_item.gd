class_name MissionItem
extends PanelContainer
@onready var slider = $MarginContainer/ColorRect/Slider

var tween : Tween
@export var quest_id : int :
	set(new_value):
		quest_id = new_value
		$VBoxContainer/QuestId.text = "Quest: " + to_roman(quest_id)
		
var format_value = "%d x "
@export var mission_description : String :
	set(desc):
		mission_description = desc
		%Description.text = mission_description
		

@export var reward_types : Const.RewardTypes = Const.RewardTypes.Coin
@export var is_complete : bool = false :
	set(new_status):
		if is_complete == new_status:
			return
		if tween != null && tween.is_running():
			return
		is_complete = new_status
		var icon = %Icon as TextureRect
		if new_status:
			icon.texture = preload("res://assets/ui/mission/mission_done.png")
			tween = get_tree().create_tween()
			slider.scale = Vector2.ZERO
			tween.tween_property(slider, "scale", Vector2(1, 1), 0.4)

		else:
			icon.texture = preload("res://assets/ui/mission/mission_in_progress.png")

@export var reward_value : int = 0:
	set(new_value):
		reward_value = new_value
		$VBoxContainer/MarginContainer/HBoxContainer/Value.text = format_value % reward_value
		
	
@export var progression : float = 0 :
	set(new_progress):
		if is_complete || (tween != null && tween.is_running()) || slider == null:
			return
		progression = max(min(1, new_progress), 0)
		slider.scale.x = progression
		


@rpc("any_peer", "call_local")
func update_desc(description : String):
	mission_description = description
	
	
@rpc("any_peer", "call_local")
func update_status(status : bool):
	is_complete = status
	
	
@rpc("any_peer", "call_local")
func update_reward_value(value : int):
	reward_value = value
	
@rpc("any_peer", "call_local")
func update_progression(progress : float):
	progression = progress
	
	
func to_roman(n):
	var digits = [[90, 'XC'], [50, 'L'], [40, 'XL'], [10, 'X'], [9, 'IX'], [5, 'V'], [4, 'IV'], [1, 'I']]
	var result = ''
	while len(digits) > 0:
		var val = digits[0][0]
		var romn = digits[0][1]
		if n < val:
			digits.remove_at(0) # Removes first element
		else:
			n -= val
			result += romn
	return result
