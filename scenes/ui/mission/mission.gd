class_name Mission
extends MarginContainer


func set_status(total_complete: int, total_mission: int)-> void:
	var status = %Status as Label
	if total_complete - total_mission == 0:
		status.text = "%d / %d" % [total_complete, total_mission]
	else:
		status.text = "Completed"
