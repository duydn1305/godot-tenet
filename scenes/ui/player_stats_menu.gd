extends MarginContainer

@onready var grid_container = $VBoxContainer/PanelContainer/GridContainer
var stats = {}
var stats_item = preload("res://scenes/ui/stat_item.tscn")
@onready var level = $VBoxContainer/HeroStats/HBoxContainer/VBoxContainer2/Level
@onready var coin = $VBoxContainer/HeroStats/HBoxContainer/VBoxContainer2/Coin

func _ready():
	visibility_changed.connect(on_visibility_changed)
	for i in grid_container.get_children():
		grid_container.remove_child(i)
	
	

func on_visibility_changed():
	for i in grid_container.get_children():
		grid_container.remove_child(i)
	if stats.has("stats_by_enum"):
		for k in stats['stats_by_enum'].keys():
			load_stats_on_board(k, stats['stats_by_enum'][k])
	if stats.has("extra_data"):
		# get level, coin, enemy killed...
		if stats['extra_data'].has('level'):
			level.text = str(stats['extra_data']['level'])
		if stats['extra_data'].has('coin'):
			coin.text = str(int(stats['extra_data']['coin']))
	
func load_stats_on_board(key : Const.Stats, value):
	var i = stats_item.instantiate()
	match key:
		Const.Stats.HP:
			i.get_node("Stat/Value").text = 'HP'
			i.get_node("Number/Value").tooltip_text = "Health point"
			i.get_node("Number/Value").text = str(value)
		Const.Stats.MP:
			i.get_node("Stat/Value").text = 'MP'
			i.get_node("Number/Value").tooltip_text = "Mana point"	
			i.get_node("Number/Value").text = str(value)
		Const.Stats.AMMOR:
			i.get_node("Stat/Value").text = 'AMR'
			i.get_node("Number/Value").tooltip_text = "Ammor point"
			i.get_node("Number/Value").text = str(value)
		Const.Stats.DMG:
			i.get_node("Stat/Value").text = 'DMG'
			i.get_node("Number/Value").tooltip_text = "Character Damage"
			i.get_node("Number/Value").text = str(value)
		Const.Stats.MOVE_SPEED:
			i.get_node("Stat/Value").text = 'MS'
			i.get_node("Number/Value").tooltip_text = "Move speed"
			i.get_node("Number/Value").text = str(value)
		Const.Stats.ATTACK_SPEED:
			i.get_node("Stat/Value").text = 'ATS'
			i.get_node("Number/Value").tooltip_text = "Attack speed"
			i.get_node("Number/Value").text = str(value)
		Const.Stats.CRIT_DMG:
			i.get_node("Stat/Value").text = 'CRD'
			i.get_node("Number/Value").tooltip_text = "Critical damage"
			i.get_node("Number/Value").text = str(value)
		Const.Stats.CRIT_RATE:
			i.get_node("Stat/Value").text = 'CRR'
			i.get_node("Number/Value").tooltip_text = "Critical rate"
			i.get_node("Number/Value").text = str(value)
		Const.Stats.EVADE:
			i.get_node("Stat/Value").text = 'EVD'
			i.get_node("Number/Value").tooltip_text = "Evade rate"
			i.get_node("Number/Value").text = str(value)
		_:
			return
	grid_container.add_child(i)
