extends PanelContainer

var basic_stats : BasicStats # tmp

var ability_item = preload("res://scenes/ui/pause_menu_ability_item.tscn")
@onready var list_skills = $MarginContainer/VBoxContainer/ScrollContainer/ListSkills
var is_data_loaded = false
func _ready():
	
	for i in list_skills.get_children():
		list_skills.remove_child(i)
	
#	skill_q
#	skill_e

func preload_skill_data():
	if is_data_loaded || basic_stats == null:
		return
	is_data_loaded = true
	load_skill_by_key(Const.ActiveSkill.Q)
	load_skill_by_key(Const.ActiveSkill.E)
	load_skill_by_key(Const.ActiveSkill.R)
	

func load_skill_by_key(skill_key_name : Const.ActiveSkill):
	if basic_stats == null:
		return
#	if stats.has(skill_key_name):
		
	var skill_data : BasicStats
	if skill_key_name == Const.ActiveSkill.Q:
		skill_data = basic_stats.skill_q
	if skill_key_name == Const.ActiveSkill.E:
		skill_data = basic_stats.skill_e
	if skill_key_name == Const.ActiveSkill.R:
		skill_data = basic_stats.skill_r
	
	if skill_data == null:
		return
	var i = ability_item.instantiate()
	var skill_name = skill_data.object_name
	var skill_desc = skill_data.object_description
	var icon = skill_data.icon
	i.setup_data(skill_name, skill_desc, icon)
	list_skills.add_child(i)
	
