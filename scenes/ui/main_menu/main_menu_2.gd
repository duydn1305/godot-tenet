extends CanvasLayer

@export_group("Timing")
@export_range(0.5, 5) var time_light := 1.5
@export_range(0.5, 5) var time_logo_dissolve := 2.0
@export_range(0.5, 5) var time_show_btn := 1.0
@export_group("")

@export var logo_dissolve: ShaderMaterial
@export var logo_shiny: ShaderMaterial

@onready var logo := %Logo
@onready var btn_container := %ButtonContainer
@onready var light := $GlobalLight
@onready var audio := $Audio

func _ready() -> void:
	BgMusic.play_type(BgMusic.Type.IDLE)
	audio.volume_db = linear_to_db(0.0)
	_anim_phase_1()

func _anim_phase_1() -> void:
	var tween := Helper.new_tween(self) as Tween
	tween.set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_CIRC)
	tween.tween_property(light, "color", Color.WHITE, time_light).from_current()
	tween.tween_method(func(progress: float) -> void:
		logo.material = logo_dissolve
		logo_dissolve.set_shader_parameter("progress", progress)
	, 1.0, 0.0, time_logo_dissolve)
	tween.tween_callback(_anim_phase_2)
	tween.play()

func _anim_phase_2() -> void:
	logo.material = logo_shiny
	var tween := Helper.new_tween(self) as Tween
	tween.set_ease(Tween.EASE_OUT).set_trans(Tween.TRANS_BOUNCE)
	tween.tween_property(btn_container, "modulate", Color.WHITE, time_show_btn).from_current()
	tween.tween_callback(func() -> void:
		audio.volume_db = linear_to_db(1.0)
		for btn in btn_container.get_children():
			(btn as Button).disabled = false
	)
	tween.play()

func _on_open_save_data_pressed() -> void:
	Store.open_save_folder()

func _on_quit_pressed() -> void:
	var confirm_modal := Helper.confirm_modal_scene.instantiate() as ConfirmModal
	Helper.layer_menu_overlay.add_child(confirm_modal)
	confirm_modal.set_header_text("Quit")
	confirm_modal.set_body_text("Are you sure? :(")
	confirm_modal.pressed.connect(func(ok: bool) -> void:
		if !ok: return
		get_tree().quit()
	)

func _on_start_pressed() -> void:
	SceneTransition.start_transition()
	await SceneTransition.transition_halfway
	get_tree().change_scene_to_file("res://scenes/lodging/house/house.tscn")

func _on_how_to_play_pressed() -> void:
	SceneTransition.start_transition()
	await SceneTransition.transition_halfway
	get_tree().change_scene_to_file("res://scenes/ui/howtoplay/htp.tscn")
