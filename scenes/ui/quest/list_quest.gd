extends CanvasLayer
@onready var listing = $MarginContainer/Listing
@export var quest_manager_node : Node 

var quest_item_scene : PackedScene = preload("res://scenes/ui/mission/item/mission_item.tscn")
var quest_by_id = {}

func clear_old_quest():
	for item in listing.get_children():
		listing.remove_child(item)
		
		
func append_new_quest(data : Dictionary):
	# quest_id, description, reward
	var item = quest_item_scene.instantiate() as MissionItem
	$MarginContainer/Listing.add_child(item, true)
	item.is_complete = data["is_complete"]
	item.mission_description = data["description"]
	item.reward_value = data["reward"]
	item.quest_id = data["quest_id"]
	quest_by_id[item.quest_id] = item

func _ready():
	if !multiplayer.is_server():
		return
	if quest_manager_node != null:
		quest_manager_node.connect("quest_failed", handle_quest_failed)
		quest_manager_node.connect("quest_new_progress", handle_quest_new_progress)
		quest_manager_node.connect("quest_completed", handle_quest_completed)
		
func handle_quest_failed(quest_id : int):
	pass
	# end game
	
func handle_quest_new_progress(quest_id : int, desc : String, progress : float):
	(quest_by_id[quest_id] as MissionItem).update_desc.rpc(desc)
	(quest_by_id[quest_id] as MissionItem).update_progression.rpc(progress)
	
func handle_quest_completed(quest_id : int, t, reward):
	(quest_by_id[quest_id] as MissionItem).update_status.rpc(true)
	# increase coin
