class_name QuestUI
extends CanvasLayer


func set_text(text: String) -> void:
	var quest_description = %Description as Label
	quest_description.text = text
