extends CanvasLayer

@onready var exit_button = $ExitButton
@onready var wiki_items := %WikiItems as WikiItems

signal exit_page()
var exiting = false
var current_tab = ""

func _ready():
	exit_button.connect("pressed", exit_wiki_page)
	wiki_items.pressed.connect(on_item_col_pressed)
	change_view_on_pressed("Enemies")


func _unhandled_input(event):
	if event.is_action_pressed("escape"):
		get_tree().root.set_input_as_handled()
		exit_wiki_page()


func exit_wiki_page():
	if exiting:
		return
	exit_button.disabled = true
	exiting = true
	exit_page.emit()
	SceneTransition.start_transition()
	await SceneTransition.transition_halfway
	call_deferred("queue_free")

func change_view_on_pressed(tab: String) -> void:
	if (current_tab == tab): return
	set_visible_wiki_detail(tab)
	current_tab = tab
	var animation := $AnimationPlayer as AnimationPlayer
	wiki_items.visible = false
	animation.play("turn_page")
	wiki_items.set_data(tab)
	await animation.animation_finished
	wiki_items.visible = true

func on_item_col_pressed(data: BasicStats) -> void:
	var wiki_detail = $ItemDetail/WikiDetail as WikiItemDetail
	wiki_detail.visible = true
	wiki_detail.set_data(data)

func set_visible_wiki_detail(tab: String):
	var wiki_detail = $ItemDetail/WikiDetail as WikiItemDetail
	if current_tab == tab and wiki_detail.visible:
		wiki_detail.visible = true
	else:
		wiki_detail.visible = false
	
	
func _on_enemies_pressed():
	var tab = "Enemies"
	change_view_on_pressed(tab)


func _on_heroes_pressed():
	var tab = "Heroes"
	change_view_on_pressed(tab)


func _on_statues_pressed():
	var tab = "Statues"
	change_view_on_pressed(tab)


func _on_bosses_pressed():
	var tab = "Bosses"
	change_view_on_pressed(tab)
