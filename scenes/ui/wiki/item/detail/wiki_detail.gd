class_name WikiItemDetail
extends MarginContainer


var current_data: BasicStats

func set_data(data: BasicStats) -> void:
	current_data = data
	call_deferred("update_data")
	

func update_data() -> void:
	%Avatar.texture = current_data.icon
	%Name.text = current_data.object_name
	%Description.text = current_data.object_description
