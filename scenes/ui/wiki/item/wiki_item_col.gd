class_name WikiItemCol
extends PanelContainer


var current_data: BasicStats
signal pressed(data: BasicStats)

func set_data(data: BasicStats) -> void:
	current_data = data
	call_deferred("update_data")
	

func update_data() -> void:
	%Icon.texture = current_data.icon
	

func _on_button_pressed():
	pressed.emit(current_data)
