class_name WikiItems
extends MarginContainer


@export var item_col_scene: PackedScene
var current_tab: String
signal pressed(data: BasicStats)


func set_data(tab: String) -> void:
	var async_loader: FolderAsyncLoader
	
	if tab == "Enemies":
		async_loader = Helper.load_resources("res://resources/characters/1000_creep_data/", self) as FolderAsyncLoader
	elif tab == "Heroes":
		async_loader = Helper.load_resources("res://resources/characters/0_data/", self) as FolderAsyncLoader
	elif tab == "Bosses":
		async_loader = Helper.load_resources("res://resources/characters/700_mini_boss/", self) as FolderAsyncLoader
	else:
		async_loader = Helper.load_resources("res://resources/characters/4400_statues/", self) as FolderAsyncLoader
	
	var data := await async_loader.finished as Array[Resource]
	_load_data.call_deferred(data)
 
func _load_data(data: Array[Resource]) -> void:
	var wiki_item_view := %GridContainer as GridContainer
	if self.get_child_count() > 0:
		var children = wiki_item_view.get_children()
		for c in children:
			wiki_item_view.remove_child.call_deferred(c)
			c.call_deferred("queue_free")
	for item in data:
		item = item as BasicStats
		var col := item_col_scene.instantiate() as WikiItemCol
		col.pressed.connect(_on_wiki_item_pressed)
		wiki_item_view.add_child.call_deferred(col)
		col.set_data.call_deferred(item)

func _on_wiki_item_pressed(data: BasicStats) -> void:
	pressed.emit(data)
