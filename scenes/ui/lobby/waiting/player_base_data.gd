class_name PlayerBaseData

var id: int
var player_name: String
var character_name: String
var ready: bool

func _to_string() -> String:
	return "<PlayerBaseData>: {
		id: %d,
		player_name: %s,
		character_name: %s,
		ready: %s
	}" % [id, player_name, character_name, ready]

func to_dict() -> Dictionary:
	return {
		"id": id,
		"player_name": player_name,
		"character_name": character_name,
		"ready": ready
	}

static func from_dict(dict: Dictionary) -> PlayerBaseData:
	var res := PlayerBaseData.new()
	res.id = dict.id
	res.player_name = dict.player_name
	res.character_name = dict.character_name
	res.ready = dict.ready
	return res

static func get_by_id(array: Array[PlayerBaseData], pid: int) -> PlayerBaseData:
	var res := array.filter(func(item: PlayerBaseData) -> bool:
		return item.id == pid
	)
	return null if res.is_empty() else res[0]

static func remove_by_id(array: Array[PlayerBaseData], pid: int) -> bool:
	for idx in array.size():
		var item := array[idx]
		if item.id == pid:
			array.remove_at(idx)
			return true
	return false
