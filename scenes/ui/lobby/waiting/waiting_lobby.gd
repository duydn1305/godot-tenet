class_name WaitingLobby
extends PanelContainer

@export var lobby_host_scene: PackedScene
@export var lobby_client_scene: PackedScene
@export var row_scene: PackedScene

var is_host := true
var lobby_name: String
var password: String

@onready var row_container := %RowContainer

func _enter_tree():
	for i in Store.player_data_by_id:
		if i > 1:
			(Store.player_data_by_id as Dictionary).erase(i)

func _ready() -> void:
	Store.player_data["player_character_id"] = int(Store.general.last_character_id)
	if is_host:
		%Start.visible = true
		%Actions.visible = true
		var lobby_host := lobby_host_scene.instantiate() as LobbyHost
		lobby_host.lobby_name = lobby_name
		lobby_host.password = password
		add_child(lobby_host)
	else:
		%Ready.visible = true
		add_child(lobby_client_scene.instantiate())
		%LobbyName.text = lobby_name
		

@rpc("any_peer", "call_local")
func request_connect(player_name: String, char_name: String) -> void:
	var final_char_name
	match char_name:
		"2": final_char_name = "Warior"
		"3": final_char_name = "Knight"
		"5": final_char_name = "Lancer"
		"6": final_char_name = "Archer"
	print("someone request_connect: ", player_name)
#	Store.player_data["player_name"] = player_name
#	Store.player_data["player_character_name"] = final_char_name
	
	var data := PlayerBaseData.new()
	data.id = multiplayer.get_remote_sender_id()
	data.player_name = player_name
	data.character_name = final_char_name
	Store.players.append(data)
	update_lobby.rpc(Helper.arr_to_arr_dict(Store.players))

@rpc("any_peer", "call_local")
func request_player_ready() -> void:
	var data := PlayerBaseData.get_by_id(Store.players, multiplayer.get_remote_sender_id())
	data.ready = !data.ready
	update_lobby.rpc(Helper.arr_to_arr_dict(Store.players))

@rpc("call_local")
func update_lobby(data: Array) -> void:
	Helper.clear_children(row_container)
	for item in data:
		var row := row_scene.instantiate() as WaitingLobbyRow
		row.data = PlayerBaseData.from_dict(item)
		row_container.add_child(row)

#func host_start_game() -> void:
#	visible = false
#	# TODO: switch to loading screen
#	LoadingScreen.show_screen()
#
#	# load main dungeon
#	# change scene to file dungegon
#	load_world()
#
#	await get_tree().create_timer(3).timeout
#	print("timeout")
#	LoadingScreen.hide_screen()
#	queue_free.call_deferred()

# host press enter game
#func host_start_game():
#	LoadingScreen.show_screen.rpc()
#	print("LoadingScreen show and wait 4s")
#	# start game
##	$UI/Team/StartGame.disabled = true
#	LoadingScreen.new_loading_stage_for_lobby()
##	print(LoadingScreen.player_loading_status, "current: ", LoadingScreen.player_stage_loading)
#	LoadingScreen.refresh_player_list.rpc(LoadingScreen.player_loading_status)
#	# get data stage
#	entering_game()
#	LoadingScreen.player_stage_loading += 1	# host done
##	print("LoadingScreen.player_stage_loading, ", LoadingScreen.player_stage_loading)
#	await LoadingScreen.all_player_stage_ready
#	queue_free_watiting_lobby.rpc()
#	LoadingScreen.hide_screen.rpc()
#	# also hide Multiplayer/UI (lobby UI)
#	get_node("/root/Multiplayer/UI").visible = false
#



# host press enter game
func host_start_game():
	# start game
	LoadingScreen.update_player_name()
	LoadingScreen.refresh_player_list.rpc(LoadingScreen.player_loading_status)
	# get data stage
#	LoadingScreen.new_loading_stage()
	LoadingScreen.new_loading_stage_for_lobby()
	game_going_to_start.rpc()
	await LoadingScreen.all_player_stage_ready
	
	# create node stage
	LoadingScreen.new_loading_stage()
	entering_game()
	LoadingScreen.player_stage_loading += 1	# host done
	await LoadingScreen.all_player_stage_ready
	get_node("/root/Multiplayer").emit_signal("all_player_spawned_dungeon")
	queue_free_watiting_lobby.rpc()	
	

@rpc("any_peer", "call_local")
func game_going_to_start():
	get_node("/root/Multiplayer/UI").visible = false
	LoadingScreen.show()
	if get_tree().root.has_node("/root/Multiplayer/UI"):
		get_tree().root.get_node("/root/Multiplayer/UI").queue_free()
#	LoadingScreen.visible = true
	# send neccesary data to host: general + meta data
	print("data store in: ", multiplayer.get_unique_id(), " .dâta: ",Store.players)
	print("data loading store in: ", multiplayer.get_unique_id(), " .dâta: ",LoadingScreen.player_loading_status)
#	player_send_data.rpc_id(1, PlayerBaseData.get_by_id(Store.players, multiplayer.get_unique_id()).to_dict(), Store.player_data) # meta, character, ...
	print("runtime player datra: ", multiplayer.get_unique_id(), " .data: ",Store.player_data)
	player_send_data.rpc_id(1, Store.player_data) # meta, character, ...


# host handle
# player send host neccessary data
@rpc("any_peer", "call_local")
func player_send_data(sender_data : Dictionary = {}):
#	var player_data = PlayerBaseData.from_dict(sender_data)
	var sender_id = multiplayer.get_remote_sender_id()
#	Store.players.append(player_data)
#	sender_data["player_name"] = Store.player_data
	
	Store.player_data_by_id[sender_id] = sender_data
	var player_name = PlayerBaseData.get_by_id(Store.players, sender_id).player_name
	Store.player_data_by_id[sender_id]['player_name'] = player_name
#	GameEvents.player_data_by_id[sender_id] = sender_data
#	print("player id: ", sender_data)
	# assume there is a little processing time between 1s -> 10s
#	await get_tree().create_timer(5+randf()*2).timeout
#	player_list_id[sender_id] = true # mark as this player send data done
	LoadingScreen.player_loading_status[sender_id]["ready"] = true
	LoadingScreen.player_stage_loading += 1
	LoadingScreen.refresh_player_list.rpc(LoadingScreen.player_loading_status)
	

@rpc("any_peer", "call_local")
func queue_free_watiting_lobby():
	queue_free.call_deferred()
		

func _on_ready_pressed() -> void:
	request_player_ready.rpc_id(1)

func _on_start_pressed() -> void:
	print("press start")
	var start_btn := %Start as Button

	start_btn.disabled = true
	var tween := Helper.new_tween(self) as Tween
	tween.tween_callback(func() -> void:
		start_btn.disabled = false
	).set_delay(1.5)
	tween.play()

	for data in Store.players:
		if data.id != 1 and !data.ready:
			Helper.toasts.add_toast("Not all players are ready!", 1.5)
			return
	multiplayer.multiplayer_peer.refuse_new_connections = true
	host_start_game()
	
func entering_game():
#	LoadingScreen.visible = true
	# Only change level on the server.
	# Clients will instantiate the level via the spawner.
	if multiplayer.is_server():
		# entering dungeon level 0 (room 0)
		load_dungeon(load("res://scenes/dungeons/main_dungeon.tscn"))

# entering new room

# load world
# file Main: contain map, player, UI, ...
func load_dungeon(dungeon_scene : PackedScene):
	# 1. clear old  in dungeon
	var level = dungeon_scene.instantiate()
	get_node("/root/Multiplayer/Dungeon").add_child(level)
	await get_tree().create_timer(1+randf()*2).timeout
	
	
	
