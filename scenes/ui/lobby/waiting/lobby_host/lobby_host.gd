class_name LobbyHost
extends Node

const AUTH_TIMEOUT := 3.5

var lobby_name: String
var password: String

func _ready() -> void:
	NetworkHelper.reset_multiplayer()
	var sm := multiplayer as SceneMultiplayer
	sm.server_relay = false
	sm.auth_timeout = AUTH_TIMEOUT
	sm.auth_callback = _auth_server
	sm.peer_authenticating.connect(_peer_ban_check)
	sm.peer_connected.connect(_peer_connected)
	sm.peer_disconnected.connect(_peer_disconnected)
	var enet := ENetMultiplayerPeer.new()
	enet.create_server(NetworkHelper.PORT_GAME, NetworkHelper.MAX_PLAYERS)
	sm.multiplayer_peer = enet

	var lobby := get_parent() as WaitingLobby
	lobby.request_connect.rpc_id(1, Store.general.player_name, Store.general.last_character_id)

func _exit_tree() -> void:
	var sm := multiplayer as SceneMultiplayer
	sm.peer_authenticating.disconnect(_peer_ban_check)
	sm.peer_connected.disconnect(_peer_connected)
	sm.peer_disconnected.disconnect(_peer_disconnected)

func _peer_connected(pid: int) -> void:
	var count := 1 + multiplayer.get_peers().size()
	if count >= NetworkHelper.MAX_PLAYERS:
		multiplayer.multiplayer_peer.refuse_new_connections = true
		if count > NetworkHelper.MAX_PLAYERS:
			multiplayer.multiplayer_peer.disconnect_peer(pid)

func _peer_disconnected(pid: int) -> void:
	PlayerBaseData.remove_by_id(Store.players, pid)
	var lobby := get_parent() as WaitingLobby
	lobby.update_lobby.rpc(Helper.arr_to_arr_dict(Store.players))
	var count := 1 + multiplayer.get_peers().size()
	if count < NetworkHelper.MAX_PLAYERS:
		multiplayer.multiplayer_peer.refuse_new_connections = false

func _peer_ban_check(peer: int) -> void:
	var sm := multiplayer as SceneMultiplayer
	var enet := sm.multiplayer_peer as ENetMultiplayerPeer
	var peer_ip := enet.get_peer(peer).get_remote_address()
	if Store.ban_list.has(peer_ip):
		sm.send_auth(peer, NetworkHelper.create(NetworkHelper.PacketType.AUTH_RESP,
		{
			"message": NetworkHelper.AuthResponse.BANNED
		}))

func _auth_server(peer: int, packet: PackedByteArray) -> void:
	var sm := multiplayer as SceneMultiplayer
	var enet := sm.multiplayer_peer as ENetMultiplayerPeer
	var peer_ip := enet.get_peer(peer).get_remote_address()
	if sm.refuse_new_connections or Store.ban_list.has(peer_ip):
		return

	var data := NetworkHelper.validate(packet)
	if data.is_empty(): return
	if data.type != NetworkHelper.PacketType.AUTH_REQ: return
	if data.password == password:
		sm.send_auth(peer, NetworkHelper.create(NetworkHelper.PacketType.AUTH_RESP,
		{
			"message": NetworkHelper.AuthResponse.OK
		}))
		sm.complete_auth(peer)
	else:
		sm.send_auth(peer, NetworkHelper.create(NetworkHelper.PacketType.AUTH_RESP,
		{
			"message": NetworkHelper.AuthResponse.WRONG
		}))
