extends Node

@export var debug_data := true

var data := BroadcastData.new()

var broadcaster := PacketPeerUDP.new()

func _ready() -> void:
	broadcaster.set_broadcast_enabled(true)
	if broadcaster.set_dest_address("255.255.255.255", NetworkHelper.PORT_BROADCAST) != OK:
		print("[Broadcaster]: FAILED!")
		queue_free()
		return
	print("[Broadcaster]: Started successfully!")
	$Timer.start()

func _exit_tree() -> void:
	broadcaster.close()

func _on_timer_timeout() -> void:
	var host := owner as LobbyHost
	data.lobby_name = host.lobby_name
	data.creator = Store.general.player_name
	data.has_pwd = !host.password.is_empty()
	data.player_count = 1 + multiplayer.get_peers().size()
	var packet := NetworkHelper.create(NetworkHelper.PacketType.BROADCAST, data.to_dict())
	broadcaster.put_packet(packet)
	if OS.has_feature("debug") and debug_data:
		print("[Broadcaster]: %s" % NetworkHelper.validate(packet))
