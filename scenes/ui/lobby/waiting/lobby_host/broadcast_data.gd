class_name BroadcastData

var lobby_name: String
var creator: String
var player_count: int
var has_pwd: bool

func to_dict() -> Dictionary:
	return {
		"lobby_name": lobby_name,
		"creator": creator,
		"player_count": player_count,
		"has_pwd": has_pwd
	}

static func from_dict(dict: Dictionary) -> BroadcastData:
	var res := BroadcastData.new()
	res.lobby_name = dict.lobby_name
	res.creator = dict.creator
	res.player_count = dict.player_count
	res.has_pwd = dict.has_pwd
	return res
