extends Button

var _browser_scene := load("res://scenes/ui/lobby/browser/browser.tscn")

func _on_pressed() -> void:
	var modal := Helper.confirm_modal_scene.instantiate() as ConfirmModal
	Helper.layer_ui_overlay.add_child(modal)
	modal.set_body_text(("Leaving also disband this lobby" if multiplayer.is_server()
			else "Do you want to leave?"))
	modal.pressed.connect(_confirm)

func _confirm(ok: bool) -> void:
	if !ok: return
	owner.queue_free()
	Helper.layer_ui_overlay.add_child(_browser_scene.instantiate())
