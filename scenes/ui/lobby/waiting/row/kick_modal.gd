class_name LobbyKickModal
extends ColorRect

var target_id: int

func _ready() -> void:
	%Confirm.grab_focus()

func _on_confirm_pressed() -> void:
	var enet := multiplayer.multiplayer_peer as ENetMultiplayerPeer
	enet.disconnect_peer(target_id)
	var checked := %Checkbox.button_pressed as bool
	if checked:
		Store.ban_list[enet.get_peer(target_id).get_remote_address()] = true
	queue_free()

func _on_cancel_pressed() -> void:
	queue_free()
