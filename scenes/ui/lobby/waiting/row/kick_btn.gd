extends Button

@export var kick_scene: PackedScene

func _on_pressed() -> void:
	var modal := kick_scene.instantiate() as LobbyKickModal
	var data := (owner as WaitingLobbyRow).data
	modal.target_id = data.id
	Helper.layer_ui_overlay.add_child(modal)
