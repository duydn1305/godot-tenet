class_name WaitingLobbyRow
extends PanelContainer

@onready var _player_label := %Player as Label
@onready var _character_label := %Character as Label
@onready var _ready_label := %Ready as Label

var data: PlayerBaseData

func _ready() -> void:
	if multiplayer.is_server():
		%Actions.visible = true
		if data.id == multiplayer.get_unique_id():
			%Kick.disabled = true

	_player_label.text = data.player_name
	_character_label.text = data.character_name
	_ready_label.text = (
			"Y" if data.id == 1 else
			"Y" if data.ready else
			"N")
