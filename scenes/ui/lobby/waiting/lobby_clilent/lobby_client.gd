extends Node

var _browser_scene := load("res://scenes/ui/lobby/browser/browser.tscn")

func _enter_tree() -> void:
	multiplayer.connected_to_server.connect(_connected)
	multiplayer.server_disconnected.connect(_disconnected)

func _exit_tree() -> void:
	multiplayer.connected_to_server.disconnect(_connected)
	multiplayer.server_disconnected.disconnect(_disconnected)

func _connected() -> void:
	var lobby := get_parent() as WaitingLobby
	lobby.request_connect.rpc_id(1, Store.general.player_name, Store.general.last_character_id)

func _disconnected() -> void:
	get_parent().queue_free()
	Helper.layer_ui_overlay.add_child(_browser_scene.instantiate())
	var modal := Helper.confirm_modal_scene.instantiate() as ConfirmModal
	modal.is_inform = true
	Helper.layer_ui_overlay.add_child(modal)
	modal.set_header_text("Disconnected")
	modal.set_body_text("Disconnected from lobby")
	pass
