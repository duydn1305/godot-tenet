class_name LobbyBrowser
extends PanelContainer

const ALIVE_TIME := 3500

@export var row_scene: PackedScene
@export var waiting_lobby_scene: PackedScene

var lobbies := {}
var _udp_pull := PacketPeerUDP.new()

@onready var _row_container := %RowContainer

func _enter_tree() -> void:
	NetworkHelper.reset_multiplayer()
	if _udp_pull.bind(NetworkHelper.PORT_BROADCAST, "0.0.0.0") != OK:
		print("[Browser]: Failed biding port!")
	else:
		print("[Browser]: Started browsing")
	$CanvasLayer/BackBtn.visible = true	

func _exit_tree() -> void:
	_udp_pull.close()
	$CanvasLayer/BackBtn.visible = false

func _connect_lobby(row: BrowserRow) -> void:
	var lobby := waiting_lobby_scene.instantiate() as WaitingLobby
	lobby.is_host = false
	lobby.lobby_name = row.data.lobby_name
	queue_free()
	Helper.layer_ui_overlay.add_child(lobby)

func _on_refresh_timer_timeout() -> void:
	for ip in lobbies.keys():
		var data = lobbies[ip] as BrowserRow
		var cur_time := Time.get_ticks_msec()
		var last_time: int = data.updated_at
		if cur_time - last_time > ALIVE_TIME:
			data.queue_free()
			lobbies.erase(ip)

	while _udp_pull.get_available_packet_count() > 0:
		var packet := _udp_pull.get_packet()
		var host_ip := _udp_pull.get_packet_ip()
		if host_ip.is_empty(): continue

		var _data := NetworkHelper.validate(packet)
		if _data.is_empty(): continue
		if _data.type != NetworkHelper.PacketType.BROADCAST: continue
		var data := BroadcastData.from_dict(_data)

		if !lobbies.has(host_ip):
			var row := row_scene.instantiate() as BrowserRow
			row.host_ip = host_ip
			row.clicked_success.connect(_connect_lobby.bind(row));
			_row_container.add_child(row)
			lobbies[host_ip] = row
		var row := lobbies[host_ip] as BrowserRow
		row.data = data
		row.updated_at = Time.get_ticks_msec()
		row.update()

# handle between home and lobby
func _on_back_pressed():
	exit_browse_lobby_page()
	
var exiting = false

func _unhandled_input(event):
	if event.is_action_pressed("escape"):
		get_tree().root.set_input_as_handled()
		exit_browse_lobby_page()

func exit_browse_lobby_page():
	if exiting:
		return
	$CanvasLayer/BackBtn.disabled = true
	exiting = true
	SceneTransition.start_transition()
	await SceneTransition.transition_halfway
	get_tree().change_scene_to_file("res://scenes/lodging/house/house.tscn")
	call_deferred("queue_free")

