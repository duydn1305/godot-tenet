extends Button

@export var edit_name_scene: PackedScene

func _on_pressed() -> void:
	Helper.layer_ui_overlay.add_child(edit_name_scene.instantiate())
