extends Button


func _on_pressed():
		# Start as client
	var player_name = "player 3"
		
	var peer = ENetMultiplayerPeer.new()
	peer.create_client("127.0.0.1", NetworkHelper.PORT_GAME)
	if peer.get_connection_status() == MultiplayerPeer.CONNECTION_DISCONNECTED:
		OS.alert("Failed to start multiplayer client")
		return
	multiplayer.multiplayer_peer = peer


	var lobby := preload("res://scenes/ui/lobby/waiting/waiting_lobby.tscn").instantiate() as WaitingLobby
	lobby.is_host = false
	lobby.lobby_name = "join via local host"
	queue_free()
	Helper.layer_ui_overlay.add_child(lobby)
