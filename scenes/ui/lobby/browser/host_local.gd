extends Button


func _on_pressed():
	# Start as server
	var peer = ENetMultiplayerPeer.new()
	var player_name = Store.general.player_name
	peer.create_server(NetworkHelper.PORT_GAME)
	if peer.get_connection_status() == MultiplayerPeer.CONNECTION_DISCONNECTED:
		OS.alert("Failed to start multiplayer server")
		return
	multiplayer.multiplayer_peer = peer
	
	multiplayer.peer_connected.connect(handle_player_join)
#	multiplayer.peer_disconnected.connect(handle_player_leave)

	var lobby := preload("res://scenes/ui/lobby/waiting/waiting_lobby.tscn").instantiate() as WaitingLobby
	lobby.is_host = true
	lobby.lobby_name = "join via local host"
	queue_free()
	Helper.layer_ui_overlay.add_child(lobby)
#	lobby.visible = false


# host control
func handle_player_join(id : int):
	print("someone join by local: ", id)
