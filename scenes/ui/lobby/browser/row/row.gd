class_name BrowserRow
extends PanelContainer

@export var join_lobby_scene: PackedScene

var host_ip: String
var data: BroadcastData
var updated_at: int

signal clicked_success

func _ready() -> void:
	%MaxPlayers.text = " / %d" % NetworkHelper.MAX_PLAYERS

func update() -> void:
	%LobbyName.text = data.lobby_name
	%CreatedBy.text = data.creator
	%Players.text = str(data.player_count)
	_update_has_pwd()

func _update_has_pwd() -> void:
	%Password.visible = data.has_pwd
	%NoPassword.visible = !data.has_pwd

func _on_button_double_clicked() -> void:
	var join_lobby := join_lobby_scene.instantiate() as JoinLobby
	join_lobby.host_ip = host_ip
	join_lobby.has_pwd = data.has_pwd
	join_lobby.joined.connect(_joined)
	Helper.layer_ui_overlay.add_child(join_lobby)

func _joined() -> void:
	clicked_success.emit()
