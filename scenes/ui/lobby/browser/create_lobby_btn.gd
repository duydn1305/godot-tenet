extends Button

@export var create_lobby_scene: PackedScene
@export var waiting_lobby_scene: PackedScene

func _on_pressed() -> void:
	var lobby_creator := create_lobby_scene.instantiate() as LobbyCreator
	lobby_creator.created.connect(_created)
	Helper.layer_ui_overlay.add_child(lobby_creator)

func _created(lobby_name: String, password: String) -> void:
	var waiting_lobby := waiting_lobby_scene.instantiate() as WaitingLobby
	waiting_lobby.is_host = true
	waiting_lobby.lobby_name = lobby_name
	waiting_lobby.password = password
	owner.queue_free()
	Helper.layer_ui_overlay.add_child(waiting_lobby)
