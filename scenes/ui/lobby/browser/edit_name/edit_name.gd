extends ColorRect

@onready var name_edit: LineEdit = %Name
@onready var name_v_label: Label = %NameValidate

func _ready() -> void:
	name_edit.text = Store.general.player_name
	name_edit.grab_focus()

func _on_save_pressed() -> void:
	if name_edit.text.is_empty():
		name_v_label.visible = true
		name_edit.grab_focus()
		return
	Store.general.player_name = name_edit.text
	Store.general.save()
	queue_free()

func _on_cancel_pressed() -> void:
	queue_free()
