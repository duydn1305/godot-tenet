class_name PasswordValidator
extends Node

signal finished(status: NetworkHelper.AuthResponse)

var host_ip: String
var password: String

func _enter_tree() -> void:
	NetworkHelper.reset_multiplayer()
	var sm := multiplayer as SceneMultiplayer
	sm.auth_timeout = LobbyHost.AUTH_TIMEOUT
	sm.auth_callback = _auth_client
	sm.peer_authenticating.connect(_auth_start)
	sm.peer_authentication_failed.connect(_connect_error)

	var enet := ENetMultiplayerPeer.new()
	if enet.create_client(host_ip, NetworkHelper.PORT_GAME) != OK:
		_connect_error(0)
	else:
		sm.multiplayer_peer = enet

func _exit_tree() -> void:
	var sm := multiplayer as SceneMultiplayer
	sm.peer_authenticating.disconnect(_auth_start)
	sm.peer_authentication_failed.disconnect(_connect_error)

func _connect_error(_pid: int) -> void:
	finished.emit(NetworkHelper.AuthResponse.ERROR)
	queue_free()

func _auth_start(host: int) -> void:
	var sm := multiplayer as SceneMultiplayer
	sm.send_auth(host, NetworkHelper.create(NetworkHelper.PacketType.AUTH_REQ,
	{
		"password": password
	}))

func _auth_client(host: int, packet: PackedByteArray) -> void:
	var data := NetworkHelper.validate(packet)
	if data.is_empty(): return
	if data.type != NetworkHelper.PacketType.AUTH_RESP: return
	var sm := multiplayer as SceneMultiplayer
	if data.message == NetworkHelper.AuthResponse.OK:
		sm.complete_auth(host)
		finished.emit(NetworkHelper.AuthResponse.OK)
		queue_free()
	else:
		sm.disconnect_peer(host)
		finished.emit(data.message)
		queue_free()
