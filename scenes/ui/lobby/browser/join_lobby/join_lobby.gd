class_name JoinLobby
extends ColorRect

@export var validator_scene: PackedScene

@onready var input_pwd := %Password as LineEdit
@onready var status_label := %Status as Label
@onready var join_btn := %Join as Button

signal joined

var host_ip: String
var has_pwd: bool

func _ready() -> void:
	input_pwd.editable = has_pwd
	input_pwd.grab_focus()
	if !has_pwd:
		join_btn.grab_focus()
		join_btn.pressed.emit()

func _on_join_pressed() -> void:
	join_btn.disabled = true
	status_label.text = "Connecting..."

	var validator := validator_scene.instantiate() as PasswordValidator
	validator.host_ip = host_ip
	validator.password = input_pwd.text if has_pwd else ""
	Helper.layer_ui_overlay.add_child(validator)

	var result := await validator.finished as NetworkHelper.AuthResponse
	if result == NetworkHelper.AuthResponse.OK:
		joined.emit()
		queue_free()
	status_label.text = (
			"Wrong password" if result == NetworkHelper.AuthResponse.WRONG else
			"Banned" if result == NetworkHelper.AuthResponse.BANNED else
			"Network error"
	)
	join_btn.disabled = false

func _on_cancel_pressed() -> void:
	queue_free()
