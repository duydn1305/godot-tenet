class_name LobbyCreator
extends ColorRect

@onready var lobby_name_edit: LineEdit = %LobbyName
@onready var password_edit: LineEdit = %Password

signal created(lobby_name: String, password: String)

func _ready() -> void:
	lobby_name_edit.grab_focus()

func _on_create_pressed() -> void:
	var valid_name := lobby_name_edit.text.strip_escapes().strip_edges()
	lobby_name_edit.text = valid_name
	if valid_name.is_empty():
		lobby_name_edit.grab_focus()
		return
	created.emit(lobby_name_edit.text, password_edit.text)
	queue_free()

func _on_cancel_pressed() -> void:
	queue_free()
