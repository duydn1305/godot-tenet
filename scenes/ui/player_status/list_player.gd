extends CanvasLayer

@export var player_ui_item : PackedScene
@onready var listing = $MarginContainer/Listing
@onready var level = $"../.."
@export var dungeon_node : Node 

var node_to_spawn = 0 :
	set(value):
		node_to_spawn = value
		if node_to_spawn == len(multiplayer.get_peers()):
			for item in $MarginContainer/Listing.get_children():
				item.node_loaded_for_all_player.emit()

@rpc("any_peer", "call_remote")
func node_loaded():
	node_to_spawn += 1
#	print("node_loaded() from :", multiplayer.get_remote_sender_id())

func _ready():
	$MarginContainer/MultiplayerSpawner.connect("spawned", handle_client_spawn)
	if !multiplayer.is_server():
		return
#	if dungeon_node != null:
#		await dungeon_node.all_player_ready
	GameEvents.player_disconnected.connect(handle_player_disconnect)
	
func handle_player_disconnect(player_id):
	for n in listing.get_children():
		if n.for_player_id == player_id:
			listing.remove_child(n)
	
func clear_list_player():
	for item in $MarginContainer/Listing.get_children():
		listing.remove_child(item)

# host init
func init_list_player():
	var host_item = get_new_item(1, Store.player_data_by_id[1]["player_name"], Store.player_data_by_id[1]["player_character_id"])
	listing.add_child(host_item, true)
	await get_tree().create_timer(1).timeout
	for player_id in multiplayer.get_peers():
		var client_item = get_new_item(player_id, Store.player_data_by_id[player_id]["player_name"], Store.player_data_by_id[player_id]["player_character_id"])
		listing.add_child(client_item, true)		
		await get_tree().create_timer(1).timeout
		
#	# fake
#	var player_item1 = get_new_item(2, "client 1")
#	listing.add_child(player_item1, true)			
#	await get_tree().create_timer(1).timeout
#	var player_item2 = get_new_item(2, "client 2")
#	listing.add_child(player_item2, true)			

func get_new_item(player_id, player_name, player_character_id):
	var object = player_ui_item.instantiate() as ListPlayerItemUI
	object.for_player_id = player_id
	object.for_player_name = player_name
	object.player_character_id = player_character_id
	return object

func handle_client_spawn(node : Node):
	print(node.for_player_id)
	if node.for_player_id == multiplayer.get_unique_id():
		node_loaded.rpc_id(1)
