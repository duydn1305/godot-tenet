extends MarginContainer

class_name ListPlayerItemUI

@onready var animation_player = $AnimationPlayer
@export var for_player_id = 1
#	set(new_value):
#		if new_value == multiplayer.get_unique_id():
#			visible = false
#		for_player_id = new_value
	
var for_player_name = "Axca Bgq"
var hp = 500		
var lv = 0
signal node_loaded_for_all_player()

@export var player_character_id : int = 5

func _ready():
#	print("for_player_id: ", for_player_id, "multiplayer: ", multiplayer.get_unique_id())

	if for_player_id == multiplayer.get_unique_id():
		visible = false
	if player_character_id == 2:
		%PlayerIcon.texture = preload("res://assets/02-warior/warior-avatar.png")
	if player_character_id == 3:
		%PlayerIcon.texture = preload("res://assets/03-knight/knight-avatar.png")
	if player_character_id == 5:
		%PlayerIcon.texture = preload("res://assets/ui/player_status/player_icon.png")
		
	if player_character_id == 6:
		%PlayerIcon.texture = preload("res://assets/06-archer/archer-icon.png")
		
#	animation_player.play("fade_in")
	if !multiplayer.is_server():
		return
	await node_loaded_for_all_player
	
#	print("for_player_name: ", for_player_name, " .for_player_id: ", for_player_id)
	
	play_animation.rpc("fade_in")
	GameEvents.player_hp_change.connect(handle_player_hp_change)
	GameEvents.player_level_up.connect(handle_player_level_up)
	set_player_name.rpc(for_player_name)
	update_player_status_hp_bar.rpc(1)

@rpc("any_peer", "call_local")
func set_player_name(player_name):
	$VBoxContainer/TextureRect/PanelContainer/Label.text = player_name
	
@rpc("any_peer", "call_local", "unreliable")
func update_player_status_hp_bar(hp_ratio):
	$VBoxContainer/TextureRect/TextureProgressBar.value = hp_ratio

@rpc("any_peer", "call_local")
func set_player_level(level):
	lv = level	
	$VBoxContainer/PanelContainer/Panel/Level.text = "lv." + str(level)

@rpc("any_peer", "call_local")
func set_player_death():
	$VBoxContainer/PanelContainer/Panel/Level.text = "died"

@rpc("any_peer", "call_local")
func set_player_alive():
	$VBoxContainer/PanelContainer/Panel/Level.text = "lv." + str(lv)

@rpc("any_peer", "call_local", "unreliable")
func play_animation(anim_name : String):
		animation_player.play(anim_name)
	

func handle_player_hp_change(player_id : int, current_hp : float, hp_ratio  : float, amount : float):
#	print("play heart. hp: ", hp, "  current hp ", current_hp, " event of : ", player_id)			
	if player_id != for_player_id:
		return
	if amount < 0:
		play_animation.rpc("hurt")
#		print("play heart. hp: ", hp, "  current hp ", current_hp)		
	if amount > 0:
		play_animation.rpc("heal")
#		print("play heal. hp: ", hp, "  current hp ", current_hp)		
#	if hp > 0 && current_hp == 0:
#		set_player_death.rpc()
#	if hp == 0 && current_hp > 0:
#		set_player_alive.rpc()
	hp = current_hp
	update_player_status_hp_bar.rpc(hp_ratio)
	
	
func handle_player_level_up(player_id : int , level : int):
	if player_id != for_player_id:
		return
	play_animation.rpc("lv_up")
	set_player_level.rpc(level)
