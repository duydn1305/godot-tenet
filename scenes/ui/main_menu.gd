extends CanvasLayer

@onready var play_button = $MarginContainer/VBoxContainer/PlayButton
@onready var howtoplay_button := $MarginContainer/VBoxContainer/HowToPlayButton as Button
@export var house_scene : PackedScene

func _ready():
	play_button.connect("pressed", to_house_scene)
	
func to_house_scene():
	SceneTransition.start_transition()
	await SceneTransition.transition_halfway
	get_tree().change_scene_to_file("res://scenes/lodging/house/house.tscn")


func _on_how_to_play_button_pressed():
	SceneTransition.start_transition()
	await SceneTransition.transition_halfway
	get_tree().change_scene_to_file("res://scenes/ui/howtoplay/htp.tscn")


func _on_quit_button_pressed():
	get_tree().quit()
