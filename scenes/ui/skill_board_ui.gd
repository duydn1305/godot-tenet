extends CanvasLayer
class_name CharacterSkillBoard
var level = 10
var hp = 70
var max_hp = 100

var mp = 45
var max_mp = 80

var xp = 23
var xp_next_lv = 50

var character_name = "player.zzz"
@onready var skill_node_by_key = {
	Const.ActiveSkill.Q: $MarginContainer/VBoxContainer/SkillSlotQ,
	Const.ActiveSkill.E: $MarginContainer/VBoxContainer/SkillSlotE,
	Const.ActiveSkill.R: $MarginContainer/VBoxContainer/SkillSlotR,
	Const.ActiveSkill.Space: $MarginContainer/VBoxContainer/SkillSlotSpace,
}

func _ready():
	var character_data : BasicStats
	if Store.general.last_character_id == "2":
		character_data = preload("res://resources/characters/0_data/2.tres")
	if Store.general.last_character_id == "3":
		character_data = preload("res://resources/characters/0_data/3.tres")
	
	
	if Store.general.last_character_id == "5":
		character_data = preload("res://resources/characters/0_data/5.tres")
	
	if Store.general.last_character_id == "6":
		character_data = preload("res://resources/characters/0_data/6.tres")
	if character_data == null:
		return
	# tmp
	if character_data.skill_q != null && character_data.skill_q.icon != null:
		(skill_node_by_key[Const.ActiveSkill.Q] as UISkillSlot).update_skill_image(character_data.skill_q.icon)
	if character_data.skill_e != null && character_data.skill_e.icon != null:
		(skill_node_by_key[Const.ActiveSkill.E] as UISkillSlot).update_skill_image(character_data.skill_e.icon)
	if character_data.skill_r != null && character_data.skill_r.icon != null:
		(skill_node_by_key[Const.ActiveSkill.R] as UISkillSlot).update_skill_image(character_data.skill_r.icon)
	if character_data.icon != null:
		$MarginContainer/VBoxContainer/Board/CharacterIcon.texture = character_data.icon
	$MarginContainer/VBoxContainer/Board/CharacterName.text = character_data.object_name
	
		
func update_hp(hp_ratio):
	$MarginContainer/VBoxContainer/Board/VBoxContainer/HealthContainer/HealthBar.value = hp_ratio

func update_mp(mp_ratio):
	$MarginContainer/VBoxContainer/Board/VBoxContainer/ManaContainer/ManaBar.value = mp_ratio
	
	
func update_level(level):
	$MarginContainer/VBoxContainer/Board/CharacterLevel.text = "lv."+str(level)

func update_exp(exp_ratio):
	$MarginContainer/VBoxContainer/Board/VBoxContainer/ExpContainer/ExpBar.value = exp_ratio
	
func set_character_name(new_character_name): # knight, warior ...
	return
	character_name = new_character_name
	$MarginContainer/VBoxContainer/Board/CharacterName.text = new_character_name

func set_player_name(player_name):
	$MarginContainer/VBoxContainer/Board/PanelContainer/PlayerName.text = player_name


func update_ui_skill_cooldown(skill : Const.ActiveSkill, total_time : float, time_remain : float):
	var skill_node = skill_node_by_key[skill]  as UISkillSlot
	skill_node.update_skill_cooldown(total_time, time_remain)
	
	
# skill not enough mp to use -> make it darker
# skill is cooldown
