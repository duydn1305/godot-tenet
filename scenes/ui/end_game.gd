extends CanvasLayer
@onready var back_to_home = %BackToHome
@onready var stats_manager = $"../../StatsManager"

func _ready():
	GameEvents.end_game.connect(handle_end_game)
	
func handle_return_home():
	NetworkHelper.reset_multiplayer()
	get_tree().change_scene_to_file("res://scenes/lodging/house/house.tscn")

func handle_end_game(win : bool, msg : String):
	BgMusic.play_type(BgMusic.Type.IDLE)
	visible = true
	if win:
		$Win.play_random()
		$Firework.visible = true
		$Bg.visible = false
	else:
		$Loose.play_random()
		$Firework.visible = false
		$Bg.visible = false
	$AnimationPlayer.play("fade_in")
	await $AnimationPlayer.animation_finished
	var extra_data 
	if stats_manager != null:
		extra_data = stats_manager.my_extra_data
	$Message.text = msg
	# fill data: damage deal, level reach, coins, ...
	# store coin
	# store achievement
	if win:
		%GameStatus.text  = "You win"
	else:
		%GameStatus.text  = "You loose"
	if extra_data != null:
		if extra_data.has("quest_count"):
			%QC.text = str(extra_data['quest_count']) # the same as floor reach
		if extra_data.has("level"):
			%LR.text = str(extra_data['level'])
		if extra_data.has("enemy_killed"):
			%EC.text = str(extra_data['enemy_killed']) 
		if extra_data.has("coin"):
			%CG.text = str(int(extra_data['coin']))
			Store.general.coin += int(extra_data['coin'])
			Store.general.save()
		achievement_handle(win)
	back_to_home.pressed.connect(handle_return_home)
	

func achievement_handle(win:bool):
	if stats_manager == null:
		return
	var extra_data = stats_manager.my_extra_data
	var key = ["achiev_1","achiev_2","achiev_3","achiev_4","achiev_5","achiev_6","achiev_7","achiev_8","achiev_9","achiev_10"]
	if win :
		Store.achievements.set_value(key[9],Store.achievements.get_value(key[9]) + 1)
		
	var enemykill := Store.achievements.get_value(key[0]) + int(extra_data['enemy_killed'])
	var coingain := Store.achievements.get_value(key[1]) + int(extra_data['coin'])
	var questcount := Store.achievements.get_value(key[2]) + int(extra_data['quest_count'])
	Store.achievements.set_value(key[0],enemykill)	
	Store.achievements.set_value(key[1],enemykill)	
	Store.achievements.set_value(key[2],enemykill)	
	Store.achievements.set_value(key[3],coingain)	
	Store.achievements.set_value(key[4],coingain)	
	Store.achievements.set_value(key[5],coingain)	
	Store.achievements.set_value(key[6],questcount)	
	Store.achievements.set_value(key[7],questcount)	
	Store.achievements.set_value(key[8],questcount)	
	Store.achievements.save()
	
	
	
