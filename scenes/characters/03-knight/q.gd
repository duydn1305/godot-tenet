extends BaseActiveAbility
@onready var xoay_hitbox = $"../../XoayHitbox" as BaseHitbox
@onready var base_normal_attack = $"../BaseNormalAttack"

var base_damage : float :
	set(new_value):
		base_damage = new_value
		xoay_hitbox.damage = base_damage

func extend_ready():
	if owner.basic_data != null:
		(owner.basic_data as BasicStats).changed.connect(handle_stats_change)
		base_damage = (owner.basic_data as BasicStats).DMG

func handle_stats_change():
	base_damage = (owner.basic_data as BasicStats).DMG

# dash example
#
func perform_ability_logic():
	$"../../XoayHitbox/XoayShape2D".disabled = false
	show_aura.rpc(true)
	$Timer.start(0.5)
	await $Timer.timeout
	show_aura.rpc(false)
	$"../../XoayHitbox/XoayShape2D".disabled = true
	


@rpc("any_peer", "call_local")
func show_aura(s : bool):
	$"../../Aura6".visible = s
#	if s: GameEvents.spawn_floating_text_str("+ammor", owner.position + Vector2.RIGHT*16)
	

	
#func teleport(character : CharacterBody2D, new_position):
#	character.position = new_position
