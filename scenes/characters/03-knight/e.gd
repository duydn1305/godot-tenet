extends BaseActiveAbility
@onready var base_normal_attack = $"../BaseNormalAttack"

# dash example
#
func perform_ability_logic():
	owner.is_dashing = true
	await velocity_component.temporary_change_speed_multiplier(skill_data.SPEED_MULTIPLIER, skill_data.ACTIVATE_TIME)
	owner.is_dashing = false
	base_normal_attack.handle_ability(2)
	
	
	
#func teleport(character : CharacterBody2D, new_position):
#	character.position = new_position
