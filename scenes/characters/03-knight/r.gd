extends BaseActiveAbility

# dash example
func extend_ready():
	$"../../HolyHitbox".damage = $"../BaseNormalAttack".damage

func perform_ability_logic():
	$"../../HolyHitbox".damage = $"../BaseNormalAttack".damage	
	holy_animation.rpc()
	for n in get_tree().get_nodes_in_group("player_alive"):
		if n.has_node("HealthComponent"):
			n.get_node("HealthComponent").call("hp_change", $"../BaseNormalAttack".damage*5)
	
@rpc("any_peer", "call_local")
func holy_animation():
	$"../../HolyHitbox/HolyShape2D".disabled = false
	$Respawn.play_random()
	animate_ray_respawn()
	$Timer.start(0.1)
	await $Timer.timeout
	$"../../HolyHitbox/HolyShape2D".disabled = true
	

func animate_ray_respawn():
	
	($"../../Visuals/Sprite2D".material as ShaderMaterial).set_shader_parameter("speed", 1)
	
	$"../../Ray".visible = true
	print($"../../Ray".visible)	
	var t = create_tween() as Tween
	($"../../Ray".material as ShaderMaterial).set_shader_parameter("cutoff", 0.3)
	t.tween_property($"../../Ray".material, "shader_parameter/cutoff", 0.13, 2).set_ease(Tween.EASE_OUT).set_trans(Tween.TRANS_CUBIC)
	t.tween_interval(2)
	t.tween_callback(func(): 
		$"../../Ray".visible = false
		($"../../Visuals/Sprite2D".material as ShaderMaterial).set_shader_parameter("speed", 0)
		
	)
#func teleport(character : CharacterBody2D, new_position):
#	character.position = new_position
