extends BaseActiveAbility

@onready var range_attack = $"../RangeAttack" as RangeAttack
@onready var animation_weapon = $"../../Visuals/AnimationWeapon"

func perform_ability_logic():
	velocity_component.move_speed_multiplier -= 0.5
	var new_speed_scale = animation_weapon.get_animation('skill-q').length*range_attack.attack_speed
	owner.play_anim.rpc('skill-q', new_speed_scale)
	await animation_weapon.animation_finished
	velocity_component.move_speed_multiplier += 0.5
	
	
#func teleport(character : CharacterBody2D, new_position):
#	character.position = new_position

func shoot_3_arrow():
	if !multiplayer.is_server():
		return
	range_attack.shot_n_arrow(skill_data.BULLET_PER_SHOT, skill_data.SHOOT_ANGLE)
