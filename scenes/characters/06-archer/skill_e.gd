extends BaseActiveAbility


@export var shootable_component : ShootableComponent
@onready var range_attack = $"../RangeAttack" as RangeAttack
@onready var animation_weapon = $"../../Visuals/AnimationWeapon"

@export var fire_out_point_a : Marker2D
@export var fire_out_point_b : Marker2D

var attack_speed

func extend_ready():
	if owner.basic_data != null:
		(owner.basic_data as BasicStats).changed.connect(handle_stats_change)
		# get from base
		shootable_component.damage = (owner.basic_data as BasicStats).DMG
		shootable_component.crit_damage = (owner.basic_data as BasicStats).CRIT_DMG
		shootable_component.crit_rate = (owner.basic_data as BasicStats).CRIT_RATE
		attack_speed = (owner.basic_data as BasicStats).ATTACK_SPEED
		# get from skil data
		shootable_component.projectile_decay = (skill_data as BasicStats).BULLET_DECAY # get data from basic_data
		shootable_component.penetration = (skill_data as BasicStats).BULLET_PENETRATION # get data from basic_data
		shootable_component.bullet_speed = (skill_data as BasicStats).BULLET_SPEED # get data from basic_data
		shootable_component.penetrate_ammor_rate = (skill_data as BasicStats).PENETRATE_AMMOR_RATE # get data from basic_data
	shootable_component.object_onwer_id = owner.player_id

func handle_stats_change():
	shootable_component.damage = (owner.basic_data as BasicStats).DMG
	shootable_component.crit_damage = (owner.basic_data as BasicStats).CRIT_DMG
	shootable_component.crit_rate = (owner.basic_data as BasicStats).CRIT_RATE
	attack_speed = (owner.basic_data as BasicStats).ATTACK_SPEED

func perform_ability_logic():
	velocity_component.move_speed_multiplier -= 0.5
	# slow down
	# pick random
#	var new_speed_scale = 1.0/attack_speed/animation_player.get_animation(normal_attack_anim_names[rand_attack_anim]).length
	var new_speed_scale = animation_weapon.get_animation('skill-e').length*attack_speed
	owner.play_anim.rpc('skill-e', new_speed_scale)
	await animation_weapon.animation_finished
	velocity_component.move_speed_multiplier += 0.5
	
	
#func teleport(character : CharacterBody2D, new_position):
#	character.position = new_position

func shoot_penetrate_arrow():
	if !multiplayer.is_server():
		return
	var mv_vector = (fire_out_point_b.global_position-fire_out_point_a.global_position).normalized()
	var init_pos = owner.position
	if fire_out_point_b != null: init_pos = fire_out_point_b.global_position
	shootable_component.shoot_a_projectile(init_pos, mv_vector)
