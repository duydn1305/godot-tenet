extends BaseActiveAbility

@onready var animation_player = $"../../AnimationPlayer"
@onready var shield_aura_hitbox = $"../../ShieldAuraHitbox" as BaseHitbox
@onready var hurtbox_component = $"../../HurtboxComponent" as BaseHurtbox

var damage_interval = 1

func extend_ready():
	if owner.basic_data != null:
		if owner.basic_data.skill_e != null:
			shield_aura_hitbox.damage = (owner.basic_data.skill_e.DMG)
			$AuraTimer.timeout.connect(handle_aura_timer_timeout)

func handle_aura_timer_timeout():
	$"../../ShieldAuraHitbox/AuraHitboxShape2D".set_deferred("disabled", !$"../../ShieldAuraHitbox/AuraHitboxShape2D".disabled)

func perform_ability_logic():
	velocity_component.is_stop = true
	# get active dash time from base
	owner.play_anim.rpc("skill-e")
	await animation_player.animation_finished
	velocity_component.is_stop = false
	handle_on_active_time()
	
	
	
func handle_on_active_time():
	show_aura.rpc(true)
	hurtbox_component.ammor += 50
	# todo: increase ammor in a period of time
	$AuraTimer.start(damage_interval/2.0)
	$DuratioinTimer.start(skill_data.ACTIVATE_TIME)
	await $DuratioinTimer.timeout
	$AuraTimer.stop()
	hurtbox_component.ammor -= 50
	$"../../ShieldAuraHitbox/AuraHitboxShape2D".set_deferred("disabled", true)	
	show_aura.rpc(false)
	

@rpc("any_peer", "call_local")
func show_aura(s : bool):
	$"../ShieldAura".visible = s
	if s: GameEvents.spawn_floating_text_str("+ammor", owner.position + Vector2.RIGHT*16)
	if s:
		($"../../Visuals/Sprite2D".material as ShaderMaterial).set_shader_parameter("speed", 1)
	else:
		($"../../Visuals/Sprite2D".material as ShaderMaterial).set_shader_parameter("speed", 0)
		
	
