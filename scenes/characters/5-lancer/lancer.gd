extends CharacterBody2D

# Set by the authority, synchronized on spawn.
@export var player_id := 1 :
	set(id):
		player_id = id
		$PlayerInput.set_multiplayer_authority(id)
#		print("set id for: ", id)

@onready var velocity_component = $VelocityComponent
@onready var visuals = $Visuals
@onready var ability_control = $BaseAbilityControl
@onready var animation_player = $AnimationPlayer

var basic_data : BasicStats
# maybe use signal emit data change

func _ready():
	set_physics_process(multiplayer.is_server())

func set_player_name(name):
	$Foreground/PlayerName.text = name
	# call update ui of this player 

var is_dashing = false

func _physics_process(delta):
	var mv_vector = $PlayerInput.move_direction
	var look_at_direction = $PlayerInput.look_at_direction
	if is_dashing: velocity_component.accelebrate_in_direction(look_at_direction)
	else: velocity_component.accelebrate_in_direction(mv_vector)
	velocity_component.move(self)

	# is client and not host
	# sent mv != zero
#	if multiplayer.get_unique_id() != 1 && mv_vector != Vector2.ZERO:
#		# inspect mv vector from other client
#		if player_id != multiplayer.get_unique_id():
#			print("client ", player_id, " sent mv to ", multiplayer.get_unique_id())
	
	# walk or idle
	# animation
	
	if ability_control.current_behavior == Const.Behave.FREE:
		if mv_vector != Vector2.ZERO:
			play_anim.rpc("walk")
		else:
			play_anim.rpc("idle")
	# left/right
	if look_at_direction.x < 0:
		visuals.scale = Vector2(-1, 1)
#		left atk hitbox
	if look_at_direction.x > 0:	
		visuals.scale = Vector2(1, 1)


@rpc("any_peer", "call_local")
func play_anim(anim : String, new_speed_scale : float = 1):
	if anim == "walk" or anim == "idle":
		animation_player.play(anim)
		return
	animation_player.play(anim, -1, new_speed_scale)
	
