extends BaseActiveAbility

@onready var animation_player = $"../../AnimationPlayer"
@onready var storm_sprite = $"../Storm"
@onready var storm_hitbox = $"../../StormHitbox"
@onready var duratioin_timer = $"../Storm/DuratioinTimer"

var light_effect = preload("res://resources/shader/lighting_2d.tscn")

func extend_ready():
	if owner.basic_data != null:
		if owner.basic_data.skill_r != null:
			storm_hitbox.damage = (owner.basic_data.skill_r.DMG)

func summon_lighting():
	var l = light_effect.instantiate()
	l.global_position = owner.position
	get_tree().get_first_node_in_group("foreground_layer").add_child(l)

func perform_ability_logic():
	velocity_component.is_stop = true
	# get active dash time from base
	owner.play_anim.rpc("skill-r")
	await animation_player.animation_finished
	velocity_component.is_stop = false
#	storm_sprite.visible = false
	turn_on_shield()

func turn_on_shield():
	$"../../StormHitbox/StormHitboxShape2D".set_deferred("disabled", false)
	animate_storm.rpc(skill_data.ACTIVATE_TIME)
	duratioin_timer.start(skill_data.ACTIVATE_TIME)
	await duratioin_timer.timeout
	$"../../StormHitbox/StormHitboxShape2D".set_deferred("disabled", true)

@rpc("any_peer", "call_local")
func animate_storm(t : float):
	storm_sprite.visible = true
	var tween = create_tween() as Tween
	(storm_sprite.material as ShaderMaterial).set_shader_parameter("progress", 0.0)
	tween.tween_property(storm_sprite.material, "shader_parameter/progress", 1.0, t)
	tween.tween_callback(func(): storm_sprite.visible = false)
