extends BaseActiveAbility
@onready var dash_hitbox = $"../../DashHitbox" as BaseHitbox

var base_damage : float :
	set(new_value):
		base_damage = new_value
		dash_hitbox.damage = base_damage  * 2

func extend_ready():
	if owner.basic_data != null:
		(owner.basic_data as BasicStats).changed.connect(handle_stats_change)
		base_damage = (owner.basic_data as BasicStats).DMG

func handle_stats_change():
	base_damage = (owner.basic_data as BasicStats).DMG

# dash example
#
func perform_ability_logic():
	$"../../DashHitbox/DashHitboxShape2D".disabled = false
	owner.is_dashing = true
	await velocity_component.temporary_change_speed_multiplier(skill_data.SPEED_MULTIPLIER, skill_data.ACTIVATE_TIME)
	owner.is_dashing = false
	$"../../DashHitbox/DashHitboxShape2D".disabled = true
	
	
	
#func teleport(character : CharacterBody2D, new_position):
#	character.position = new_position
