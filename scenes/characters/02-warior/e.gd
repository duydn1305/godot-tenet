extends BaseActiveAbility

# dash example

func perform_ability_logic():
#	owner.play_anim.rpc("gong")
#	await owner.animation_player.animation_finished
	handle_inc_stats()

	
func handle_inc_stats():
	shining.rpc(1)
	GameEvents.spawn_floating_text_str.rpc("speed up", owner.position + Vector2.RIGHT*16, Color.BLUE)
	await velocity_component.temporary_change_speed_multiplier(skill_data.SPEED_MULTIPLIER, skill_data.ACTIVATE_TIME)
	shining.rpc(0)
	
@rpc("any_peer", "call_local")
func shining(speed : float):
	print("set")
	($"../../Visuals/Sprite2D".material as ShaderMaterial).set_shader_parameter("speed", speed)
