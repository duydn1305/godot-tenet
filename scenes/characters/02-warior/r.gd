extends BaseActiveAbility

# dash example
@onready var animation_player = $"../../Visuals/AnimationPlayer"

func perform_ability_logic():
	show_aura.rpc(true)
	set_gong.rpc(true)
	owner.play_anim.rpc("gong")
	$Timer2.start(1)
	await $Timer2.timeout
#	await animation_player.animation_finished
	set_gong.rpc(false)
	
	handle_inc_stats()

	
func handle_inc_stats():
	print("handle_inc_stats")
	var old_value = $"../BaseNormalAttack".damage
	$"../BaseNormalAttack".damage += 5
	$"../BaseChargeAttack".damage += 5
	$"../BaseNormalAttack".attack_speed += 0.3
	$"../BaseChargeAttack".attack_speed += 0.3
	$Timer.start(skill_data.ACTIVATE_TIME)
	await $Timer.timeout
	show_aura.rpc(false)
	if $"../BaseNormalAttack".damage -5 == old_value:
		print("giong")
		$"../BaseNormalAttack".damage -= 5
		$"../BaseChargeAttack".damage -= 5
		$"../BaseNormalAttack".attack_speed -= 0.3
		$"../BaseChargeAttack".attack_speed -= 0.3
	else:
		print("khac")
@rpc("any_peer", "call_local")
func show_aura(s : bool):
	$"../../Aura7".visible = s
	if s: GameEvents.spawn_floating_text_str("rage", owner.position + Vector2.RIGHT*16, Color.ORANGE_RED)
	
@rpc("any_peer", "call_local")
func set_gong(s):
	owner.is_gong = s
