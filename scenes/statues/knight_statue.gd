extends RigidBody2D
@onready var health_component = $HealthComponent
@onready var hurtbox_component = $HurtboxComponent
@onready var animation_player = $AnimationPlayer
@onready var timer = $Timer
@onready var impact : RandomAudio = $Impact
@onready var collapse : RandomAudio = $Collapse
var basic_data : BasicStats
# statue id : 4400 -> 4406
@export var object_id : int :
	set(new_id):
		print("set n ew")
		object_id = new_id

signal statue_collapsed()

func set_texture():
	if object_id == 4400:
		$Sprite2D.texture = preload("res://scenes/statues/knight_statue_normalmap.tres")
	if object_id == 4401:
		$Sprite2D.texture = preload("res://scenes/statues/assassin_statue_normalmap.tres")
		

func _ready():
	set_texture()
#	object_id = 4401
#	var statue_texture = preload("res://assets/statues/knight-statue.png")
#	$Sprite2D.texture = statue_texture
#	if statue_texture != null:
	if !multiplayer.is_server():
		return
	$HurtboxComponent.ammor = 100
	health_component.health_changed.connect(handle_health_changed)
	health_component.died.connect(handle_died)
	timer.timeout.connect(handle_timer_timeout)

func handle_health_changed(curernt, zz, ratio):
	play_hurt_effect.rpc()
	$Foreground/HP.value = ratio
	
	
func handle_died():
	play_sound.rpc("collapse")
	timer.stop()
	statue_collapsed.emit()
	get_tree().call_group("enemy", "change_strategy", VelocityComponent.PlayerRankType.Nearest)	
	# play collapse effect
	play_collapse_effect.rpc()
	await get_tree().create_timer(2).timeout
	GameEvents.emit_defend_objective_collapse(object_id, Const.ObjectTypes.Statue)
	
@rpc("any_peer", "call_local")	
func play_collapse_effect():	
	$Sprite2D.material = null
	$ColorRect.visible = false
	
@rpc("any_peer", "call_local")	
func play_hurt_effect():
	animation_player.play("hurt")
	play_sound.rpc("impact")

func handle_timer_timeout():
	get_tree().call_group("enemy", "change_strategy", VelocityComponent.PlayerRankType.Statue)

@rpc("any_peer", "call_local")
func play_sound(type):
	match type:
		"impact": impact.play_random()
		"collapse": collapse.play_random()
