class_name WeightedTable

var weighted_sum = 0
# {id: {weight, item}}
var dict = {}

func table_is_empty():
	return dict.is_empty()

func add_item(id, weight, item):
	dict[id] = {
		"weight": weight,
		"item": item,
	}
	weighted_sum += weight

func get_table_size():
	return dict.size()

func remove_item(id):
	dict.erase(id)
	
# exlude list id
func pick_item(exlude: PackedInt32Array = []):
	var adjust_weighted_sum = 0
	for id in dict:
		if not id in exlude:
			adjust_weighted_sum += dict[id]["weight"]
	var prob = randf_range(0, adjust_weighted_sum)
	var prefix_sum = 0
	for id in dict:
		if id in exlude:
			continue
		prefix_sum += dict[id]["weight"]
		if prefix_sum >= prob:
			return dict[id]["item"]
	print("not found")
	
