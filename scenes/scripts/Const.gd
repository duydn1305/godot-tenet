class_name  Const

# naming: use behave instead action to avoiod collision with input.action
enum Behave {FREE, PERFORMING_NORMAL_ATTACK, CHARGING, SKILL, PERFORMING_CHARGE_ATTACK} 

# skill Q, E, R and skill of worship statue
enum ActiveSkill {Q, E, R, Space}
enum AbilityType {Active, Passive}

enum AbilitySource {Instinct, Collectable, Upgrade}

#class Stats2:
#	const HP = "hp"

enum Stats {
	HP,
	AMMOR,
	MP,
	XP,
	DMG,
	COOLDOWN,
	ACTIVATE_TIME,
	AREA,
	CRIT_DMG,
	CRIT_RATE,
	MP_COST,
	EXP_MULTIPLIER,
	SPEED_MULTIPLIER,
	
	FROZEN_RATE,
	STUN_RATE,
	SET_FIRE_RATE,
	SLOW_FIRE_RATE,
	PENETRATE_AMMOR_RATE,
	
	ATTACK_SPEED,
	BULLET_SPEED,
	BULLET_PER_SHOT,
	EVADE,
	
	MOVE_SPEED,
	ACCELEBRATION,
	
	MP_REFILL,
	MP_REFILL_INTERVAL,
	BULLET_DECAY,
	BULLET_PENETRATION,
	EXP,
	COIN,
	
	# no need to store in resourse
	CURRENT_EXP,
	LEVEL,
	EXP_NEED,
}

enum ObjectTypes {Creep, Elite, MiniBoss, Boss, Player, Statue, Fountain, Shop, Projectile}

enum StatsType {Basic, LevelUp, Skill, MetaUpgrade}

enum RewardTypes {Coin, HP}


enum QuestType {COMBAT_WAVE, SURVIVAL, DEFENSE, MINI_BOSS, FINAL_BOSS}
enum EventModifier {
	NORMAL,
	MORE_HP,
	MORE_AMMOR,
	MORE_DMG,
	MORE_CRIT,
	MORE_UNIT,
	MORE_ATK_SPEED,
	MORE_MV_SPEED,
	MORE_LEVEL,
}

enum MapEnv {JUNGLE, DESERT, LAVA, ICE}
