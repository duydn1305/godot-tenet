extends BaseHitbox
class_name ProjectileComponent
#func _ready():
@export var mv_vector : Vector2
@export var MAX_SPEED : float = 200
@export var projectile_decay : float = 1
@export var delta_angle = PI/2
@export var penetration : float = 1
var basic_data : BasicStats
var tween : Tween
@onready var health_component = $HealthComponent as HealthComponent


func _ready():
	$DeathComponent.get_node("GPUParticles2D").texture = $Sprite2D.texture # tmp
	if !multiplayer.is_server():
		return
	health_component.died.connect(handle_ending)
	health_component.max_health = penetration
	health_component.current_health = penetration

func object_collide():
	($HealthComponent as HealthComponent).hp_change(-1)

# todo: rename function
func handle_ending():
	if tween.is_running(): tween.kill()
	$CollisionShape2D.set_deferred("disabled", true)
	$Sprite2D.visible = false

func fire_out(rotate_enable : bool = true): # start the bullet/projectile
	if rotate_enable: rotate(mv_vector.angle() + delta_angle)
	tween = create_tween()
	tween.tween_property(self, "position", mv_vector * MAX_SPEED * projectile_decay + position, projectile_decay)
	tween.tween_callback(handle_ending)
