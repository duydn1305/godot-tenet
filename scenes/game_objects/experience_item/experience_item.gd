extends Node2D
# this node own by host

@onready var stats_manager : StatsManager = get_tree().get_first_node_in_group("stats_manager")
@onready var collision_shape_2d = $Area2D/CollisionShape2D
@onready var sprite = $Sprite2D
@export var exp_gain = 0

func _ready():
	if !multiplayer.is_server():
		return
	$Area2D.area_entered.connect(on_area_entered)


func tween_collect(percent: float, start_position: Vector2):
	var player_node = get_tree().get_first_node_in_group("entities_layer").get_node("Players/"+str(player_collect))
	if player_node == null:
		return

	global_position = start_position.lerp(player_node.global_position, percent)
	var direction_from_start = player_node.global_position - start_position
	
	var target_rotation = direction_from_start.angle() + deg_to_rad(90)
	rotation = lerp_angle(rotation, target_rotation, 1 - exp(-2 * get_process_delta_time()))


func collect():
	#GameEvents.emit_experience_vial_collected(1)
	GameEvents.emit_player_collect_exp(player_collect, exp_gain)
	queue_free.call_deferred()

var has_collector = false
var player_collect : int

func disable_collision():
	collision_shape_2d.disabled = true

# player nearby object
func on_area_entered(other_area):
	if has_collector:
		return
	disable_collision.call_deferred()
	# get player id
	player_collect = other_area.owner.player_id
	
	var tween = create_tween()
	tween.set_parallel()
	tween.tween_method(tween_collect.bind(global_position), 0.0, 1.0, .5)\
	.set_ease(Tween.EASE_IN)\
	.set_trans(Tween.TRANS_BACK)
	tween.tween_property(sprite, "scale", Vector2.ZERO, .05).set_delay(.45)
	tween.chain()
	tween.tween_callback(collect)

