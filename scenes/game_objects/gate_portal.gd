extends Area2D

var gate_id : int
# may show special when has evenmodier
var modifier : Const.EventModifier = Const.EventModifier.NORMAL
			
var player_in_gate = {}

signal all_player_enter_gate(gate_id : int, modifier : Const.EventModifier) # pause game, load new map, may has event modifier

func _ready():
	if !multiplayer.is_server():
		return
	body_entered.connect(handle_player_enter_gate)
	body_exited.connect(handle_player_exit_gate)
	set_new_modifier(modifier)
	
func handle_player_enter_gate(body):
	player_in_gate[body.player_id] = true
	if len(player_in_gate) == len(get_tree().get_nodes_in_group("player_alive")):
		all_player_enter_gate.emit(gate_id, modifier)

func handle_player_exit_gate(body):
	player_in_gate.erase(body.player_id)

func set_new_modifier(new_value : Const.EventModifier):
	modifier = new_value
	if new_value == Const.EventModifier.NORMAL:
		$Normal.visible = true
		$HasModifier.visible = false
	else:
		$Normal.visible = false
		$HasModifier.visible = true
		$PanelContainer/Label.text = "Hard"
