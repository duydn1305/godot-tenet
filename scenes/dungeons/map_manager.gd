extends Node

class_name MapManager
@onready var map = $"../Map"

var tilemaps : Array[GameTilemap]

var current_env = Const.MapEnv.JUNGLE # jungle, dessert, lava, ice...
var passed_env = []

var jungles = [
	preload("res://resources/maps/data/1-1.tres"),
	preload("res://resources/maps/data/1-2.tres"),
	preload("res://resources/maps/data/1-3.tres"),
	preload("res://resources/maps/data/1-4.tres"),
	preload("res://resources/maps/data/1-6.tres"),
	preload("res://resources/maps/data/1-7.tres"),
	preload("res://resources/maps/data/1-8.tres"),
	preload("res://resources/maps/data/1-9.tres"),
]
var deserts = [
	preload("res://resources/maps/data/2-4.tres"),
	preload("res://resources/maps/data/2-5.tres"),
	preload("res://resources/maps/data/2-6.tres"),
	preload("res://resources/maps/data/2-7.tres"),
	preload("res://resources/maps/data/2-8.tres"),
	preload("res://resources/maps/data/2-9.tres"),
]

signal all_map_load
var map_loading_count = 0 :
	set(new_value):
		map_loading_count = new_value
		if new_value == len(multiplayer.get_peers()):
			map_loading_count = 0
			await get_tree().create_timer(0.5).timeout
			all_map_load.emit()
			
func _ready():
	$"../MapSpawner".spawned.connect(handle_client_spawn_map)
	if !multiplayer.is_server():
		return
#	clear_old_map()
#	rand_env()
#	load_map(23)

@rpc("any_peer", "call_remote")
func client_tell_host_done():
	map_loading_count += 1

func handle_client_spawn_map(node):
	print("map loadded on play8er: ", multiplayer.get_unique_id())
	client_tell_host_done.rpc_id(1)

#	
func clear_old_map():
	for i in map.get_children():
		map.remove_child(i)
		
var map_idx = 0 :
	set(new_value):
		map_idx = new_value
		if len(tilemaps) > 0:
			map_idx = new_value % len(tilemaps)
		
func load_map(quest_id : int):
	var selected_item
	if quest_id <= 12:
		current_env = Const.MapEnv.JUNGLE
		if quest_id == 6 || quest_id == 12:
			selected_item = jungles.back()
		else:
			var n = len(jungles)-1
			selected_item = jungles[(quest_id-1)%n]
	if quest_id > 12:
		current_env = Const.MapEnv.DESERT		
		if quest_id == 18 || quest_id == 24:
			selected_item = deserts.back()
		else:
			var n = len(deserts)-1
			selected_item = deserts[(quest_id-1)%n]
	map_loading_count = 0
	var tilemap = selected_item.scene.instantiate()
#	map.add_child.call_deferred(tilemap, true)
#	await get_tree().create_timer(0.5).timeout
	map.add_child(tilemap, true)
	await get_tree().create_timer(0.5).timeout

func get_spawn_defend_object_location() -> Node2D:
	return $"../Map/TileMap/DefendObjectSpawnLocation"
	

func get_spawn_enemy_location() -> Node2D:
	return $"../Map/TileMap/EnemySpawnLocation"
	
	
func get_spawn_gate_location() -> Node2D:
	return $"../Map/TileMap/GateSpawnLocation"
	
	
func get_spawn_player_location() -> Node2D:
	return $"../Map/TileMap/PlayerSpawnLocation"

func replace_position_player(id: int, player_index : int):
	var pos2D = get_spawn_player_location().get_child(player_index)
	var player_nodes = get_tree().get_nodes_in_group("player_alive")
	for n in player_nodes:
		if n.name == str(id):
			n.position = pos2D.position
	if !get_tree().has_group("player_died"):
		return
	player_nodes = get_tree().get_nodes_in_group("player_died")
	for n in player_nodes:
		if n.name == str(id):
			n.position = pos2D.position
