extends Node

@onready var character_skill_board : CharacterSkillBoard = $"../UI/CharacterSkillBoard"
@onready var gameplay_vignette = $"../UI/GameplayVignette"
@onready var end_die_screen = $"../UI/DieScreen"

@export var main_camera : Camera2D

func _ready():
	if !multiplayer.is_server():
		return
#	await $"..".all_player_ready
		
#	player_health_changed.connect(handle_player_hp_change)
#	player_collect_exp.connect(handle_player_collect_exp)
	# todo: enemie health changed, boss health change
	GameEvents.player_hp_change.connect(handle_player_hp_change)
	GameEvents.player_mp_change.connect(handle_player_mp_change)
	GameEvents.player_level_up.connect(handle_player_level_up)
	GameEvents.player_skill_cooldown.connect(handle_player_skill_cooldown)
	GameEvents.player_die.connect(handle_player_die)
	GameEvents.player_respawn.connect(handle_player_respawn)
	
	# boss
	GameEvents.boss_spawned.connect(handle_boss_spawned)
	GameEvents.boss_died.connect(handle_boss_died)
	
func handle_boss_spawned(object_id, boss_name):
	# show boss hp
	# connect  boss hp change
	pass
func handle_boss_died(object_id):
	# hide boss hp
	pass

func handle_boss_hp_change():
	# update ui
	# update by percent
	pass

func handle_player_respawn(player_id):
	set_die_screen.rpc_id(player_id, false)

func handle_player_die(player_id):
	set_die_screen.rpc_id(player_id, true)
		
@rpc("any_peer", "call_local")
func set_die_screen(_visible : bool):
	end_die_screen.visible = _visible
	main_camera.is_died = _visible

@rpc("any_peer", "call_local", "unreliable")
func update_ui_hp(hp_ratio : float):
	character_skill_board.update_hp(hp_ratio)

@rpc("any_peer", "call_local", "unreliable")
func update_ui_mp(mp_ratio : float):
	character_skill_board.update_mp(mp_ratio)
	
@rpc("any_peer", "call_local")
func update_ui_level(level : int):
	character_skill_board.update_level(level)
	
@rpc("any_peer", "call_local")
func update_ui_exp(exp_ratio : float):
	character_skill_board.update_exp(exp_ratio)
	

@rpc("any_peer", "call_local")
func update_ui_skill_board(active_by_key : Const.ActiveSkill, cooldown_seconds : float, time_remain : float):
	character_skill_board.update_ui_skill_cooldown(active_by_key, cooldown_seconds, time_remain)
	
# hp, mp, lv, name, chararcter face
@rpc("call_local", "any_peer")
func setup_ui(player_name : String, character_name : String):
	character_skill_board.set_player_name(player_name)
	character_skill_board.set_character_name(character_name)
	character_skill_board.update_hp(1)
	character_skill_board.update_mp(1)
	character_skill_board.update_exp(0)
	character_skill_board.update_level(0)

func init_ui_list_player():
	$"../UI/ListPlayer".init_list_player()

# handle by host
# call to update other player ui
func handle_player_hp_change(player_id : int, current_hp : float, hp_ratio  : float, amount : float):
	update_ui_hp.rpc_id(player_id, hp_ratio)
	if amount < 0:
		gameplay_vignette.flash.rpc_id(player_id)

# handle by host
# call to update other player ui
func handle_player_mp_change(player_id : int, current_mp : float, mp_ratio  : float):
	update_ui_mp.rpc_id(player_id, mp_ratio)

func handle_player_level_up(player_id : int, level : int):
	update_ui_level.rpc_id(player_id, level)
	
func handle_player_skill_cooldown(player_id : int, active_by_key : Const.ActiveSkill, cooldown_seconds : float, time_remain : float):
	update_ui_skill_board.rpc_id(player_id, active_by_key, cooldown_seconds, time_remain)
