extends Node
@onready var map_manager = $"../MapManager"

var character_resource = {
	2: preload("res://resources/characters/0_data/2.tres"),
	3: preload("res://resources/characters/0_data/3.tres"),
	5: preload("res://resources/characters/0_data/5.tres"),
	6: preload("res://resources/characters/0_data/6.tres"),
}

func _ready():
	print("multiplayer id: ", multiplayer.get_unique_id(), ". _ready for player spawn node")
	
	$"../PlayersSpawner".spawned.connect(handle_player_spawned)
	if !multiplayer.is_server():
		return
	
#	add_player(1, 0)
#	var player_cnt = 1
#	for id in multiplayer.get_peers():
#		add_player(id, player_cnt)
#		player_cnt += 1

	multiplayer.peer_disconnected.connect(del_player)
	

# client side
func handle_player_spawned(node):
#	print("node spawned in client: ", node.name, ". netwrok id: ", multiplayer.get_unique_id())
#	print("is this node owned by me? ", str(multiplayer.get_unique_id()) == node.name)
	# assume that this player need a litle time to load
#	await get_tree().create_timer(1 + randf()*2).timeout
	print("multiplayer id: ", multiplayer.get_unique_id(), ". node name: ", node.name)
	if str(multiplayer.get_unique_id()) == node.name:
		print("handle_player_spawned")
		LoadingScreen.player_has_loaded.rpc_id(1)
	

# host ready MainDungeon, load player, set authority
# load map, enemies spawner
# quest manager
func add_player(id: int, player_index : int):
	# tmp
	var pos2D = map_manager.get_spawn_player_location().get_child(player_index)
#	var pos2D = map_manager.get_spawn_player_location().get_child(0)
	
#	var character = lancer_scene.instantiate()
	var select_character_id = Store.player_data_by_id[id]['player_character_id']
#	print(Store.player_data_by_id[id]['meta_upgrade'])
	# todo: for loop meta -> add upgrade data to character resource
	var character_basic_data = (character_resource[select_character_id] as BasicStats).duplicate()
	character_basic_data.skill_q = character_basic_data.skill_q.duplicate()
	character_basic_data.skill_e = character_basic_data.skill_e.duplicate()
	character_basic_data.skill_r = character_basic_data.skill_r.duplicate()
#	Store.player_data_by_id[id]['meta_upgrade']['meta_23'] = 5
#	Store.player_data_by_id[id]['meta_upgrade']['meta_24'] = 5
	for meta_id in Store.player_data_by_id[id]['meta_upgrade']:
		# apply to character_basic_data
		var upgrade_quantity = Store.player_data_by_id[id]['meta_upgrade'][meta_id]
		if upgrade_quantity == 0: continue
		match meta_id:
			"meta_1": 
				character_basic_data.DMG += character_basic_data.DMG*upgrade_quantity*0.05
			"meta_2":
				character_basic_data.CRIT_DMG += upgrade_quantity*0.05 + 0.05
				character_basic_data.CRIT_RATE += upgrade_quantity*0.03
			"meta_3":
				character_basic_data.HP += upgrade_quantity*30
				character_basic_data.AMMOR += upgrade_quantity*5
			"meta_4":
				character_basic_data.EVADE += upgrade_quantity*0.05
				character_basic_data.MOVE_SPEED += upgrade_quantity*3
			"meta_5":
				character_basic_data.REDUCE_DMG_FROM_LAVA_ENEMIES += 0.1*upgrade_quantity
			"meta_6":
				character_basic_data.REDUCE_DMG_FROM_DESERT_ENEMIES += 0.1*upgrade_quantity
			"meta_7":
				character_basic_data.REDUCE_DMG_FROM_JUNGLE_ENEMIES += 0.1*upgrade_quantity
			"meta_8":
				character_basic_data.EXTRA_DMG_TO_LAVA_ENEMIES += 0.1*upgrade_quantity
			"meta_9":
				character_basic_data.EXTRA_DMG_TO_DESERT_ENEMIES += 0.1*upgrade_quantity
			"meta_10":
				character_basic_data.EXTRA_DMG_TO_JUNGLE_ENEMIES += 0.1*upgrade_quantity
			# todo: 11 - 16
			"meta_17":
				if character_basic_data.skill_q != null:
					(character_basic_data.skill_q as BasicStats).COOLDOWN *= (1-0.05*upgrade_quantity)
				if character_basic_data.skill_e != null:
					(character_basic_data.skill_e as BasicStats).COOLDOWN *= (1-0.05*upgrade_quantity)
				if character_basic_data.skill_r != null:
					(character_basic_data.skill_r as BasicStats).COOLDOWN *= (1-0.05*upgrade_quantity)
			"meta_18":
				character_basic_data.MOVE_SPEED *= (1+0.04*upgrade_quantity)
			"meta_22":
				character_basic_data.MP += upgrade_quantity*10
			"meta_23":
				character_basic_data.MP_REFILL += upgrade_quantity*2
			"meta_24":
				character_basic_data.MP_REFILL_INTERVAL *= (1-0.05*upgrade_quantity)
			"meta_25":
				if character_basic_data.skill_q != null:
					(character_basic_data.skill_q as BasicStats).MP_COST *= (1-0.03*upgrade_quantity)
				if character_basic_data.skill_e != null:
					(character_basic_data.skill_e as BasicStats).MP_COST *= (1-0.03*upgrade_quantity)
				if character_basic_data.skill_r != null:
					(character_basic_data.skill_r as BasicStats).MP_COST *= (1-0.03*upgrade_quantity)
			"meta_26":
				character_basic_data.PENETRATE_AMMOR_RATE += 0.05*upgrade_quantity
			"meta_27":
				#todo: update exp multiplier in exp manaager
				character_basic_data.EXP_MULTIPLIER += 0.05*upgrade_quantity
			"meta_28":
				#todo: update coin multiplier				
				character_basic_data.COIN_MULTIPLIER += 0.1*upgrade_quantity
			"meta_29":
				character_basic_data.ATTACK_SPEED *= (1+0.04*upgrade_quantity)
			"meta_30":
				character_basic_data.CAMERA_ZOOM_OUT += 0.04*upgrade_quantity
				var new_value = ($"../MainCamera" as Camera2D).zoom - Vector2(0.04*upgrade_quantity, 0.04*upgrade_quantity)
				$"../MainCamera".update_camera_zoom.rpc_id(id, new_value)
			"meta_31":
				character_basic_data.BULLET_PENETRATION += upgrade_quantity
				
				
	
	var character = character_basic_data.scene.instantiate()
	# todo: load by character
	# Set player id.
	character.player_id = id
	character.basic_data = character_basic_data
	
#	print("add player: ", id)
	# Randomize character position. todo: get position
	character.position = pos2D.position
	character.name = str(id)
#	character.set_player_name(str(id))
	character.set_player_name(Store.player_data_by_id[id]['player_name'])
#	print("wait 5 s")
#	await get_tree().create_timer(5).timeout
#	print("wait done, add child")
	
	#compute other metric: ability
	print(Store.player_data_by_id[id])
#	$"../UIManager".setup_ui.rpc_id(id, Store.player_data_by_id[id])
#	await get_tree().create_timer(1 + randf()*2).timeout
	$"../Entities/Players".add_child(character, true)
#	if id == 1: LoadingScreen.player_stage_loading += 1


func del_player(id: int):
	GameEvents.emit_player_disconnected(id)
	print("del player")
	if not $"../Entities/Players".has_node(str(id)):
		return
	$"../Entities/Players".get_node(str(id)).queue_free()
	

	# set up skill ui
	# setup skill
	# setup teammate icons
