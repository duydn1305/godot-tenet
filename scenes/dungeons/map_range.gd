extends Area2D

var stuck_enemy_node : Array[Node2D]
var stuck_player_node : Array[Node2D]

func _ready():
	if !multiplayer.is_server():
		return
	body_exited.connect(handle_body_exited)
	$JobInterval.timeout.connect(handle_relocation_timer_timeout)
	
func handle_body_exited(body : Node):
	if body.is_queued_for_deletion():
		return
	if body.get_parent().name == "Enemies":
		stuck_enemy_node.append(body)
	if body.get_parent().name == "Players":
		stuck_player_node.append(body)
		
func handle_relocation_timer_timeout():
	# enemies
	if len(stuck_enemy_node) > 0:
		if stuck_enemy_node[-1] != null:
#			print("relocation: ",stuck_enemy_node[-1].name)
#			stuck_enemy_node[-1].position = ($"../../MapManager" as MapManager).get_spawn_enemy_location().get_children()[0].position
			stuck_enemy_node[-1].position = ($"../../MapManager" as MapManager).get_spawn_enemy_location().get_children().pick_random().position
		stuck_enemy_node.pop_back()
	# player
	if len(stuck_player_node) > 0:
		if stuck_player_node[-1] != null:
#			print("relocation: ",stuck_enemy_node[-1].name)
#			stuck_enemy_node[-1].position = ($"../../MapManager" as MapManager).get_spawn_enemy_location().get_children()[0].position
			stuck_player_node[-1].position = ($"../../MapManager" as MapManager).get_spawn_player_location().get_children().pick_random().position
		stuck_player_node.pop_back()
