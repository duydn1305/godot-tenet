extends Node

@onready var quest_manager = $"../QuestManager" as QuestManager
@onready var map_manager = $"../MapManager" as MapManager
var gate_scene = preload("res://scenes/game_objects/dungegon_gate.tscn")
@onready var portals = $"../Entities/Portals"
@onready var mob_spawn_manager = $"../MobSpawnManager" as MobSpawnManager

# quest faield -> pause game -> button back to home
# quest completed -> clear enemies
# quest completed -> spawn gate
# gate vote -> clear entities

var pool_quests = WeightedTable.new()
var pool_modifier = WeightedTable.new()
# new quest
# done -> spawn gate
# vote new room
var game_difficulty_point = 0
# signal entering_new_map()
@export var max_room_per_map = 5 # llast room is boss
var room_count = 0 :
	set(new_count):
		room_count = new_count
		if room_count == max_room_per_map:
			room_count = 0
var total_player_alive : int = 0 :
	set(new_value):
		if new_value == 0:
			all_player_die.emit()
		total_player_alive = max(0, new_value)
		
var player_alive_id = {} # handle die + disconnect
		
signal all_player_die

var last_completed_quest_id : int
var last_selected_modifier : Const.EventModifier

func _ready():
	if !multiplayer.is_server():
		return
#	await $"..".all_player_ready
	total_player_alive = 1 + len(multiplayer.get_peers())
	player_alive_id[1] = true
	for pid in multiplayer.get_peers():
		player_alive_id[pid] = true
	# manually config pool quest
	pool_quests.add_item(Const.QuestType.COMBAT_WAVE, 10, {
		"quest_type": Const.QuestType.COMBAT_WAVE,
	})
	pool_quests.add_item(Const.QuestType.SURVIVAL, 10, {
		"quest_type": Const.QuestType.SURVIVAL,		
	})
	pool_quests.add_item(Const.QuestType.DEFENSE, 5, {
		"quest_type": Const.QuestType.DEFENSE,
		"defend_objects": [
			{"object_id": 4400, "object_type": Const.ObjectTypes.Statue},
			{"object_id": 4401, "object_type": Const.ObjectTypes.Statue},
		]
	})
	pool_quests.add_item(Const.QuestType.MINI_BOSS, 10, {
		"quest_type": Const.QuestType.MINI_BOSS,
		"boss_objects": [
			{"object_id": 701, "object_type": Const.ObjectTypes.MiniBoss}, # pig poss
			{"object_id": 700, "object_type": Const.ObjectTypes.MiniBoss}, # cube
			{"object_id": 702, "object_type": Const.ObjectTypes.MiniBoss}, # scropion
			{"object_id": 703, "object_type": Const.ObjectTypes.MiniBoss}, # manticore
		]
	})
	# manually config pool event modifier
	pool_modifier.add_item(Const.EventModifier.NORMAL, 10, {
		"event_modifier": Const.EventModifier.NORMAL
	})
	pool_modifier.add_item(Const.EventModifier.MORE_HP, 10, {
		"event_modifier": Const.EventModifier.MORE_HP
	})
	pool_modifier.add_item(Const.EventModifier.MORE_AMMOR, 10, {
		"event_modifier": Const.EventModifier.MORE_AMMOR
	})
	pool_modifier.add_item(Const.EventModifier.MORE_DMG, 10, {
		"event_modifier": Const.EventModifier.MORE_DMG
	})
	pool_modifier.add_item(Const.EventModifier.MORE_CRIT, 10, {
		"event_modifier": Const.EventModifier.MORE_CRIT
	})
	pool_modifier.add_item(Const.EventModifier.MORE_UNIT, 10, {
		"event_modifier": Const.EventModifier.MORE_UNIT
	})
	pool_modifier.add_item(Const.EventModifier.MORE_ATK_SPEED, 10, {
		"event_modifier": Const.EventModifier.MORE_ATK_SPEED
	})
	pool_modifier.add_item(Const.EventModifier.MORE_MV_SPEED, 10, {
		"event_modifier": Const.EventModifier.MORE_MV_SPEED
	})
	pool_modifier.add_item(Const.EventModifier.MORE_LEVEL, 10, {
		"event_modifier": Const.EventModifier.MORE_LEVEL
	})
	
	
	
#	create_quest(QuestType.COMBAT_WAVE)
	quest_manager.quest_failed.connect(handle_quest_failed)
	quest_manager.quest_completed.connect(handle_quest_completed)
	GameEvents.player_die.connect(handle_player_die)
	GameEvents.player_respawn.connect(handle_player_respawn)
	GameEvents.player_disconnected.connect(handle_player_disconnect)
	
func handle_player_disconnect(player_id):
	Helper.toasts.add_toast.rpc("Player disconnected!", 2)
	handle_player_die(player_id)
	
func handle_player_die(player_id):
	print(player_alive_id)
	if player_alive_id.has(player_id):
		total_player_alive -= 1
		player_alive_id.erase(player_id)
	pass
	
func handle_player_respawn(player_id):
	print(player_alive_id)
	if !player_alive_id.has(player_id):
		total_player_alive += 1
		player_alive_id[player_id] = true
	
	
func start_new_routine():
	# pick random
	# todo: + event modifier
	mob_spawn_manager.apply_difficulty_for_new_quest(last_completed_quest_id, last_selected_modifier)

#	quest_manager.create_quest(Const.QuestType.SURVIVAL)
	var new_quest_item = pool_quests.pick_item([Const.QuestType.MINI_BOSS])
	if last_completed_quest_id == 5 || last_completed_quest_id == 11 || last_completed_quest_id == 17 || last_completed_quest_id == 23:
		new_quest_item = pool_quests.dict[Const.QuestType.MINI_BOSS]["item"]
			
		
	var quest_type = new_quest_item['quest_type']
	match quest_type:
		Const.QuestType.DEFENSE:
			var rand_object = new_quest_item['defend_objects'].pick_random()
			quest_manager.create_quest(quest_type, rand_object['object_id'], rand_object['object_type'])
		Const.QuestType.SURVIVAL:
			quest_manager.create_quest(Const.QuestType.SURVIVAL)
		Const.QuestType.COMBAT_WAVE:
			quest_manager.create_quest(Const.QuestType.COMBAT_WAVE)
		Const.QuestType.MINI_BOSS:
			var rand_object
			if last_completed_quest_id == 5: # harcode
				rand_object = new_quest_item['boss_objects'][0]
			if last_completed_quest_id == 11:
				rand_object = new_quest_item['boss_objects'][1]
			if last_completed_quest_id == 17:
				rand_object = new_quest_item['boss_objects'][2]
			if last_completed_quest_id == 23:
				rand_object = new_quest_item['boss_objects'][3]
				
			quest_manager.create_quest(quest_type, rand_object['object_id'], rand_object['object_type'])
			
			
			
#	quest_manager.create_quest(Const.QuestType.DEFENSE, 4401, Const.ObjectTypes.Statue)
	
func handle_quest_failed(quest_id):
	print("you loose")
	show_end_game_screen.rpc(false, "You failed the quest! Try again!")
	
@rpc("any_peer", "call_local")
func show_end_game_screen(status : bool, msg : String = ""):
	get_tree().paused = true
	$"../UIManager".set_die_screen(false)
	GameEvents.emit_end_game(status, msg)
	


func handle_quest_completed(quest_id, t, c):
	last_completed_quest_id = quest_id
	clean_enemies()
	await get_tree().create_timer(2).timeout
	# todo: change map when entering quest 13
	if quest_id == 24: # last quest
		show_end_game_screen.rpc(true, "You have completed all quest! Congratulation!")
		return
	spawn_gate()
	heal_hp_for_alive_player(50)
	
func heal_hp_for_alive_player(hp : float):
	for n in get_tree().get_nodes_in_group("player_alive"):
		if n.has_node("HealthComponent"):
			n.get_node("HealthComponent").call("hp_change", 50)
	
func clear_gate():
	for i in portals.get_children():
		$"../Entities/Portals".remove_child.call_deferred(i)

func spawn_gate():
	# clear old children
	clear_gate()
	for i in map_manager.get_spawn_gate_location().get_children():
		var gate = gate_scene.instantiate() as Node2D
		gate.position = (i as Node2D).position
		gate.all_player_enter_gate.connect(proceed_to_next_room)
		gate.set_new_modifier(pool_modifier.pick_item()['event_modifier'])
		portals.add_child.call_deferred(gate, true)
		
	# asusme spawned
func proceed_to_next_room(gate_id, modifier):
	last_selected_modifier = modifier
	for i in portals.get_children():
		i.all_player_enter_gate.disconnect(proceed_to_next_room)
		i.visible = false
	# wait for a little time -> show effect ??
	pause_current_game.rpc()
	LoadingScreen.show_screen.rpc()
	
#	await get_tree().create_timer(1).timeout
	# clear all gates
	clear_gate()
	clean_statue()
	handle_new_map_loaded()
	# todo, discountnect
	
@rpc("any_peer", "call_local")
func pause_current_game():
	get_tree().paused = true

@rpc("any_peer", "call_local")
func unpaused_all_player():
	# call ui to hide team status (ready)
	SceneTransition.start_transition()
	await SceneTransition.transition_allway
	get_tree().paused = false
	LoadingScreen.hide_screen()

# (5 floor -> 1 floor danh boss ) x 4
func handle_new_map_loaded():
	map_manager.clear_old_map()
	map_manager.load_map(last_completed_quest_id+1)
	await map_manager.all_map_load
	map_manager.replace_position_player(1, 0)
	var player_index = 1
	for peer_id in multiplayer.get_peers():
		map_manager.replace_position_player(peer_id, player_index)
	respawn_died_player()
	unpaused_all_player.rpc()
	start_new_routine()

func respawn_died_player():
	if !get_tree().has_group("player_died"):
		return
	var names : Array[String]
	for obj in get_tree().get_nodes_in_group("player_died"):
		names.append(obj.name)
	for peer_id in multiplayer.get_peers():
		if names.has(str(peer_id)):
			print("hasass")
			GameEvents.emit_player_respawn.rpc(peer_id)
	if names.has(str(1)):	
		GameEvents.emit_player_respawn.rpc(1)
	

func clean_enemies():
	# may pause game -> loading screen
	# clean entities
	# clean enemies
	# clean statue
	# clean shopper
	for item in $"../Entities/Enemies".get_children():
		item.queue_free.call_deferred()
	
	for item in $"../Entities/Statues".get_children():
		item.queue_free.call_deferred()
		
func clean_statue():
	for item in $"../Entities/Statues".get_children():
		item.queue_free.call_deferred()
	
	
