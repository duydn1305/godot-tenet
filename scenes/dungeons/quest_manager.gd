extends Node

class_name QuestManager

@onready var mob_spawn_manager = $"../MobSpawnManager" as MobSpawnManager
@onready var list_quest = $"../UI/ListQuest"
@onready var game_manager = $"../GameManager"

# can be number enemies killed, boss killed, time remain
# type = combat, boss, defend (foundtain, shopping, statue), peaceful
# base on: character level, dungeon depth, event modifier


signal quest_failed(id : int)
signal quest_completed(id : int, quest_type : Const.QuestType, coin_reward : int)
signal quest_new_progress(id : int, new_description : String, progress : float)
# todo: preload scene of defences quest: shopping + statue
# todo: when quest complete -> change new map + init object: mobs, boss, shopping
# todo: when done -> spawn three door, if next room is boos, spawn only once
# todo: voting 
# todo: enemies stast base on difficulty 

var format_combat_wave = "Kill all monsters in dungeon (%d/%d)"
var format_combat_survival = "Survive in %d seconds"
var format_combat_defence = "Protect the %s from the enemies" # statue, fountain, shopper
var format_combat_mini_boss = "Defeat the boss of current map" # statue, fountain, shopper
# sub target
var format_sub_target_kill_streaks =  "%d  Kill streaks (reset after 5s if not kill any enemy)"
var format_sub_target_finish_time =  "Finish in %ds"
var format_sub_target_hp_lost =  "Damage taken < %d"

var current_quest_descrpition
var sub_target
var quest_type : Const.QuestType
var goal = 0
var progress_percent = 0.0
var reward = 0
var quest_count = 0

var current_progress_count = 0 :
	set(new_count):
		current_progress_count = new_count
		if goal == 0: 
			progress_percent = 0
		else:
			progress_percent = max(0, 1.0*current_progress_count/goal)
			if current_progress_count == goal:
				handle_quest_completed(quest_count)
			
func get_reward_base_on_quest(type : Const.QuestType):
	var new_reward = randi()%70 + 50 # 50 - 120c
	# check special round
	if type == Const.QuestType.MINI_BOSS:
		new_reward += 100
	# increase coin by quest count
	var extra_ratio_by_quest = 5.0/24.0*float(quest_count)
	return int(new_reward + new_reward*extra_ratio_by_quest)
			
func create_quest(type : Const.QuestType, object_id : int = 0, object_type : Const.ObjectTypes = Const.ObjectTypes.Creep):
#	mob_spawn_manager.spawn_boss(1)
	quest_count += 1
	list_quest.clear_old_quest()
	quest_type = type
	current_progress_count = 0
	# todo: set quest in ui	
	match type:
		Const.QuestType.COMBAT_WAVE:
			var n_wave = 6 # todo: set to 6 wave as 6 enemies typpe
			var n_enemies_per_wave = 5 # todo: more enemy base on difficulty
			var time_per_wave = 6
			goal = n_wave * n_enemies_per_wave
			current_quest_descrpition = format_combat_wave % [0, goal]
			reward = get_reward_base_on_quest(type) # 50 - 120c
			list_quest.append_new_quest({
				"quest_id": quest_count,
				"description": current_quest_descrpition,
				"reward": reward,
				"is_complete": false,
			})
			$Ticker.start()
			for i in n_wave:
				await $Ticker.timeout
				# treat wave index as enemies index in ENV
				await mob_spawn_manager.spawn_mobs(i, n_enemies_per_wave)
			# wave increase, more difficult enemies
		Const.QuestType.SURVIVAL:
			# todo: check if no player alive -> quest faield
			var survive_time = 90 # 1p30s
			goal = survive_time
			current_quest_descrpition = format_combat_survival % goal
			reward = get_reward_base_on_quest(type) # 50 - 120c
			list_quest.append_new_quest({
				"quest_id": quest_count,
				"description": current_quest_descrpition,
				"reward": reward,
				"is_complete": false,
			})
			var stop_spawn_time = 60
			# spawan 6 wave in 60s 
			var time_per_wave = 10
			var n_enemies_per_wave = 5 # tmp
			$Ticker.start()
			for i in survive_time:
				await $Ticker.timeout
				stop_spawn_time -= 1
				# check if enemies is all died
				if stop_spawn_time < 0:
					if $"../Entities/Enemies".get_child_count() == 0:
						current_progress_count = goal
						current_quest_descrpition = format_combat_survival % (goal-current_progress_count)						
						quest_new_progress.emit(quest_count, current_quest_descrpition, progress_percent)
						break
				# enemy exist
				current_progress_count += 1
				current_quest_descrpition = format_combat_survival % (goal-current_progress_count)
				quest_new_progress.emit(quest_count, current_quest_descrpition, progress_percent)	
				
				# 0s spawn enemy 0, 10s spawn enemy 1, ...
				if i%time_per_wave == 0 && stop_spawn_time >= 0:
					mob_spawn_manager.spawn_mobs(int(i/time_per_wave), n_enemies_per_wave)
		Const.QuestType.MINI_BOSS:
			# pick randomb boss
			mob_spawn_manager.spawn_boss(object_id)
			goal = 1
			current_quest_descrpition = format_combat_mini_boss
			reward = get_reward_base_on_quest(type) # 50 - 120c
			list_quest.append_new_quest({
				"quest_id": quest_count,
				"description": current_quest_descrpition,
				"reward": reward,
				"is_complete": false,
			})
			# wave increase, more difficult enemies
			# init boss
			# update ui boss hp + boss name
			# handle boss hp phase
			# boss die -> complete
		Const.QuestType.DEFENSE:
			# spawn statue/shop/foutain: random
			mob_spawn_manager.spawn_defend_object(object_id, object_type)
			var survive_time = 90 # 1p30s
			goal = 1 # defend ok or not
			if object_type == Const.ObjectTypes.Statue:
				current_quest_descrpition = format_combat_defence % "statue"
			if object_type == Const.ObjectTypes.Shop:
				current_quest_descrpition = format_combat_defence % "merchant"
			if object_type == Const.ObjectTypes.Shop:
				current_quest_descrpition = format_combat_defence % "fountain"
				
			reward = get_reward_base_on_quest(type) # 50 - 120c
			list_quest.append_new_quest({
				"quest_id": quest_count,
				"description": current_quest_descrpition,
				"reward": reward,
				"is_complete": false,
			})
			var stop_spawn_time = 60
			# spawan 6 wave in 60s 
			var time_per_wave = 10
			var n_enemies_per_wave = 4 # tmp
			var enemy_all_died = false
			$Ticker.start()
			for i in survive_time:
				await $Ticker.timeout
				stop_spawn_time -= 1
				# todo: update ui progress
				# 0s spawn enemy 0, 10s spawn enemy 1, ...				
				if i%time_per_wave == 0 && stop_spawn_time >= 0:
					mob_spawn_manager.spawn_mobs(int(i/time_per_wave), n_enemies_per_wave)
				if stop_spawn_time < 0:
					if $"../Entities/Enemies".get_child_count() == 0:
						current_progress_count = goal
						quest_new_progress.emit(quest_count, current_quest_descrpition, progress_percent)
						break
				if i+1 == survive_time:
					current_progress_count += 1
					quest_new_progress.emit(quest_count, current_quest_descrpition, progress_percent)		
#					quest_failed.emit(quest_count)
			# survived when timeout
			

func _ready():
	if !multiplayer.is_server():
		return
#	await $"..".all_player_ready
	# todo: random event
	GameEvents.player_killed_enemies.connect(handle_player_killed_enemies)
#	create_quest(Const.QuestType.COMBAT_WAVE)
	game_manager.all_player_die.connect(handle_all_player_die)
	GameEvents.defend_objective_collapse.connect(handle_defend_objective_collapse)
	GameEvents.boss_died.connect(handle_boss_kill)

func handle_defend_objective_collapse(object_id, object_type):
	if object_type == Const.ObjectTypes.Statue:
		quest_failed.emit(quest_count)

func handle_all_player_die():
	quest_failed.emit(quest_count)
	# todo: show UI
	
	
func handle_defend_object_collapse():
	quest_failed.emit(quest_count)	
	# todo: show UI

func handle_player_killed_enemies(player_id : int, enemes_id : int, coin : int):
	if quest_type == Const.QuestType.COMBAT_WAVE:
		current_progress_count += 1
		current_quest_descrpition = format_combat_wave % [current_progress_count, goal]
		quest_new_progress.emit(quest_count, current_quest_descrpition, progress_percent)

func handle_boss_kill(boss_id):
	current_progress_count += 1
	quest_new_progress.emit(quest_count, current_quest_descrpition, progress_percent)
	

func  handle_quest_completed(quest_id):
	quest_completed.emit(quest_id, quest_type, reward)
	Helper.toasts.add_toast.rpc("Quest completed, find a gate to travel. All members should be in the same gate!", 5)
