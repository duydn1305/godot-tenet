extends Node
class_name StatsManager
# this node hold data player
# tmp
var player_stats_by_id = {}
var my_stats = {}
# tmp
var raw_stats : BasicStats

var my_extra_data = {
	'level': 0,
	'quest_count': 0,
	'coin': 0.0,
	'enemy_killed': 0,
	'boss_killed': {} # store id of boss, count for all member
} # enemy kill, coin, bla bla
var extra_data_by_player_id = {}

func _ready():
	raw_stats = $"../PlayerSpawnManager".character_resource[int(Store.general.last_character_id)]
	if !multiplayer.is_server():
		return
	GameEvents.player_level_up.connect(handle_player_level_up)
	await get_tree().create_timer(2).timeout # tmp
	var p1 = find_player_node(1)
	if p1 != null:
#		print("found player node")
		player_stats_by_id[1] = p1.basic_data
		my_stats = p1.basic_data.to_dict()
		
	for player_id in multiplayer.get_peers():
		var p = find_player_node(player_id)
		if p != null:
#			print("found player node")
			player_stats_by_id[player_id] = p.basic_data
			update_player_stats.rpc_id(player_id, p.basic_data.to_dict())
			
	# extra data
	extra_data_by_player_id[1] = my_extra_data
	for player_id in multiplayer.get_peers():
		extra_data_by_player_id[player_id] = my_extra_data.duplicate(true)
	GameEvents.player_killed_enemies.connect(handle_player_killed_enemies)
	GameEvents.boss_died.connect(handle_boss_died)
	$Timer.timeout.connect(on_timeout)
	$"../QuestManager".quest_completed.connect(on_quest_completed)

func on_quest_completed(id, quest_type : Const.QuestType, coin_reward : int):
	extra_data_by_player_id[1]['quest_count'] += 1
	extra_data_by_player_id[1]['coin'] += coin_reward
	for player_id in multiplayer.get_peers():
		extra_data_by_player_id[player_id]['quest_count'] += 1
		extra_data_by_player_id[player_id]['coin'] += coin_reward
	# todo: count quest by type


func on_timeout():
	my_extra_data = extra_data_by_player_id[1]
	for player_id in multiplayer.get_peers():
		sync_extra_data.rpc_id(player_id, extra_data_by_player_id[player_id])
	
@rpc("any_peer", "call_local")
func sync_extra_data(data : Dictionary):
		my_extra_data = data

func find_player_node(player_id : int):
	var player_node : Node
	for node in get_tree().get_nodes_in_group("player_alive"):
		if node.name == str(player_id): player_node = node
	return player_node

func handle_player_level_up(player_id : int, current_level):
	var player_node = find_player_node(player_id)
	# player death
	if player_node == null:
		return
	# update source data in gameplay
	var basic_data = (player_node.basic_data as BasicStats)
	if basic_data.level_up == null:
		return
	player_node.basic_data.DMG += basic_data.level_up.DMG
	player_node.basic_data.HP += basic_data.level_up.HP
	player_node.basic_data.MP += basic_data.level_up.MP
	player_node.basic_data.MOVE_SPEED += basic_data.level_up.MOVE_SPEED
	player_node.basic_data.ATTACK_SPEED += basic_data.level_up.ATTACK_SPEED
	player_node.basic_data.AMMOR += basic_data.level_up.AMMOR
	player_node.basic_data.CRIT_RATE += basic_data.level_up.CRIT_RATE
	player_node.basic_data.CRIT_DMG += basic_data.level_up.CRIT_DMG
	(player_node.basic_data as BasicStats).emit_changed()
	extra_data_by_player_id[player_id]['level'] = current_level
	update_player_stats.rpc_id(player_id, player_node.basic_data.to_dict())
#	max_health += (owner.basic_data as BasicStats).level_up.HP
#	hp_change(max_health*0.1)

@rpc("any_peer", "call_local", "reliable")
func update_player_stats(dict : Dictionary):
	my_stats = dict
#	if len(extra_data) > 0:
#		if !my_stats.has('extra_data'): my_stats['extra_data'] = {}
#		for id in extra_data:
#			my_stats['extra_data'][id] = extra_data
	# todo: load raw
#	print("my_stats: ", my_stats)

func handle_player_killed_enemies(killer_player_id : int, enemes_id : int, c : int):
	extra_data_by_player_id[killer_player_id]['enemy_killed'] += 1
	# other player get half
	var coin = float(c)/2.0
	# increase half for all player
	extra_data_by_player_id[1]['coin'] += coin
	for player_id in multiplayer.get_peers():
		extra_data_by_player_id[player_id]['coin'] += coin
	# increasse half for the killer
	extra_data_by_player_id[killer_player_id]['coin'] += coin
	
func handle_boss_died(boss_id : int):
	extra_data_by_player_id[1]['boss_killed'][boss_id] = 1
	for player_id in multiplayer.get_peers():
		extra_data_by_player_id[player_id]['boss_killed'][boss_id] = 1
	
