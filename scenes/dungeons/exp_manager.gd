extends Node
@onready var ui_manager = $"../UIManager"


var player_exp = {
	1: {
		Const.Stats.CURRENT_EXP: 0,
		Const.Stats.LEVEL: 0,
		Const.Stats.EXP_NEED: 10,
		Const.Stats.EXP_MULTIPLIER: 1,
	}
}

func _ready():
	if !multiplayer.is_server():
		return
	GameEvents.player_collect_exp.connect(handle_player_collect_exp)
	for id in multiplayer.get_peers():
		player_exp[id] = {
			Const.Stats.CURRENT_EXP: 0,
			Const.Stats.LEVEL: 0,
			Const.Stats.EXP_NEED: 10,
			Const.Stats.EXP_MULTIPLIER: 1,
		}

# rule: player recieve 100% value of item, other player + 50%
func handle_player_collect_exp(player_id : int, amount: float):
	if player_id == 1:
		gain_exp(1, amount)
		for other_player in multiplayer.get_peers():
			gain_exp(other_player, amount/2.0)
		return
	gain_exp(1, amount/2.0)	
	for other_player in multiplayer.get_peers():
		if player_id == other_player:
			gain_exp(other_player, amount)
		else: 
			gain_exp(other_player, amount/2.0)
	
# todo: handle multi level up
func gain_exp(player_id : int, amount: float):
	# handle death player
	var alive = false
	for player_alive in get_tree().get_nodes_in_group("player_alive"):
		if str(player_id) == player_alive.name: alive = true
	if !alive: return
	# handle inc exp and level	
	var exp_need = player_exp[player_id][Const.Stats.EXP_NEED]
	var exp_multiplier = player_exp[player_id][Const.Stats.EXP_MULTIPLIER]
	player_exp[player_id][Const.Stats.CURRENT_EXP] += amount*exp_multiplier
	if player_exp[player_id][Const.Stats.CURRENT_EXP] >= exp_need:
		player_exp[player_id][Const.Stats.CURRENT_EXP] -= exp_need
		player_has_level_up(player_id)
		player_exp[player_id][Const.Stats.EXP_NEED] = get_exp_need_for_level(player_id, "lancer")
	var new_exp_ratio = 1.0*player_exp[player_id][Const.Stats.CURRENT_EXP]/player_exp[player_id][Const.Stats.EXP_NEED]
	# update ui
#	print("player_id: ", player_id, " .amt: ",amount, " rat: ", new_exp_ratio)
	
	ui_manager.update_ui_exp.rpc_id(player_id, new_exp_ratio)
	
	
func player_has_level_up(player_id : int):
	player_exp[player_id][Const.Stats.LEVEL] += 1
	var new_level = player_exp[player_id][Const.Stats.LEVEL]
	GameEvents.emit_player_level_up(player_id, new_level)
	
	
func get_exp_need_for_level(player_id, which_character) -> float:
	var which_level = player_exp[player_id][Const.Stats.LEVEL]
	var exp_need = 10 + which_level*10
	return exp_need
	
# rule: player recieve 100% value of item, other player + 50%
func handle_player_gain_coin(player_id : int, amount: float):
	# todo: show coin on ui
	pass
