extends Node2D

# this file handle game loading logic for each player

func _enter_tree():
	BgMusic.play_type(BgMusic.Type.COMBAT)
	pass
#	print("get_tree().paused: ", get_tree().paused)
#	get_tree().paused = true
#	LoadingScreen.show_screen()
#	for i in $Map/TileMap.get_children():
#		$Map/TileMap.remove_child(i)
	
func handle_server_disconnected():
	multiplayer.multiplayer_peer = null
	Helper.toasts.add_toast("Host quit the game, return to house", 5)
	SceneTransition.start_transition()
	get_tree().change_scene_to_file("res://scenes/lodging/house/house.tscn")
	
	
func _ready():
	multiplayer.server_disconnected.connect(handle_server_disconnected)
	# use case: wait for all client loaded from multipler spawn
	# We only need to spawn players on the server.
	if not multiplayer.is_server():
		return
	if has_node("/root/Multiplayer") && get_node("/root/Multiplayer") != null:
		print("wait all_player_spawned_dungeon")
		await get_node("/root/Multiplayer").all_player_spawned_dungeon
		print("done all_player_spawned_dungeon")
	
	# no one can join as soon as host enter game
#	multiplayer.peer_connected.connect(add_player)
#	multiplayer.peer_disconnected.connect(del_player)
	
	# 1. map
	$MapManager.load_map(1)
#	await get_tree().create_timer(3).timeout
	# 2. player
	$PlayerSpawnManager.add_player(1, 0)
	var player_cnt = 1
	for id in multiplayer.get_peers():
		$PlayerSpawnManager.add_player(id, player_cnt)
		player_cnt += 1
	
	$GameManager.start_new_routine()
	# 3. UI
	$UIManager.init_ui_list_player()
	for player_id in Store.player_data_by_id:
		var player_name = Store.player_data_by_id[player_id]['player_name']
		var character_name = Store.player_data_by_id[player_id]['player_character_name']
		$UIManager.setup_ui.rpc_id(player_id, player_name, character_name)
#	owner.unpaused_all_player.rpc()
#	await LoadingScreen.all_player_stage_ready
#	all_player_ready.emit() # for child of dung
	unpaused_all_player.rpc()
	

# host out, disconnect player, return to home
#func _exit_tree():
#	if not multiplayer.is_server():
#		return
#	multiplayer.peer_connected.disconnect(add_player)
#	multiplayer.peer_disconnected.disconnect(del_player)

#
#

@rpc("any_peer", "call_local")
func unpaused_all_player():
	# call ui to hide team status (ready)
	SceneTransition.start_transition()
	await SceneTransition.transition_halfway
	get_tree().paused = false
	LoadingScreen.hide_screen()
