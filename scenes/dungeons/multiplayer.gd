extends Node

@onready var host_btn = $UI/Panel/VBoxContainer/HostBtn
@onready var join_btn = $UI/Panel/VBoxContainer/JoinBtn

# todo: get data of player
# remain data of player in current game play -> store in game states ??

signal all_player_spawned_dungeon

func _ready():
	LoadingScreen.reset_player_loading_status()
	# Start paused
	get_tree().paused = true
	# You can save bandwith by disabling server relay and peer notifications.
	multiplayer.server_relay = false

	host_btn.connect("pressed", _on_host_pressed)
	join_btn.connect("pressed", _on_connect_pressed)
	$UI/Team/StartGame.connect("pressed", host_start_game)
	$MultiplayerSpawner.connect("spawned", handle_multiplayer_spawned)
#	$MultiplayerSpawner.set_spawn_function(extra_spawn)

@rpc("any_peer", "call_local")
func extra_spawn(data):
	print("call by custom spawn function, on netwrok id: ", multiplayer.get_unique_id())
	print(data)

			
# tmp
func handle_multiplayer_spawned(node : Node):
	print(node.name, " network id: ", multiplayer.get_unique_id())
	LoadingScreen.player_has_loaded.rpc_id(1)

	
func _on_host_pressed():
	host_btn.disabled = true
	join_btn.disabled = true
	
	# Start as server
	var peer = ENetMultiplayerPeer.new()
	var PORT = $UI/Panel/VBoxContainer/Port.text as int
	var player_name = $UI/Panel/VBoxContainer/Name.text
	var character_name = $UI/Panel/VBoxContainer/Character.text

#	GameEvents.player_data["player_name"] = player_name
#	Store.general.player_name = player_name
#	Store.general.last_character_id = character_name # tmp
	Store.player_data["player_name"] = player_name
	Store.player_data["player_character_id"] = int(Store.general.last_character_id)
	Store.player_data["player_character_name"] = character_name

	peer.create_server(PORT)
	if peer.get_connection_status() == MultiplayerPeer.CONNECTION_DISCONNECTED:
		OS.alert("Failed to start multiplayer server")
		return
	multiplayer.multiplayer_peer = peer
	$UI/Team/StartGame.disabled = false
	
	
	multiplayer.peer_connected.connect(handle_player_join)
	multiplayer.peer_disconnected.connect(handle_player_leave)
	print(multiplayer.get_unique_id())
	
	var data := PlayerBaseData.new()
	data.id = multiplayer.get_unique_id()
	data.player_name = player_name
	data.character_name = character_name
	Store.players.append(data)
	


func _on_connect_pressed():
	host_btn.disabled = true
	join_btn.disabled = true
	# Start as client
	var txt : String = $UI/Panel/VBoxContainer/IP.text
	var PORT = $UI/Panel/VBoxContainer/Port.text as int
	var player_name = $UI/Panel/VBoxContainer/Name.text
	var character_name = $UI/Panel/VBoxContainer/Character.text
#	GameEvents.player_data["player_name"] = player_name
#	Store.general.player_name = player_name
#	Store.general.last_character_id = character_name # tmp
	Store.player_data["player_name"] = player_name
	Store.player_data["player_character_id"] = int(Store.general.last_character_id)
	Store.player_data["player_character_name"] = character_name
	
	if txt == "":
		OS.alert("Need a remote to connect to.")
		return
	var peer = ENetMultiplayerPeer.new()
	peer.create_client(txt, PORT)
	if peer.get_connection_status() == MultiplayerPeer.CONNECTION_DISCONNECTED:
		OS.alert("Failed to start multiplayer client")
		return
	multiplayer.multiplayer_peer = peer
	$UI/Team/StartGame.visible = false
	# send general data to host
#	player_send_data.rpc_id(1, GameEvents.player_data) # meta, character, ...

	var data := PlayerBaseData.new()
	data.id = multiplayer.get_unique_id()
	data.player_name = player_name
	data.character_name = character_name
	Store.players.append(data)
	

# host press enter game
func host_start_game():
	# start game
	$UI/Team/StartGame.disabled = true
	LoadingScreen.update_player_name()
	print(LoadingScreen.player_loading_status)
	print(Store.players)
	LoadingScreen.refresh_player_list.rpc(LoadingScreen.player_loading_status)
	# get data stage
	LoadingScreen.new_loading_stage()
	game_going_to_start.rpc()
	
	# queue free
	await LoadingScreen.all_player_stage_ready
	
	# create node stage
	LoadingScreen.new_loading_stage()
	entering_game()
	LoadingScreen.player_stage_loading += 1	# host done
	await LoadingScreen.all_player_stage_ready
	all_player_spawned_dungeon.emit()

@rpc("any_peer", "call_local")
func game_going_to_start():
	$UI.visible = false
	LoadingScreen.show()
	$UI.queue_free()
#	LoadingScreen.visible = true
	# send neccesary data to host: general + meta data
	print("multiplayerId: ", multiplayer.get_unique_id(), ". data: ", Store.player_data)
	player_send_data.rpc_id(1, PlayerBaseData.get_by_id(Store.players, multiplayer.get_unique_id()).to_dict(), Store.player_data) # meta, character, ...
	

# host handle
# player send host neccessary data
@rpc("any_peer", "call_local")
func player_send_data(sender_data : Dictionary, extra_data : Dictionary = {}):
	var player_data = PlayerBaseData.from_dict(sender_data)
	var sender_id = multiplayer.get_remote_sender_id()
	Store.players.append(player_data)
	Store.player_data_by_id[sender_id] = extra_data
#	GameEvents.player_data_by_id[sender_id] = sender_data
#	print("player id: ", sender_data)
	# assume there is a little processing time between 1s -> 10s
#	await get_tree().create_timer(5+randf()*2).timeout
#	player_list_id[sender_id] = true # mark as this player send data done
	LoadingScreen.player_loading_status[sender_id]["ready"] = true
	LoadingScreen.player_loading_status[sender_id]["player_name"] = player_data.player_name
	LoadingScreen.player_stage_loading += 1
	LoadingScreen.refresh_player_list.rpc(LoadingScreen.player_loading_status)
	

	
# show loading screen
func entering_game():
#	LoadingScreen.visible = true
	# Only change level on the server.
	# Clients will instantiate the level via the spawner.
	if multiplayer.is_server():
		# entering dungeon level 0 (room 0)
		load_dungeon(load("res://scenes/dungeons/main_dungeon.tscn"))

# entering new room

# load world
# file Main: contain map, player, UI, ...
func load_dungeon(dungeon_scene : PackedScene):
	# 1. clear old  in dungeon
	var level = dungeon_scene.instantiate()
	$Dungeon.add_child(level)
#	await get_tree().create_timer(1+randf()*2).timeout
	
	
	

#var player_list_id = {
#	1: false
#}
	
#func open_member_table():
#	$UI/Panel.visible = false
#	$UI/Team.visible = true
#	refresh_player_list(player_list_id)
# host control
func handle_player_join(id : int):
	LoadingScreen.player_loading_status[id] = {
		"ready": false,
		"player_name": str(id),
	}

func handle_player_leave(id : int):
	LoadingScreen.player_loading_status.erase(id)

#@rpc("any_peer", "call_local")
#func refresh_player_list(player_list : Dictionary):
#	for node in $UI/Team/TeamInfo.get_children():
#		$UI/Team/TeamInfo.remove_child(node)
#	for player in player_list:
#		var label_node = Label.new()
#		label_node.text = str(player)
#		if player_list[player]:
#			label_node.text = str(player) + " - ready"
#		$UI/Team/TeamInfo.add_child(label_node)
#
