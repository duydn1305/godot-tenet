extends Node

class_name MobSpawnManager

var enemy_data_dupplicate_source = {
	
}

var enemy_data = {
	1000: preload("res://resources/characters/1000_creep_data/1000.tres"),
	1001: preload("res://resources/characters/1000_creep_data/1001.tres"),
	1002: preload("res://resources/characters/1000_creep_data/1002.tres"),

	1003: preload("res://resources/characters/1000_creep_data/1003.tres"),
	1004: preload("res://resources/characters/1000_creep_data/1004.tres"),
	1005: preload("res://resources/characters/1000_creep_data/1005.tres"),
	
	1006: preload("res://resources/characters/1000_creep_data/1006.tres"),
	1007: preload("res://resources/characters/1000_creep_data/1007.tres"),
	1008: preload("res://resources/characters/1000_creep_data/1008.tres"),
	1009: preload("res://resources/characters/1000_creep_data/1009.tres"),
	1010: preload("res://resources/characters/1000_creep_data/1010.tres"),
	1011: preload("res://resources/characters/1000_creep_data/1011.tres"),
}

var statue_data = {
	4400: preload("res://resources/characters/4400_statues/4400.tres"),
	4401: preload("res://resources/characters/4400_statues/4401.tres"),
}
var boss_data = {
	700: preload("res://resources/characters/700_mini_boss/700.tres"),
	701: preload("res://resources/characters/700_mini_boss/701.tres"),
	702: preload("res://resources/characters/700_mini_boss/702.tres"),
	703: preload("res://resources/characters/700_mini_boss/703.tres"),
}

@onready var map_manager = $"../MapManager" as MapManager

func _ready():
	if !multiplayer.is_server():
		return
		
func get_mob_id_from_env(idx, env : Const.MapEnv):
	if env == Const.MapEnv.JUNGLE:
		return 1000+idx
	# todo, add more enemies for map jungle
	if env == Const.MapEnv.DESERT:
		return 1006+idx
	
# mob_env_index: 0 to 5, base on env
func spawn_mobs(mob_env_index : int, n: int, interval : float = 1):
	for i in n:
		var data = (enemy_data_dupplicate_source[get_mob_id_from_env(mob_env_index, map_manager.current_env)] as BasicStats)
		var enemy = data.scene.instantiate()
		enemy.basic_data = data
		
		enemy.position = map_manager.get_spawn_enemy_location().get_children().pick_random().position
#		enemy.position = map_manager.get_spawn_enemy_location().get_children()[0].position
		$"../Entities/Enemies".add_child.call_deferred(enemy, true)
		await get_tree().create_timer(1).timeout


# when player enter new round: update enemy data

func apply_difficulty_for_new_quest(quest_id : int, event : Const.EventModifier = Const.EventModifier.NORMAL):
	var n_player = len(multiplayer.get_peers()) + 1
	for data_id in boss_data:
		enemy_data_dupplicate_source[data_id] = (boss_data[data_id] as BasicStats).duplicate()
		aplpy_difficulty(enemy_data_dupplicate_source[data_id], n_player, quest_id, event)
	for data_id in enemy_data:
		enemy_data_dupplicate_source[data_id] = (enemy_data[data_id] as BasicStats).duplicate()		
		aplpy_difficulty(enemy_data_dupplicate_source[data_id], n_player, quest_id, event)

func aplpy_difficulty(data : BasicStats, n_player : int, quest_id : int, event : Const.EventModifier = Const.EventModifier.NORMAL):
	# param that impact the enemy stats: 
	# 1. number of player
	# 2. quest id
	# 3. event modifier
	var player_ratio = 0.0
	match n_player:
		1: player_ratio = 1.0
		2: player_ratio = 1.2
		3: player_ratio = 1.5
		4: player_ratio = 2
		5: player_ratio = 3
	
	var extra_ratio_by_quest = 5.0/24.0*float(quest_id)
	# ratio extra by event
	(data as BasicStats).HP *= player_ratio+extra_ratio_by_quest
	(data as BasicStats).DMG *= player_ratio+extra_ratio_by_quest
	(data as BasicStats).ATTACK_SPEED *= 1.5 # harcode

# statue id : 4400 -> 4406
func spawn_defend_object(object_id : int, object_type : Const.ObjectTypes):
	# todo: statue_id -> change statue sprite
	var statue = (statue_data[object_id] as BasicStats).scene.instantiate()
	statue.object_id = object_id
	statue.basic_data = statue_data[object_id]
	statue.position = map_manager.get_spawn_defend_object_location().get_children().pick_random().position
	$"../Entities/Statues".add_child(statue, true)

func spawn_boss(object_id : int):
	# init
	for i in $"../Entities/Bosses".get_children():
		$"../Entities/Bosses".remove_child(i) # tmp
#	var boss = get_tree().get_first_node_in_group("boss")
	var boss = (enemy_data_dupplicate_source[object_id] as BasicStats).scene.instantiate()
	boss.position = map_manager.get_spawn_enemy_location().get_children().pick_random().position
	boss.basic_data = enemy_data_dupplicate_source[object_id]
	var boss_hp_component = boss.get_node("HealthComponent") as HealthComponent
	GameEvents.emit_boss_spawn(object_id, (enemy_data_dupplicate_source[object_id] as BasicStats).object_name)
	if boss_hp_component != null:
		boss_hp_component.health_changed.connect(GameEvents.emit_boss_hp_changed)
		var on_die = func():
			GameEvents.emit_boss_died(object_id)
		boss_hp_component.died.connect(on_die)
	$"../Entities/Bosses".add_child(boss)
	
