class_name Shockwave1
extends CanvasLayer

@onready var shockwave: Sprite2D = $Sprite

func start(pos: Vector2, radius: float, duration: float) -> void:
	layer = Helper.layer_shape.layer
	shockwave.scale = Vector2.ONE * (radius * 0.02)
	shockwave.global_position = pos
	var tween: Tween = Helper.new_tween()
	tween.tween_method(_update, 0.0, 1.0, duration)
	tween.tween_callback(queue_free)
	tween.play()

func _update(progress: float) -> void:
	var mat := shockwave.material as ShaderMaterial
	mat.set_shader_parameter("progress", progress)
