extends Node2D

@export var explosion_scene: PackedScene
@export var shockwave_scene: PackedScene

func _unhandled_input(event: InputEvent) -> void:
	if event is InputEventMouseButton and event.is_pressed():
		var pos = get_global_mouse_position()
		var radius := randf_range(50, 150)
		var duration := randf_range(0.8, 1.6)
		
		var explosion := explosion_scene.instantiate() as CircleExplosion1
		var shockwave := shockwave_scene.instantiate() as Shockwave1
		Helper.layer_shape.normal.add_child(explosion)
		Helper.layer_shape.top.add_child(shockwave)
		explosion.start(pos, radius, duration)
		shockwave.start(pos, radius, duration)
