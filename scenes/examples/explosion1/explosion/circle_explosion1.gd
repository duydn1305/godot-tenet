class_name CircleExplosion1
extends Sprite2D

func start(pos: Vector2, radius: float, duration: float) -> void:
	scale = Vector2.ONE * (radius * 0.02)
	global_position = pos
	var tween: Tween = Helper.new_tween()
	tween.tween_method(_update, 0.0, 1.0, duration)
	tween.tween_callback(queue_free)
	tween.play()

func _update(progress: float) -> void:
	var mat: ShaderMaterial = material
	mat.set_shader_parameter("progress", progress)
