@tool
extends Node2D

@onready var player := $Icon;

var grass_mat := preload("res://scenes/examples/grass1/grass_mat.tres")

func _process(delta: float) -> void:
	grass_mat.set_shader_parameter("player_pos", [
		player.global_position
	])
