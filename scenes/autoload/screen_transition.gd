extends CanvasLayer

signal transition_halfway()
signal transition_allway()

func start_transition():
	$ColorRect.visible = true
	$AnimationPlayer.play("fade_in")
	await $AnimationPlayer.animation_finished
	transition_halfway.emit()
	$AnimationPlayer.play_backwards("fade_in")
	await $AnimationPlayer.animation_finished
	transition_allway.emit()
	$ColorRect.visible = false	
