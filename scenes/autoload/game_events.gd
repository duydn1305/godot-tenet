extends Node

signal all_player_character_loaded()

signal player_killed_enemies(player_id : int, enemes_id : int, coin : int)
signal player_hp_change(player_id : int, current_hp : float, hp_ratio  : float, amount : float)
signal player_mp_change(player_id : int, current_mp : float, mp_ratio  : float)
signal player_level_up(player_id : int, which_level : int)
signal player_collect_exp(player_id : int, amount : float)
signal player_skill_cooldown(player_id : int, active_by_key : Const.ActiveSkill, cooldown_seconds : float, time_remain : float)
signal player_die(player_id : int)
signal player_respawn(player_id : int)
signal end_game(win : bool, msg : String)
signal defend_objective_collapse(object_id : int, object_type : Const.ObjectTypes)

signal boss_spawned(object_id, boss_name)
signal boss_died(object_id)
signal boss_hp_changed(current : float, amount : float, ratio : float)

signal player_disconnected(player_id)

func emit_all_player_character_loaded():
	all_player_character_loaded.emit()

func emit_player_killed_enemies(player_id : int, enemes_id : int, coin : int):
	player_killed_enemies.emit(player_id, enemes_id, coin)
	
func emit_player_hp_change(player_id : int, current_hp : float, hp_ratio  : float, amount : float):
	player_hp_change.emit(player_id, current_hp, hp_ratio, amount)
	
func emit_player_mp_change(player_id : int, current_mp : float, mp_ratio  : float):
	player_mp_change.emit(player_id, current_mp, mp_ratio)
	
func emit_player_level_up(player_id : int, which_level : int):
	player_level_up.emit(player_id, which_level)	

func emit_player_collect_exp(which_player : int, amount : float):
	player_collect_exp.emit(which_player, amount)

func emit_player_skill_cooldown(player_id : int, active_by_key : Const.ActiveSkill, cooldown_seconds : float, time_remain : float):
	player_skill_cooldown.emit(player_id, active_by_key, cooldown_seconds, time_remain)

@rpc("any_peer", "call_local", "reliable")
func emit_player_died(player_id : int):
	player_die.emit(player_id)
	
@rpc("any_peer", "call_local", "reliable")
func emit_player_respawn(player_id : int):
	player_respawn.emit(player_id)
	
func emit_boss_spawn(boss_id, boss_name):
	boss_spawned.emit(boss_id, boss_name)

func emit_boss_died(boss_id):
	boss_died.emit(boss_id)
	
func emit_boss_hp_changed(current : float, amount : float, ratio : float):
	boss_hp_changed.emit(current, amount, ratio)
	
@rpc("any_peer", "call_local")
func emit_end_game(win : bool, msg : String):
	end_game.emit(win, msg)
	
func emit_defend_objective_collapse(object_id : int, object_type : Const.ObjectTypes):
	defend_objective_collapse.emit(object_id, object_type)

func emit_player_disconnected(player_id):
	player_disconnected.emit(player_id)
	
var floating_text_scene = preload("res://scenes/ui/floating_text.tscn")

# spawn txt. this is tmp, must be place in the manager node
@rpc("authority", "call_local")
func spawn_floating_text(damage : float, global_position, color : Color = Color.WHITE):
	var floating_text = floating_text_scene.instantiate() as Node2D
	floating_text.text_color = color
	
	floating_text.global_position = global_position + (Vector2.UP * 16)
	
	var format_string = "%0.1f"
	if round(damage) == damage:
		format_string = "%0.0f"
	get_tree().get_first_node_in_group("foreground_layer").add_child(floating_text, true)
	floating_text.start(format_string % damage)
	
@rpc("authority", "call_local")	
func spawn_floating_text_str(msg : String, global_position, color : Color = Color.WHITE):
	var floating_text = floating_text_scene.instantiate() as Node2D
	floating_text.text_color = color	
	floating_text.global_position = global_position
	get_tree().get_first_node_in_group("foreground_layer").add_child(floating_text, true)
	floating_text.start(msg)
