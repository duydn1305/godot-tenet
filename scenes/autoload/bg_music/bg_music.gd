extends Node

enum Type {
	IDLE,
	COMBAT,
	BOSS
}

@export var idle_songs: Array[AudioStream]
@export var combat_songs: Array[AudioStream]
@export var boss_songs: Array[AudioStream]

@onready var _audio := $AudioStreamPlayer as AudioStreamPlayer

var _map := {}
var _cur_type: Type

func _ready() -> void:
	_map[Type.IDLE] = idle_songs
	_map[Type.COMBAT] = combat_songs
	_map[Type.BOSS] = boss_songs
	_audio.finished.connect(play_type.bind(_cur_type, true))

func play_type(type: Type, is_repeat := false) -> void:
	await get_tree().create_timer(2).timeout
	if !is_repeat and _audio.stream != null and type == _cur_type:
		return
	_cur_type = type
	var songs: Array[AudioStream] = _map.get(type, [])
	if !songs.is_empty():
		play(0.0, songs.pick_random())
	else:
		pause()

func play(from := 0.0, stream: AudioStream = null) -> void:
	pause()
	_audio.stream = stream
	_audio.play(from)
	resume()

func pause() -> void:
	_audio.playing = false

func resume() -> void:
	_audio.playing = true
