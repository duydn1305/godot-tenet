class_name GameSettingsSound

const KEYS := {
	"master_volume": ["c_volume", "master"],
	"music_volume": ["c_volume", "music"],
	"sound_volume": ["c_volume", "sound"]
}

var master_idx: int
var music_idx: int
var sound_idx: int

var _bus_idx_to_cfg: Dictionary

func _init() -> void:
	master_idx = AudioServer.get_bus_index("Master")
	music_idx = AudioServer.get_bus_index("Music")
	sound_idx = AudioServer.get_bus_index("Sound")
	_bus_idx_to_cfg = {
		master_idx: "master_volume",
		music_idx: "music_volume",
		sound_idx: "sound_volume"
	}

func set_default() -> void:
	save_volume(master_idx, 1.0)
	save_volume(music_idx, 1.0)
	save_volume(sound_idx, 1.0)

func startup() -> void:
	set_volume(master_idx, get_volume(master_idx))
	set_volume(music_idx, get_volume(music_idx))
	set_volume(sound_idx, get_volume(sound_idx))

func get_volume(bus_idx: int) -> float:
	var cfg_name: String = _bus_idx_to_cfg.get(bus_idx)
	return GameSettings.get_value(KEYS[cfg_name])

func set_volume(bus_idx: int, value: float) -> void:
	AudioServer.set_bus_volume_db(bus_idx, linear_to_db(value))

func save_volume(bus_idx: int, value: float) -> void:
	var cfg_name: String = _bus_idx_to_cfg.get(bus_idx)
	GameSettings.set_value(KEYS[cfg_name], value)
