extends Node

var display: GameSettingsDisplay
var sound: GameSettingsSound

var save_path: String
var config: ConfigFile
var environment: Environment
var existed_when_create: bool
var base_size: Vector2i

@onready var freq: GameSettingsFreqCheck = $FreqCheck

func _init() -> void:
	save_path = ProjectSettings.get_setting("application/config/project_settings_override")
	existed_when_create = FileAccess.file_exists(save_path)
	config = ConfigFile.new()
	display = GameSettingsDisplay.new()
	sound = GameSettingsSound.new()
	base_size = Vector2i(
		ProjectSettings.get_setting("display/window/size/viewport_width"),
		ProjectSettings.get_setting("display/window/size/viewport_height")
	)

func _ready() -> void:
	get_window().min_size = base_size
	environment = $WorldEnvironment.environment

	if !existed_when_create:
		display.set_default()
		sound.set_default()
		save()
	else:
		config.load(save_path)
	display.startup()
	sound.startup()

func save() -> void:
	config.save(save_path)

func get_value(key: Array) -> Variant:
	return config.get_value(key[0], key[1])

func set_value(key: Array, value: Variant) -> void:
	return config.set_value(key[0], key[1], value)
