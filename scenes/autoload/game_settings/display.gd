class_name GameSettingsDisplay

const KEYS := {
	"display_mode": ["display", "window/size/mode"],
	"fps_limit": ["application", "run/max_fps"],
	"vsync": ["display", "window/vsync/vsync_mode"],
	"color_brightness": ["c_color", "brightness"],
	"color_contrast": ["c_color", "contrast"],
	"color_saturation": ["c_color", "saturation"]
}

func set_default() -> void:
	save_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
	save_fps_limit(60)
	save_vsync(DisplayServer.VSYNC_ADAPTIVE)
	save_color_brightness(1.0)
	save_color_contrast(1.0)
	save_color_saturation(1.0)

func startup() -> void:
	set_color_brightness(get_color_brightness())
	set_color_contrast(get_color_contrast())
	set_color_saturation(get_color_saturation())

func get_mode() -> DisplayServer.WindowMode:
	return GameSettings.get_value(KEYS.display_mode)

func get_fps_limit() -> int:
	return GameSettings.get_value(KEYS.fps_limit)

func get_vsync() -> DisplayServer.VSyncMode:
	return GameSettings.get_value(KEYS.vsync)

func get_color_brightness() -> float:
	return GameSettings.get_value(KEYS.color_brightness)

func get_color_contrast() -> float:
	return GameSettings.get_value(KEYS.color_contrast)

func get_color_saturation() -> float:
	return GameSettings.get_value(KEYS.color_saturation)

func set_mode(mode: DisplayServer.WindowMode) -> void:
	if mode == DisplayServer.window_get_mode(): return
	if mode == DisplayServer.WINDOW_MODE_WINDOWED:
		DisplayServer.window_set_flag(DisplayServer.WINDOW_FLAG_BORDERLESS, false)
	DisplayServer.window_set_mode(mode)
	if mode == DisplayServer.WINDOW_MODE_WINDOWED:
		GameSettings.freq.update_window()

func set_fps_limit(fps_limit: int) -> void:
	if fps_limit == Engine.max_fps: return
	Engine.max_fps = fps_limit

func set_vsync(mode: DisplayServer.VSyncMode) -> void:
	if mode == DisplayServer.window_get_vsync_mode(): return
	DisplayServer.window_set_vsync_mode(mode)

func set_color_brightness(value: float) -> void:
	GameSettings.environment.adjustment_brightness = value

func set_color_contrast(value: float) -> void:
	GameSettings.environment.adjustment_contrast = value

func set_color_saturation(value: float) -> void:
	GameSettings.environment.adjustment_saturation = value

func save_mode(mode: DisplayServer.WindowMode) -> void:
	GameSettings.set_value(KEYS.display_mode, mode)

func save_fps_limit(fps_limit: int) -> void:
	GameSettings.set_value(KEYS.fps_limit, fps_limit)

func save_vsync(mode: DisplayServer.VSyncMode) -> void:
	GameSettings.set_value(KEYS.vsync, mode)

func save_color_brightness(value: float) -> void:
	GameSettings.set_value(KEYS.color_brightness, value)

func save_color_contrast(value: float) -> void:
	GameSettings.set_value(KEYS.color_contrast, value)

func save_color_saturation(value: float) -> void:
	GameSettings.set_value(KEYS.color_saturation, value)
