class_name GameSettingsFreqCheck
extends Timer

const SAVE_PATH := "user://freq.cfg"
const KEYS := {
	"window_size": ["window", "size"],
	"window_position": ["window", "position"]
}

var cfg := ConfigFile.new()
var window: Window

func _ready() -> void:
	window = get_window()
	var existed := FileAccess.file_exists(SAVE_PATH)
	if !existed:
		save_value(KEYS.window_size, Vector2i(1280, 720))
		save_value(KEYS.window_position, GameSettings.base_size/2)
		save()
	else:
		cfg.load(SAVE_PATH)

	if DisplayServer.window_get_mode() == DisplayServer.WINDOW_MODE_WINDOWED:
		update_window()

func get_window_size() -> Vector2i:
	return cfg.get_value(KEYS.window_size[0], KEYS.window_size[1])

func get_window_position() -> Vector2i:
	return cfg.get_value(KEYS.window_position[0], KEYS.window_position[1])

func update_window() -> void:
	window.size = get_window_size()
	window.position = get_window_position()

func save() -> void:
	cfg.save(SAVE_PATH)

func save_value(key: Array, value: Variant) -> void:
	return cfg.set_value(key[0], key[1], value)

func _should_save() -> bool:
	var display_mode := DisplayServer.window_get_mode()
	if display_mode == DisplayServer.WINDOW_MODE_WINDOWED:
		var w_size: Vector2i = window.size
		var w_pos: Vector2i = window.position
		if get_window_size() != w_size or get_window_position() != w_pos: return true
	return false

func _update_data() -> void:
	if !_should_save(): return
	save_value(KEYS.window_size, window.size)
	save_value(KEYS.window_position, window.position)
	save()
