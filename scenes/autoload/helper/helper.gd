extends Node

@export var confirm_modal_scene: PackedScene

@onready var layer_shape: DrawShapeLayer = $DrawShape
@onready var layer_ui_overlay: CanvasLayer = $GameUIOverlay
@onready var layer_menu_overlay: CanvasLayer = $PauseMenuOverlay
@onready var toasts := $ToastContainer as ToastContainer

func new_tween(bind_node: Node = null) -> Tween:
	var tween := get_tree().create_tween()
	tween.set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT).stop()
	if bind_node:
		tween.bind_node(bind_node)
	return tween

func load_resources(folder_path: String, bind_node: Node = null) -> FolderAsyncLoader:
	return FolderAsyncLoader.new(folder_path, bind_node)

func get_default_name() -> String:
	var res := OS.get_environment("USERNAME")
	if !res: res = OS.get_environment("USER")
	if !res: res = OS.get_environment("LOGNAME")
	if !res: res = OS.get_environment("COMPUTERNAME")
	if !res: res = OS.get_model_name()
	if res == "GenericDevice": res = OS.get_distribution_name()
	if !res: res = "Adventurer"
	return res

func clear_children(node: Node) -> void:
	for child in node.get_children():
		child.queue_free()

func arr_to_arr_dict(array: Array) -> Array[Dictionary]:
	var res: Array[Dictionary] = []
	for item in array:
		res.append(item.to_dict())
	return res
