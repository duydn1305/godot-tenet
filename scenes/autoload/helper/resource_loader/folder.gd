class_name FolderAsyncLoader

signal finished(array: Array[Resource])

var _thread := Thread.new()

func _init(folder_path: String, bind_node: Node) -> void:
	_thread.start(_start.bind(folder_path))
	if bind_node:
		bind_node.tree_exiting.connect(func() -> void:
			wait_to_finish()
		)

func wait_to_finish() -> void:
	_thread.wait_to_finish()

func _start(path) -> void:
	var dir := DirAccess.open(path)
	var files := dir.get_files()
	var result: Array[Resource] = []
	for file in files:
		file = file.trim_suffix(".import").trim_suffix(".remap")
		if !file.ends_with(".res") and !file.ends_with(".tres"): continue
		var resource = load(path.path_join(file))
		if resource:
			result.append(resource)
	finished.emit(result)

