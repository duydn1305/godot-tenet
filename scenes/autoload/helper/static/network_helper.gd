class_name NetworkHelper

const PORT_GAME := 26699
const PORT_BROADCAST := 26799
const MAX_PLAYERS := 5

enum PacketType {
	BROADCAST,
	AUTH_REQ,
	AUTH_RESP
}

enum AuthResponse {
	OK,
	WRONG,
	BANNED,
	ERROR
}

static func reset_multiplayer() -> void:
	var multiplayer = Helper.multiplayer as SceneMultiplayer
	if multiplayer.multiplayer_peer:
		multiplayer.multiplayer_peer.close()
		multiplayer.multiplayer_peer = null
	Store.ban_list.clear()
	Store.players.clear()

static func create(type: int, data: Dictionary) -> PackedByteArray:
	var packet := {
		"type": type
	}
	packet.merge(data)
	return JSON.stringify(packet).to_utf8_buffer()

static func validate(packet: PackedByteArray) -> Dictionary:
	var data: Variant = JSON.parse_string(packet.get_string_from_utf8())
	if typeof(data) != TYPE_DICTIONARY: return {}
	data = data as Dictionary
	if data == null or typeof(data.get("type")) != TYPE_FLOAT: return {}
	return data
