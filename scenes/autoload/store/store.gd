extends Node

var meta_progression: SaveMetaProgression
var achievements: SaveAchievements
var shop: SaveShop
var general: SaveGeneral
var ban_list := {}
var players: Array[PlayerBaseData] = []
var me: PlayerBaseData

func _init() -> void:
	DirAccess.make_dir_absolute("user://portable")
	meta_progression = SaveMetaProgression.load_and_create()
	player_data["meta_upgrade"] = meta_progression._data
	achievements = SaveAchievements.load_and_create()
	shop = SaveShop.load_and_create()
	general = SaveGeneral.load_and_create()
	if !general.player_name:
		general.player_name = Helper.get_default_name()

static func open_save_folder() -> void:
	OS.shell_show_in_file_manager(ProjectSettings.globalize_path("user://portable"))

# todo: structure data as resource
# tmp -> place in world/game_manager
var player_data = {
	"player_name": "default",
	"player_character_name": "Lancer",
	"player_character_id": 5,
}

# for host store data
var player_data_by_id = {
	1: player_data
}

# A. when init game dungeon
# 1. character
func load_character_stats(character_id):
	# load tres
	# get damage from res
	var res = {
		"hp": 120,
		"damage": 15,
	}
	
	# data has hp and dmg
	player_data[Const.Stats.HP] = res.hp
	player_data[Const.Stats.DMG] = res.damage

# 2. check meta progression: same as character data
func load_upgrade():
	# foreach upgrade, id -> which stats -> update player data
	# data has increate crit damage
	var res = {
		"crit_chance": 0.2
	}
	player_data[Const.Stats.CRIT_RATE] += res.crit_chance
	
