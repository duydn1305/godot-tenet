extends CanvasLayer

@rpc("any_peer", "call_local")
func refresh_player_list(_player_loading_status : Dictionary):
	print("call refresh_player_list, ", _player_loading_status)
	for node in $TeamContainer/TeamInfo.get_children():
		$TeamContainer/TeamInfo.remove_child(node)
	for player_id in _player_loading_status:
		var label_node = Label.new()
		label_node.text = _player_loading_status[player_id]["player_name"]
		if _player_loading_status[player_id].has("ready") && _player_loading_status[player_id]["ready"]:
			label_node.text = _player_loading_status[player_id]["player_name"] + " - ready"
		$TeamContainer/TeamInfo.add_child(label_node)

@rpc("any_peer", "call_local")
func hide_screen():
	visible = false

@rpc("any_peer", "call_local")
func show_screen():
	visible = true

# own by host
var player_loading_status = {
	1: {
		"ready": false,
		"player_name": "no name",
	}
}

func reset_player_loading_status():
	player_loading_status = {
		1: {
			"ready": false,
			"player_name": "no name",
		}
	}
# tmp

var player_stage_to_emit = 1
signal all_player_stage_ready()
# own by host
var player_stage_loading := 0 :
	set(new_count):
		if new_count == 0:
			for player_id in player_loading_status:
				player_loading_status[player_id]["ready"] = false
#			player_loading_status = {}
#			for data in Store.players:
#				player_loading_status[data.id] = {
#					"player_name": data.player_name,
#					"ready": false,
#				}
#				print("preset list player ready status: ", len(Store.players))
		player_stage_loading = new_count
#		if player_stage_loading == len(Store.players):
		if player_stage_loading == player_stage_to_emit: # tmp
			await get_tree().create_timer(1).timeout # avoid leak signal in same function
			all_player_stage_ready.emit()

# by default, loading screen wait for all player to transmit signal == len(player)
func new_loading_stage(new_player_stage_to_emit : float = 0):
	if new_player_stage_to_emit == 0:
		player_stage_to_emit = len(player_loading_status)
	else: player_stage_to_emit = new_player_stage_to_emit
	player_stage_loading = 0
	
func new_loading_stage_for_lobby():
	player_stage_to_emit = len(Store.players)
	player_loading_status = {}
	for data in Store.players:
		player_loading_status[data.id] = {
			"player_name": data.player_name,
			"ready": false,
		}
	print("preset list player ready status: ", len(Store.players))
	player_stage_loading = 0
	
# own by host
func update_player_name():
	for data in Store.players:
		player_loading_status[data.id] = {
			"player_name": data.player_name,
		}

			
# own by host
@rpc("any_peer", "call_local")
func player_has_loaded():
	var sender_id = multiplayer.get_remote_sender_id() 
	player_loading_status[sender_id]["ready"] = true
	player_stage_loading += 1
#	refresh_player_list.rpc(player_loading_status)
