extends Area2D
@onready var interactive_object_component = $InteractiveObjectComponent
@export var meta_upgrade_page : PackedScene
var meta_upgrade_opening = false

func _ready():
	interactive_object_component.connect("object_selected", handle_open_meta_upgrade_table)
	
	
func handle_open_meta_upgrade_table():
	if meta_upgrade_opening:
		return
	meta_upgrade_opening = true
	SceneTransition.start_transition()
	var page_instance = meta_upgrade_page.instantiate()
	get_tree().root.add_child(page_instance)
	await SceneTransition.transition_halfway
	page_instance.connect("exit_page", func():
		await get_tree().create_timer(2).timeout
		meta_upgrade_opening = false
	)
