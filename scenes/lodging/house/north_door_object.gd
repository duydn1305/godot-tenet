extends Area2D

@onready var interactive_object_component = $InteractiveObjectComponent
var door_opening = false

func _ready():
	interactive_object_component.connect("object_selected", handle_open_door)
	
	
func handle_open_door():
	if door_opening:
		return
	door_opening = true
	SceneTransition.start_transition()
#	var	page_instance = preload("res://scenes/ui/lobby/browser/browser.tscn").instantiate()
	await SceneTransition.transition_halfway
	get_tree().change_scene_to_file("res://scenes/dungeons/multiplayer.tscn")
#	get_tree().root.add_child(page_instance)
#
#	page_instance.connect("exit_page", func():
#		await get_tree().create_timer(2).timeout
#		door_opening = false
#	)
