extends Node2D

@onready var character_selection = $Visual/CharacterSelection

var character_resource = {
	2: preload("res://resources/characters/0_data/2.tres"),
	3: preload("res://resources/characters/0_data/3.tres"),
	5: preload("res://resources/characters/0_data/5.tres"),
	6: preload("res://resources/characters/0_data/6.tres"),
}


func _ready():
	BgMusic.play_type(BgMusic.Type.IDLE)
	get_tree().set_multiplayer(MultiplayerAPI.create_default_interface())
	get_tree().paused = false
#	var peer = ENetMultiplayerPeer.new()
#	peer.create_server(4433)
#	multiplayer.multiplayer_peer = peer
	load_character(int(Store.general.last_character_id))
#	player.player_id = 1
	if character_selection != null:
		character_selection.character_changed.connect(load_character)
		character_selection.hide_current_character()

func load_character(current_character_id):
	LoadingScreen.show_screen()
	print("LoadingScreen.show_screen()")
	var selected_character = (character_resource[current_character_id] as BasicStats).scene
	var player = selected_character.instantiate()
	for i in get_tree().get_nodes_in_group("player_alive"):
		$Visual.remove_child(i)
	$Visual.add_child(player)
	player.global_position = $SpawnLocation.get_child(0).position
	LoadingScreen.hide_screen()
	print("LoadingScreen.hide_screen()")
	player.set_player_name(Store.general.player_name)
	

#func _process(delta):
#	if Input.is_key_pressed(KEY_ESCAPE):
#		SceneTransition.start_transition()
#		await SceneTransition.transition_halfway
#		get_tree().change_scene_to_file("res://scenes/ui/main_menu/main_menu_2.tscn")
#


func _unhandled_input(event):
	if event.is_action_pressed("escape"):
		get_tree().root.set_input_as_handled()
		if $CanvasLayer.get_child_count() > 0:
			return
		var confirm_modal = preload("res://scenes/ui/common/confirm_modal/confirm_modal.tscn").instantiate() as ConfirmModal
		$CanvasLayer.add_child(confirm_modal)
		confirm_modal.set_header_text("Exit")
		confirm_modal.set_body_text("Do you want to go back?")
		confirm_modal.pressed.connect(confirm_quit)

func confirm_quit(ok):
	if !ok:
		return
	
	SceneTransition.start_transition()
	await SceneTransition.transition_halfway
	get_tree().change_scene_to_file("res://scenes/ui/main_menu/main_menu_2.tscn")

