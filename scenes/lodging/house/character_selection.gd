extends Area2D


@onready var interactive_object_component = $InteractiveObjectComponent
@export var character_select_page : PackedScene
var meta_upgrade_opening = false
signal character_changed(current_character_id)

func _ready():
	interactive_object_component.connect("object_selected", handle_open_meta_upgrade_table)
	
	
func handle_open_meta_upgrade_table():
	if meta_upgrade_opening:
		return
	meta_upgrade_opening = true
	SceneTransition.start_transition()
	var page_instance = character_select_page.instantiate()
	get_tree().root.add_child.call_deferred(page_instance)
	await SceneTransition.transition_halfway
	page_instance.connect("exit_page", func():
		LoadingScreen.show_screen()
		await get_tree().create_timer(2).timeout
		meta_upgrade_opening = false
		hide_current_character()
	)

func hide_current_character():
	var current_character_id = int(Store.general.last_character_id)
	$Warior.visible = true
	$Knight.visible = true
	$Lancer.visible = true
	$Archer.visible = true
		
	if current_character_id == 2:
		$Warior.visible = false
	if current_character_id == 3:
		$Knight.visible = false
	if current_character_id == 5:
		$Lancer.visible = false
	if current_character_id == 6:
		$Archer.visible = false
	character_changed.emit(current_character_id)
