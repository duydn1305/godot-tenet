extends Area2D
@onready var interactive_object_component = $InteractiveObjectComponent
@export var wiki_page : PackedScene
var wiki_opening = false

func _ready():
	interactive_object_component.connect("object_selected", handle_open_wiki_table)
	
	
func handle_open_wiki_table():
	if wiki_opening:
		return
	wiki_opening = true
	SceneTransition.start_transition()
	var page_instance = wiki_page.instantiate()
	get_tree().root.add_child(page_instance)
	await SceneTransition.transition_halfway
	page_instance.connect("exit_page", func():
		await get_tree().create_timer(2).timeout
		wiki_opening = false
	)
