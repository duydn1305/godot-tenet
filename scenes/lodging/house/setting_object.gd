extends Area2D
@onready var interactive_object_component = $InteractiveObjectComponent
@export var setting_page : PackedScene
var setting_opening = false

func _ready():
	interactive_object_component.connect("object_selected", handle_open_setting_table)
	
	
func handle_open_setting_table():
	if setting_opening:
		return
	setting_opening = true
	SceneTransition.start_transition()
	var page_instance = setting_page.instantiate()
	get_tree().root.add_child(page_instance)
	await SceneTransition.transition_halfway
	page_instance.connect("exit_page", func():
		await get_tree().create_timer(2).timeout
		setting_opening = false
	)
