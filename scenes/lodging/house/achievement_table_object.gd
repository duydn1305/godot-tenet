extends Area2D
@onready var interactive_object_component = $InteractiveObjectComponent
@export var ac_page : PackedScene
var ac_opening = false

func _ready():
	interactive_object_component.connect("object_selected", handle_open_wiki_table)
	
	
func handle_open_wiki_table():
	if ac_opening:
		return
	ac_opening = true
	SceneTransition.start_transition()
	var page_instance = ac_page.instantiate()
	get_tree().root.add_child(page_instance)
	await SceneTransition.transition_halfway
	page_instance.connect("exit_page", func():
		await get_tree().create_timer(2).timeout
		ac_opening = false
	)
