extends CharacterBody2D

@onready var velocity_component = $VelocityComponent as VelocityComponent
@onready var visual = $Visuals
@onready var animation_player = $AnimationPlayer as AnimationPlayer

var boss_name : String = "Cube" # todo: get from data
var basic_data : BasicStats

var strategy : VelocityComponent.PlayerRankType = VelocityComponent.PlayerRankType.Nearest
var is_underneath = false

func _ready():
	set_physics_process(multiplayer.is_server())
	if !multiplayer.is_server():
		return
	
	# set damage
#	($Visuals/HitboxComponent as BaseHitbox).damage = 2
	if basic_data != null:
		($Aura/HitboxComponent as BaseHitbox).damage = basic_data.DMG/5
		($Abilities/ShootableComponent as ShootableComponent).damage = basic_data.DMG
		
#	$AttackTimer.timeout.connect(handle_attack_timer)
#	$TailAttackTimer.timeout.connect(handle_tail_attack_timer)
	$AttackIntervalTimer.timeout.connect(handle_attack_timer)
	$TornadoAttackTimer.timeout.connect(handle_tornaldo_attack_timer)
	$GoUnderneathTimer.timeout.connect(handle_go_under)
	print($HealthComponent.max_health)
	

@rpc("any_peer", "call_local")
func play_anim(anim_name, backwards : bool = false):
	if backwards: animation_player.play_backwards(anim_name)
	else: animation_player.play(anim_name)
		
func handle_go_under():
	if !multiplayer.is_server():
		return
	if !is_underneath:
		play_anim.rpc("appear", true)
		await animation_player.animation_finished
		var t = create_tween() as Tween
		t.tween_property($Aura, "scale", Vector2(0.2, 0.12), 0.3).set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_CUBIC)		
		$Visuals.visible = false
		# if is on the surface: then activate hurt box		
		$HurtboxComponent/CollisionShape2D.set_deferred("disabled", true)
		$FootCollision.set_deferred("disabled", true)
		# speed up
		velocity_component.increase_max_speed_multiplier(2)
	else:
		# 0.4, 0.24
		var t = create_tween() as Tween
		t.tween_property($Aura, "scale", Vector2(0.4, 0.24), 0.3).set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_CUBIC)
		$Visuals.visible = true
		play_anim.rpc("appear")
		# if is below: then diable hurt box
		$HurtboxComponent/CollisionShape2D.set_deferred("disabled", false)
		$FootCollision.set_deferred("disabled", false)
		velocity_component.increase_max_speed_multiplier(-2)
		
		
	is_underneath = !is_underneath
	
func _process(delta):
	if is_underneath:
		velocity_component.find_player_by_rank_type(strategy)
		velocity_component.accelebrate_to_player()
		velocity_component.move(self)
	

func handle_attack_timer():
	if !multiplayer.is_server():
		return
	$Aura/HitboxComponent/CollisionShape2D.disabled = !$Aura/HitboxComponent/CollisionShape2D.disabled
	
func handle_tornaldo_attack_timer():
	if !multiplayer.is_server():
		return
	if is_underneath:
		return
	$Abilities/ShootableComponent.shoot_projectiles($Visuals/BulletPoint.global_position, velocity_component.player_node.global_position - $Visuals/BulletPoint.global_position, 6, deg_to_rad(360), 0, false)
	
	
#
#	($Abilities/ShootableComponent as ShootableComponent).shoot_projectiles($Visual/Cube/Parts.global_position, velocity_component.player_node.global_position - $Visual/Cube/Parts.global_position, 10, deg_to_rad(90), 0.05)
#
#func _on_timer_timeout():
#	is_dropping = true
#	await velocity_component.temporary_change_speed_multiplier(-1, 1)
#	drop_cube.rpc()
#
#
#func _on_time_transform_timeout():
#	if is_dropping:
#		return
#	cube_transform.rpc()
