extends CharacterBody2D
@onready var visuals = $Visuals
@onready var velocity_component = $VelocityComponent
@onready var ShoottableComponent = $Abilities/ShootableComponent as ShootableComponent
var basic_data : BasicStats
var strategy : VelocityComponent.PlayerRankType = VelocityComponent.PlayerRankType.Nearest
var move_speed_multiplier = 10
var dang_dung_lai = false

var bullet_per_shot = 5
var deg_per_shot = 90

#là hàm khởi tạo vd khi con boss đã sẵn sàng rồi thì nó sẽ chạy 
func _ready():
	if !multiplayer.is_server():
		return
	$Timer.timeout.connect(laotoi)
	$Abilities/TGBanDan.timeout.connect(on_shoot_timer_timeout)
		# set damage
	($Visuals/HitboxComponent as BaseHitbox).damage = 2
	if basic_data != null:
		($Visuals/HitboxComponent as BaseHitbox).damage = basic_data.DMG
		bullet_per_shot = basic_data.BULLET_PER_SHOT
		deg_per_shot = basic_data.SHOOT_ANGLE
		ShoottableComponent.damage = basic_data.DMG
		$AttackInterval
	$AttackInterval.timeout.connect(on_off_hitbox)
		
	
#hàm này dùng để xử lí frame liên tục
func _process(delta):
	if dang_dung_lai:
		return
	#tính vector di chuyển hướng đến nhân vật
	velocity_component.find_player_by_rank_type(strategy)
	velocity_component.accelebrate_to_player()
	velocity_component.move(self)
	#khi mà boss đang trong trạng thái tấn công thì dừng lại 
		

	var move_sign = sign(velocity.x)
	if move_sign != 0:
		visuals.scale = Vector2(move_sign, 1)
	
func on_off_hitbox():
	$Visuals/HitboxComponent/CollisionShape2D.set_deferred("disabled", !$Visuals/HitboxComponent/CollisionShape2D.disabled)
	

func laotoi():
	print("doi 2s")
	
	# dung lai trong 2s
	velocity_component.stop_instantly()
	dang_dung_lai = true
	await get_tree().create_timer(2).timeout
	
	dang_dung_lai = false
	
	velocity_component.move_speed_multiplier = 2
	print("tang toc 2s")
	$AttackInterval.start()
	#2 giay sau
	$AnimationPlayer.play("attack")
	
	await get_tree().create_timer(2).timeout
	$AttackInterval.stop()
		
	velocity_component.move_speed_multiplier = 1
	print("binh thuong")
	$Visuals/HitboxComponent/CollisionShape2D.disabled = true
	$AnimationPlayer.play("walk")

func on_shoot_timer_timeout():
	var init_position : Vector2
	var mv_vector : Vector2
	init_position = global_position
	mv_vector = velocity
	ShoottableComponent.shoot_projectiles(init_position, mv_vector, bullet_per_shot, deg_to_rad(deg_per_shot))
	
	
	
	
	
	
	
	
	
	

