extends CharacterBody2D

@onready var velocity_component = $VelocityComponent as VelocityComponent
@onready var visual = $Visual
@export var drop_time : float = 5
@onready var animation_player = $Visual/AnimationPlayer
@onready var animation_transform = $Visual/AnimationTransform

var is_dropping = false
var is_transforming = false
var boss_name : String = "Cube" # todo: get from data
var basic_data : BasicStats

var strategy : VelocityComponent.PlayerRankType = VelocityComponent.PlayerRankType.Nearest

func _ready():
	set_physics_process(multiplayer.is_server())
	if !multiplayer.is_server():
		return
	$Timer.timeout.connect(_on_timer_timeout)
	$TimeTransform.timeout.connect(_on_time_transform_timeout)
	
	# set damage
	($HitboxComponent as BaseHitbox).damage = 2
	if basic_data != null:
		($HitboxComponent as BaseHitbox).damage = basic_data.DMG

func _process(delta):
	if is_dropping:
		return
		
	velocity_component.find_player_by_rank_type(strategy)
	velocity_component.accelebrate_to_player()
	velocity_component.move(self)
	
	var move_sign = sign(velocity.x)
	if move_sign != 0:
		visual.scale = Vector2(move_sign, 1)
		

@rpc("any_peer", "call_local")
func drop_cube():
	velocity_component.stop_instantly()
	animation_player.play("drop")
	await animation_player.animation_finished
	$ShareTimer.start(1)
	await $ShareTimer.timeout
	is_dropping = false
	animation_player.play("fly_up")
	await animation_player.animation_finished
	animation_player.play("flying")
	
	
@rpc("any_peer", "call_local")
func cube_transform():
	is_transforming = true
	animation_transform.play("transform_and_shoot")
	print("shot")
	handle_shot()
	$ShareTimer.start(2)
	await $ShareTimer.timeout
	animation_transform.play_backwards("reform")
	await animation_transform.animation_finished
	is_transforming = false

func handle_shot():
	if !multiplayer.is_server():
		return
	($Abilities/ShootableComponent as ShootableComponent).shoot_projectiles($Visual/Cube/Parts.global_position, velocity_component.player_node.global_position - $Visual/Cube/Parts.global_position, 10, deg_to_rad(90), 0.05)

func _on_timer_timeout():
	await velocity_component.temporary_change_speed_multiplier(5, 1)
	is_dropping = true
	drop_cube.rpc()


func _on_time_transform_timeout():
	if is_dropping:
		return

	cube_transform.rpc()
