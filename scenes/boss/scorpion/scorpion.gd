extends CharacterBody2D

@onready var velocity_component = $VelocityComponent as VelocityComponent
@onready var visual = $Visuals
@onready var animation_player = $AnimationPlayer as AnimationPlayer

var boss_name : String = "Cube" # todo: get from data
var basic_data : BasicStats

var strategy : VelocityComponent.PlayerRankType = VelocityComponent.PlayerRankType.Nearest

func _ready():
	set_physics_process(multiplayer.is_server())
	if !multiplayer.is_server():
		return
	
	# set damage
	($Visuals/HitboxComponent as BaseHitbox).damage = 2
	if basic_data != null:
		($Visuals/HitboxComponent as BaseHitbox).damage = basic_data.DMG
		($Abilities/ShootableComponent as ShootableComponent).damage = basic_data.DMG/2.0
		
	$AttackTimer.timeout.connect(handle_attack_timer)
	$TailAttackTimer.timeout.connect(handle_tail_attack_timer)

func _process(delta):
	velocity_component.find_player_by_rank_type(strategy)
	velocity_component.accelebrate_to_player()
	velocity_component.move(self)
	
	var move_sign = sign(velocity.x)
	if move_sign != 0:
		visual.scale = Vector2(move_sign, 1)
		

func handle_attack_timer():
	if !multiplayer.is_server():
		return
		
	if animation_player.current_animation == "tail_attack":
		return
	velocity_component.temporary_change_speed_multiplier(-0.5, 0.5)
	animation_player.play("attack")
	await animation_player.animation_finished
	animation_player.play("walk")
	
func handle_tail_attack_timer():
	if !multiplayer.is_server():
		return
		
	velocity_component.temporary_change_speed_multiplier(-0.5, 0.5)
	animation_player.play("tail_attack")
	await animation_player.animation_finished
	animation_player.play("walk")

func handle_shot():
	if !multiplayer.is_server():
		return
	
	$Abilities/ShootableComponent.shoot_projectiles($Visuals/BulletPoint.global_position, velocity_component.player_node.global_position - $Visuals/BulletPoint.global_position, 7, deg_to_rad(30), 0.1)
#
#	($Abilities/ShootableComponent as ShootableComponent).shoot_projectiles($Visual/Cube/Parts.global_position, velocity_component.player_node.global_position - $Visual/Cube/Parts.global_position, 10, deg_to_rad(90), 0.05)
#
#func _on_timer_timeout():
#	is_dropping = true
#	await velocity_component.temporary_change_speed_multiplier(-1, 1)
#	drop_cube.rpc()
#
#
#func _on_time_transform_timeout():
#	if is_dropping:
#		return
#	cube_transform.rpc()
