extends Node2D

@export var health_component: Node
@export var sprite: Sprite2D


func _ready():
	var particle := $GPUParticles2D as GPUParticles2D
	particle.texture = sprite.texture
	var particle_mat := particle.material as CanvasItemMaterial
	particle_mat.particles_anim_h_frames = sprite.hframes
	particle_mat.particles_anim_v_frames = sprite.vframes
	if !multiplayer.is_server():
		return
	health_component.died.connect(on_died)
	

func on_died():
	if owner == null || not owner is Node2D:
		return

	var spawn_position = owner.global_position
	emit_particle.rpc(spawn_position)
	

# logic doc lap o client
@rpc("any_peer", "call_local", "unreliable")
func emit_particle(spawn_position : Vector2):
	var entities = get_tree().get_first_node_in_group("entities_layer").get_node("Particles")
	if get_parent() == null:
		return
	get_parent().remove_child(self)
	entities.add_child(self)
	
	global_position = spawn_position
	$AnimationPlayer.play("die")
