extends Node

# mana logoc
# use by activeable skill
# can be deplete when use skill
# increase every period of time
# low mana will darken skill UI slot
# when skill is cooldown or low mana -> still show is cooldown
# when low mana, regen enough -> update skill slot

class_name ManaComponent
@onready var timer = $RefillTimer
@onready var stats_manager : StatsManager = get_tree().get_first_node_in_group("stats_manager")
@export var player_id : int = 1
var max_mana : float
var mana_ratio : float
var current_mana : float : 
	set(v):
		v = min(v, max_mana)
		v = max(0, v)
		current_mana = v
		if max_mana <= 0:
			mana_ratio = 0
		else: mana_ratio = min(current_mana / max_mana, 1)

var mp_per_refill_time = 3

signal mana_changed(current_value : float) # for chilrend of owner node

# enemy stast
# own by host
func _ready():
	if !multiplayer.is_server():
		return
	
	# used by player character
	player_id = $"../".name.to_int()
#	GameEvents.player_level_up.connect(handle_level_up)
#	if stats_manager != null:
#		max_mana = stats_manager.get_stats(player_id, Const.ObjectTypes.Player, Const.Stats.MP)
#		current_mana = max_mana
	if owner.basic_data != null:
		max_mana = (owner.basic_data as BasicStats).MP
		current_mana = max_mana
		$RefillTimer.wait_time = (owner.basic_data as BasicStats).MP_REFILL_INTERVAL
		mp_per_refill_time = (owner.basic_data as BasicStats).MP_REFILL
		timer.timeout.connect(handle_refill_timeout)
		(owner.basic_data as BasicStats).changed.connect(handle_stats_change)


func handle_stats_change():
	max_mana = (owner.basic_data as BasicStats).MP
	mp_change(0)	

func handle_refill_timeout():
	mp_change(mp_per_refill_time)
	
#func handle_level_up(_player_id, current_level):
#	if player_id != _player_id:
#		return
#	var mp_increase_when_lv_up = max_mana*0.3 # tmp
#	print(mp_increase_when_lv_up)
#	mp_change(mp_increase_when_lv_up)
	

# when attack, there is a chance to increase mp, or heal by statue
func mp_change(amount: float): # can be positive -> increase
	if !multiplayer.is_server():
		return
	current_mana += amount
	GameEvents.emit_player_mp_change(player_id, current_mana,mana_ratio)
#	stats_manager.player_health_changed.emit(player_id, current_mana, mana_ratio)
	# local logic for children
	mana_changed.emit(current_mana)



# when level up, stats update, and hp, mp can increase
# when enter founatin -> can refill
# can use item to refill

#sol
# listen for event level up (may put in GameEvent) -> update mana
# listen for event mana_steal, life_steal, directly call this compnent to imcrease
