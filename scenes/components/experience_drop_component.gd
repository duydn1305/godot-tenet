extends Node
# own by host

#@export_range(0, 1) var drop_percent: float = .5
@export var health_component: HealthComponent
@export var exp_scene: PackedScene
var exp_gain = 0

func _ready():
	if !multiplayer.is_server():
		return
	health_component.died.connect(handle_on_died)
	if owner.basic_data != null:
		exp_gain = (owner.basic_data as BasicStats).EXP
	# todo: add coin

func handle_on_died():
	spawn_exp()
	
func spawn_exp():
	if exp_scene == null:
		return
	
	if not owner is Node2D:
		return
	
	var spawn_position = (owner as Node2D).global_position
	var exp_instance = exp_scene.instantiate() as Node2D
	exp_instance.global_position = spawn_position
	exp_instance.exp_gain = exp_gain
	var collectable_node = get_tree().get_first_node_in_group("entities_layer").get_node("Collectables")
	collectable_node.call_deferred("add_child", exp_instance, true)
