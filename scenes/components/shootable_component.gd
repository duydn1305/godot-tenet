extends Node
class_name ShootableComponent

@export var project_tile_scene : PackedScene
@onready var projectiles_node = get_tree().get_first_node_in_group("projectiles")


@export var projectile_decay : float = 1
@export var damage : float = 1
@export var penetration : int = 1
@export var bullet_speed : float = 80
@export var charge_attack_ratio : float = 0

var crit_damage = 0
var crit_rate = 0
var penetrate_ammor_rate = 0
var object_onwer_id = 1

func shoot_projectiles(init_position : Vector2, mv_vector : Vector2, n : int = 1, angle : float = 30, delay_per_shot : float = 0, enable_rotate : bool = true):
	if (n <= 1):
		shoot_a_projectile(init_position, mv_vector)
		return
	var angle_per_shot = angle/(n-1.0)
	var angle_start_from = -angle/2
	for i in n:
		var current_angle = angle_start_from + angle_per_shot * i
		if delay_per_shot > 0:
			$Timer.start(delay_per_shot)
			await $Timer.timeout
			# todo: delat
		shoot_a_projectile(init_position, mv_vector.rotated(current_angle), enable_rotate)
		
	

# animation call box cast and ranged attack
func shoot_a_projectile(init_position : Vector2, mv_vector : Vector2, enable_rotate : bool = true):
	var object = project_tile_scene.instantiate() as ProjectileComponent
#	object.mv_vector = (owner.get_node("PlayerInput").look_at_direction+owner.position).normalized()
	object.mv_vector = mv_vector.normalized()
#	object.mv_vector = Vector2.DOWN
	object.global_position = init_position
	object.damage = damage
	object.projectile_decay = projectile_decay
	object.penetration = penetration
	object.MAX_SPEED = bullet_speed
	object.charge_attack_ratio = charge_attack_ratio
	object.crit_damage = crit_damage
	object.crit_rate = crit_rate
	object.penetrate_ammor_rate = penetrate_ammor_rate
	object.object_id = object_onwer_id
	
	# todo: set decay, mv speed
	if projectiles_node != null:
		projectiles_node.add_child(object, true)
	else:
		get_tree().root.add_child(object, true)
	object.fire_out(enable_rotate)
