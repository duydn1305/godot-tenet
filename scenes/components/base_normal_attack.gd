extends Node
class_name BaseNormalAttack

@onready var velocity_component = $"../../VelocityComponent"
@onready var ability_control : BaseAbilityControl = $".."
@export var animation_player : AnimationPlayer
@export var normal_attack_anim_names : Array = ["attack-01", "attack-02", "attack-03"]
@export var box_cast_1 : BaseHitbox
@export var box_cast_2 : BaseHitbox
@export var box_cast_3 : BaseHitbox

var player_input

var attack_speed = 1: # get from stats manager
	set(new_value):
		attack_speed = new_value
		
var damage = 3 :
	set(new_value):
		damage = new_value
		if box_cast_1 != null:
			box_cast_1.damage = damage
		if box_cast_2 != null:
			box_cast_2.damage = damage
		if box_cast_3 != null:
			box_cast_3.damage = damage
			
var move_slower_ratio = 0.3

var can_attack = true

var crit_damage : float :
	set(new_value):
		crit_damage = new_value
		if box_cast_1 != null:
			box_cast_1.crit_damage = crit_damage
		if box_cast_2 != null:
			box_cast_2.crit_damage = crit_damage
		if box_cast_3 != null:
			box_cast_3.crit_damage = crit_damage

var crit_rate : float :
	set(new_value):
		crit_rate = new_value
		if box_cast_1 != null:
			box_cast_1.crit_rate = crit_rate
		if box_cast_2 != null:
			box_cast_2.crit_rate = crit_rate
		if box_cast_3 != null:
			box_cast_3.crit_rate = crit_rate

func _ready():
	set_physics_process(multiplayer.is_server())
	if !multiplayer.is_server():
		return
	# set damage
	player_input = $"../../PlayerInput"
	# init value
	if owner.basic_data != null:
		attack_speed = (owner.basic_data as BasicStats).ATTACK_SPEED
		damage = (owner.basic_data as BasicStats).DMG
		crit_damage = (owner.basic_data as BasicStats).CRIT_DMG
		crit_rate = (owner.basic_data as BasicStats).CRIT_RATE
		(owner.basic_data as BasicStats).changed.connect(handle_stats_change)
	set_object_id_from_owner()
	extend_ready()

func set_object_id_from_owner():
	if box_cast_1 != null:
		box_cast_1.object_id = owner.player_id
	if box_cast_2 != null:
		box_cast_2.object_id = owner.player_id
	if box_cast_3 != null:
		box_cast_3.object_id = owner.player_id

func handle_stats_change():
	attack_speed = (owner.basic_data as BasicStats).ATTACK_SPEED
	damage = (owner.basic_data as BasicStats).DMG
	crit_damage = (owner.basic_data as BasicStats).CRIT_DMG
	crit_rate = (owner.basic_data as BasicStats).CRIT_RATE
	extend_handle_stats_change()



func extend_handle_stats_change():
	pass

func extend_ready():
	pass
	
func _physics_process(delta):
	# 2. normal attack
#	if can_attack && ability_control.current_behavior == Const.Behave.FREE && Input.is_action_pressed("normal_attack"):
#		handle_ability()
#		return
	if can_attack && ability_control.current_behavior == Const.Behave.FREE && player_input.normal_attack:
		var rand_attack_anim = randi() % len(normal_attack_anim_names)
		handle_ability(rand_attack_anim)
		return

# auth by host
func handle_ability(rand_attack_anim : int) -> void:
	begin_ability()
	velocity_component.move_speed_multiplier -= 0.5
	# slow down
	can_attack = false
	ability_control.behaving(Const.Behave.PERFORMING_NORMAL_ATTACK)
	# pick random
#	var new_speed_scale = 1.0/attack_speed/animation_player.get_animation(normal_attack_anim_names[rand_attack_anim]).length
	var new_speed_scale = animation_player.get_animation(normal_attack_anim_names[rand_attack_anim]).length*attack_speed
	owner.play_anim.rpc(normal_attack_anim_names[rand_attack_anim], new_speed_scale)
	await animation_player.animation_finished
	ability_control.behaving(Const.Behave.FREE)
	can_attack = true
	velocity_component.move_speed_multiplier += 0.5
	end_ability()
	

# abstract
func begin_ability():
	pass
	

func end_ability():
	pass

# animation call box cast and ranged attack
func character_perform_action():
	pass
	
