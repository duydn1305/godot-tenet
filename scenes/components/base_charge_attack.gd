extends Node
class_name BaseChargeAttack

@export var animation_player : AnimationPlayer
@onready var ability_control : BaseAbilityControl = $".."
@onready var velocity_component : VelocityComponent = $"../../VelocityComponent"

var progress_value_tween : Tween
@export var progress_bar : ProgressBar
@export var box_cast : BaseHitbox

# first value when begin to charge
var init_charging_power_ratio = 0.1
# value charged. when animation call, get this value * damage
var last_charged_power_ratio = 0
#var attack_speed = 1.2 # get from stats manager
var attack_speed = 1 # get from stats manager
var can_attack = true

var damage = 0 :
	set(new_value):
		damage = new_value
		if box_cast != null:
			box_cast.damage = new_value

var crit_damage : float :
	set(new_value):
		crit_damage = new_value
		if box_cast != null:
			box_cast.crit_damage = crit_damage

var crit_rate : float :
	set(new_value):
		crit_rate = new_value
		if box_cast != null:
			box_cast.crit_rate = crit_rate
			
func _ready():
	set_physics_process(multiplayer.is_server())
	if !multiplayer.is_server():
		return
	if owner.basic_data != null:
		attack_speed = (owner.basic_data as BasicStats).ATTACK_SPEED
		damage = (owner.basic_data as BasicStats).DMG
		crit_damage = (owner.basic_data as BasicStats).CRIT_DMG
		crit_rate = (owner.basic_data as BasicStats).CRIT_RATE
		(owner.basic_data as BasicStats).changed.connect(handle_stats_change)
	extend_ready()
	set_object_id_from_owner()

func set_object_id_from_owner():
	if box_cast != null:
		box_cast.object_id = owner.player_id

func handle_stats_change():
	attack_speed = (owner.basic_data as BasicStats).ATTACK_SPEED
	damage = (owner.basic_data as BasicStats).DMG
	crit_damage = (owner.basic_data as BasicStats).CRIT_DMG
	crit_rate = (owner.basic_data as BasicStats).CRIT_RATE
	extend_handle_stats_change()



func extend_handle_stats_change():
	pass

func extend_ready():
	pass
	
# bool can_attack, can_charge, can_use_skill_q, w, e
# process user input for controlling character action: normal attack, charge attack, skill q, e, r and worskip skill f
func _physics_process(delta):
#	var strength = Input.get_action_strength("charge_attack")
	var strength = $"../../PlayerInput".charge_attack_strength
	var charge_attack_hold = strength > 0.5
	
	# 1. charge attack
	# free -> charging -> perform charge attack
	if can_attack && ability_control.current_behavior == Const.Behave.FREE:
		if charge_attack_hold:
			handle_begin_to_charge()
			return
	if can_attack && ability_control.current_behavior == Const.Behave.CHARGING:
		if !charge_attack_hold:
			handle_base_charge_attack()
			return

# CHARGE ATTACK
func handle_begin_to_charge():
	begin_to_charge()
	velocity_component.move_speed_multiplier -= 0.5 # temporary
	ability_control.behaving(Const.Behave.CHARGING)
	progress_value_tween = get_tree().create_tween().set_loops()
	# The interpolation starts slowly and speeds up towards the end.
	progress_value_tween.tween_property(progress_bar, "value", 1, 1).set_ease(Tween.EASE_IN).set_trans(Tween.TRANS_SINE)
	progress_value_tween.tween_property(progress_bar, "value", init_charging_power_ratio, 2).set_ease(Tween.EASE_OUT).set_trans(Tween.TRANS_SINE)
	progress_bar.visible = true

func handle_base_charge_attack():
	# end charge
	end_to_charge()
	begin_ability()
	ability_control.behaving(Const.Behave.PERFORMING_CHARGE_ATTACK)
	progress_value_tween.kill()
	last_charged_power_ratio = progress_bar.value
	var new_speed_scale = animation_player.get_animation('charge-attack').length*attack_speed
	owner.play_anim.rpc("charge-attack", new_speed_scale)
	progress_bar.value = init_charging_power_ratio
	progress_bar.visible = false
	await animation_player.animation_finished
	ability_control.behaving(Const.Behave.FREE)
	velocity_component.move_speed_multiplier += 0.5 # temporary
	end_ability()
	
	

# abstract
#func character_charge_attack(power_ratio):
#	pass
# abstract
func begin_to_charge():
	pass
	

func end_to_charge():
	pass


func begin_ability():
	pass
	

func end_ability():
	pass

# animation call box cast and ranged attack
func character_perform_action():
	# set charge attack ratio
	box_cast.charge_attack_ratio = last_charged_power_ratio
