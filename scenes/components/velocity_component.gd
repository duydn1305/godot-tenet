extends Node

class_name VelocityComponent

@export var MAX_SPEED : float = 40
@export var ACCELEBRATION : float = 5
var move_speed_multiplier = 1
var velocity = Vector2.ZERO
var is_stop = false

var player_node : Node2D
@onready var owner_node : Node2D  = owner
var basic_data : BasicStats
func _ready():
	if owner.basic_data != null:
		MAX_SPEED = (owner.basic_data as BasicStats).MOVE_SPEED
		ACCELEBRATION = (owner.basic_data as BasicStats).ACCELEBRATION
		(owner.basic_data as BasicStats).changed.connect(handle_stats_change)


func handle_stats_change():
	MAX_SPEED = (owner.basic_data as BasicStats).MOVE_SPEED
	ACCELEBRATION = (owner.basic_data as BasicStats).ACCELEBRATION

enum PlayerRankType {Random, Nearest, LowestHP, LowestHPPercent, LowestMP, StrongestAD, StrongestAP, StrongestAmror, Statue}
# function for enemies
# EnemyTargetController, trong đó có các option
#- Nearest
#- Lowest HP
#- Lowest HP Percent
#- Lowest MP
#- Strongest AD
#- Strongest AP
#- Strongest Armor
func find_player_by_rank_type(rank_type : PlayerRankType):
	if owner_node == null:
		return
	match rank_type:
		PlayerRankType.Nearest:
			var min_dist = 999999.0
			for tmp_player_node in get_tree().get_nodes_in_group("player_alive"):
				var dist = tmp_player_node.global_position.distance_squared_to(owner_node.global_position)
				if dist < min_dist:
					min_dist = dist
					player_node = tmp_player_node
		PlayerRankType.Statue:
			var statue = get_tree().get_first_node_in_group("statue")
			player_node = statue
		_:
			# todo: filter out died player
			player_node = get_tree().get_nodes_in_group("player_alive").pick_random()

func accelebrate_to_player():
	# check if player exist
	if player_node == null:
		return Vector2.ZERO
	# check owner exist
	var owner_2d = owner as Node2D
	if owner_2d == null:
		return Vector2.ZERO

	var direction = (player_node.global_position - owner_2d.global_position).normalized()
	accelebrate_in_direction(direction)

func deccelebrate():
	velocity = velocity.lerp(Vector2.ZERO, 1-exp(-get_process_delta_time()*ACCELEBRATION))
	

func accelebrate_in_direction(direction : Vector2):
	var desire_v = direction.normalized()*MAX_SPEED*move_speed_multiplier
	velocity = velocity.lerp(desire_v, 1-exp(-get_process_delta_time()*ACCELEBRATION))
	
	
func stop_instantly():
	velocity = Vector2.ZERO
	
	
func move(charcter_body: CharacterBody2D):
	if is_stop:
		return
	charcter_body.velocity = velocity
	charcter_body.move_and_slide()
	velocity = charcter_body.velocity


func increase_max_speed_multiplier(p):
	move_speed_multiplier += p
	
func temporary_change_speed_multiplier(p : float, t : float):
	increase_max_speed_multiplier(p)
	$TemporarySpeed.start(t)
	await $TemporarySpeed.timeout
	increase_max_speed_multiplier(-p)
