extends Node2D
@onready var health_component : HealthComponent = $"../HealthComponent"
@onready var damaged_audio = $DamagedAudio
@onready var heal_audio = $HealAudio
@onready var level_up_audio = $LevelUpAudio
@onready var moving_audio = $MovingAudio
@export var animation_player : AnimationPlayer
@onready var attack = $Attack
@onready var charge_attack = $ChargeAttack
@onready var q = $Q

# note when set up sound
# 0. source audio from 1-3 item is okay
# 1. max disctanct 100-150 is okay
# 2. remember to connect hp component
# 3. host controll audio -> rpc for client


var turn_off_for_host = false # testing purpose
var _is_server = false
func _ready():
	if !multiplayer.is_server():
		return
	_is_server = true
	health_component.health_changed.connect(handle_heath_changed)
	GameEvents.player_level_up.connect(handle_level_up)
	$Timer.timeout.connect(handle_timer_timout)
	
func _exit_tree():
	if _is_server: GameEvents.player_level_up.disconnect(handle_level_up)
	

func handle_heath_changed(current, amount, ratio):
	if amount < 0: play_sound.rpc("hurt")
	if amount > 0: play_sound.rpc("heal")

func handle_level_up(player_id : int, which_level : int):
	if player_id == owner.player_id:
		play_sound.rpc("lvup")

func handle_timer_timout():
	if animation_player.current_animation == "walk":
		play_sound.rpc("walk")
		
@rpc("any_peer", "call_local")
func play_sound(what_sound):
	if turn_off_for_host && multiplayer.get_unique_id() == 1: return
	match what_sound:
		"hurt": damaged_audio.play_random()
		"heal": heal_audio.play_random()
		"lvup": level_up_audio.play_random()
		"walk": moving_audio.play_random()
		"attack": attack.play_random()
		"charge_attack": charge_attack.play_random()
		"q": q.play_random()
		
	
