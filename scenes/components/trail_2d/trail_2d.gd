@tool
class_name Trail2D
extends Line2D

## Minimum distance that parent of this node need to move
## in one frame to spawn a trail
@export var move_distance := 12.0:
	set(value):
		move_distance = value
		_distance = value**2

## Life time of a trail in milliseconds (ms)
@export_range(10, 5000, 10) var life_time := 150

var _life_time: Array[float] = []

@onready var _distance := move_distance**2

func _enter_tree() -> void:
	if Engine.is_editor_hint():
		update_configuration_warnings()
	points.clear()
	_life_time.clear()

func _get_configuration_warnings() -> PackedStringArray:
	var parent := get_parent() as Node2D
	return [] if parent else ["Expect a Node2D parent"]

func _process(_delta: float) -> void:
	var parent := get_parent() as Node2D
	if parent == null: return

	global_transform = Transform2D()

	var tick := Time.get_ticks_msec()
	if _life_time.size() > 0 and tick - _life_time[0] > life_time:
		remove_point(0)
		_life_time.remove_at(0)

	var pos := parent.global_position
	if get_point_count() == 0 or pos.distance_squared_to(points[-1]) >= _distance:
		add_point(pos)
		_life_time.append(tick)
