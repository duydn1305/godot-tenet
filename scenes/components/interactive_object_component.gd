extends Node

@onready var object = $".."
@onready var animation_player = $"../AnimationPlayer"

signal object_selected()
var player_nearby = false

func _ready():
	object.connect("body_entered", player_entering)
	object.connect("body_exited", player_exiting)
	
func player_entering(_body):
	print("player_entering")
	player_nearby = true
	animation_player.play("body_enter")
	
func player_exiting(_body):
	animation_player.play("RESET")
	print("player_exiting")
	player_nearby = false

func _unhandled_input(event):
	if event.is_action_pressed("interact_with_object") && player_nearby:
		get_tree().root.set_input_as_handled()
		animation_player.play("selected")
		await animation_player.animation_finished
		animation_player.play("RESET")
		object_selected.emit()
