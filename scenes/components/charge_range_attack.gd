extends BaseChargeAttack
class_name RangeChargeAttack

@export var shootable_component : ShootableComponent
@export var fire_out_point_a : Marker2D
@export var fire_out_point_b : Marker2D

func extend_ready(): # call by parent
	if owner.basic_data != null:
		shootable_component.damage = (owner.basic_data as BasicStats).DMG
		shootable_component.projectile_decay = (owner.basic_data as BasicStats).BULLET_DECAY
		shootable_component.penetration = (owner.basic_data as BasicStats).BULLET_PENETRATION
		shootable_component.bullet_speed = (owner.basic_data as BasicStats).BULLET_SPEED
		shootable_component.damage = damage
		shootable_component.crit_damage = crit_damage
		shootable_component.crit_rate = crit_rate
	shootable_component.object_onwer_id = owner.player_id
	
func extend_handle_stats_change():
	shootable_component.damage = damage
	shootable_component.crit_damage = crit_damage
	shootable_component.crit_rate = crit_rate
	
func begin_to_charge():
	owner.play_anim.rpc("charge")

# animation call box cast and ranged attack
func character_perform_action():
	if !multiplayer.is_server():
		return
	# set charge attack ratio
	shootable_component.charge_attack_ratio = last_charged_power_ratio
	
	var mv_vector = (fire_out_point_b.global_position-fire_out_point_a.global_position).normalized()
	var init_pos = owner.position
	if fire_out_point_b != null: init_pos = fire_out_point_b.global_position
	shootable_component.shoot_a_projectile(init_pos, mv_vector)
#	
