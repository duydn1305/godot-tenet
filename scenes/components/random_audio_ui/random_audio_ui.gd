class_name RandomAudioUI
extends AudioStreamPlayer

@export_group("General")
@export var control_nodes: Array[Control]
@export var hover_audio: RandomAudioData
@export var left_audio: RandomAudioData
@export var focus_audio: RandomAudioData

@export_group("Button")
@export var button_nodes: Array[Button]
@export var click_audio: RandomAudioData

var _base_pitch_scale: float

func _ready() -> void:
	_base_pitch_scale = pitch_scale
	for control in control_nodes:
		if !control: continue
		if hover_audio and !hover_audio.sounds.is_empty():
			control.mouse_entered.connect(play_on_hover)
		if left_audio and !left_audio.sounds.is_empty():
			control.mouse_exited.connect(play_on_left.bind(control))
		if focus_audio and !focus_audio.sounds.is_empty():
			control.focus_entered.connect(play_on_focus)

	for button in button_nodes:
		if !button: continue
		if click_audio and !click_audio.sounds.is_empty():
			button.pressed.connect(play_on_click)

func play_random(random_data: RandomAudioData) -> void:
	stream = random_data.sounds.pick_random()
	pitch_scale = randf_range(
			_base_pitch_scale * (1.0 - random_data.pitch_variant),
			_base_pitch_scale * (1.0 + random_data.pitch_variant))
	play()

func play_on_hover() -> void:
	play_random(hover_audio)

func play_on_left(control: Control) -> void:
	if Rect2(Vector2(), control.size).has_point(control.get_local_mouse_position()): return
	play_random(left_audio)

func play_on_focus() -> void:
	play_random(focus_audio)

func play_on_click() -> void:
	play_random(click_audio)
