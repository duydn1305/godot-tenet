extends Node2D

# FILE LOGIC: 
# create struct PlayerInput holding data input of each player
# move, attack, charge attack, mouse looking at (useful for archer)
# skill q, e, r, space
# rpc accept dict -> function to convert struct to dict
# SYNC FLOW:
# 1. this node is authorized by player
# 2. send data to host
# 3. host receive player input and resend to all client of this node path (sync for current player)
# 4. this node in server-side will process using this input (for consistence)
# 5. also in client-side, store it to avoid jitter


class PlayerInput:
	var move_direction = Vector2()
	var normal_attack : int = 0
	var charge_attack_strength = 0.0
	var look_at_direction = Vector2()
	var skill_press = {
		Const.ActiveSkill.Q: false,
		Const.ActiveSkill.E: false,
		Const.ActiveSkill.R: false,
		Const.ActiveSkill.Space: false,
	}
	
	static func get_default() -> PlayerInput:
		return PlayerInput.new(Vector2.ZERO, false, 0, Vector2.ZERO)
	
	func _init(
		_move_direction : Vector2, 
		_normal_attack : int, 
		_charge_attack_strength : float, 
		_look_at_direction : Vector2, 
		skill_q : bool = false,
		skill_e : bool = false,
		skill_r : bool = false,
		skill_space : bool = false):
		move_direction = _move_direction
		normal_attack = _normal_attack
		charge_attack_strength = _charge_attack_strength
		look_at_direction = _look_at_direction
		skill_press[Const.ActiveSkill.Q] = skill_q
		skill_press[Const.ActiveSkill.E] = skill_e
		skill_press[Const.ActiveSkill.R] = skill_r
		skill_press[Const.ActiveSkill.Space] = skill_space
	
	func to_dict() -> Dictionary:
		return {
			"move_direction": move_direction,
			"normal_attack": normal_attack,
			"charge_attack_strength": charge_attack_strength,
			"look_at_direction": look_at_direction,
			"skill_press": skill_press
		}
	static func parse_from_dict(data_dict : Dictionary) -> PlayerInput:
		var _move_direction
		var _normal_attack
		var _charge_attack_strength
		var _look_at_direction
		var _skill_press
		var _q
		var _e
		var _r
		var _space
		if data_dict.has("move_direction"): _move_direction = data_dict["move_direction"]
		if data_dict.has("normal_attack"): _normal_attack = data_dict["normal_attack"]
		if data_dict.has("charge_attack_strength"): _charge_attack_strength = data_dict["charge_attack_strength"]
		if data_dict.has("look_at_direction"): _look_at_direction = data_dict["look_at_direction"]
		if data_dict.has("skill_press"): 
			_skill_press = data_dict["skill_press"] as Dictionary
			if _skill_press.has(Const.ActiveSkill.Q): _q = _skill_press[Const.ActiveSkill.Q]
			if _skill_press.has(Const.ActiveSkill.E): _e = _skill_press[Const.ActiveSkill.E]
			if _skill_press.has(Const.ActiveSkill.R): _r = _skill_press[Const.ActiveSkill.R]
			if _skill_press.has(Const.ActiveSkill.Space): _space = _skill_press[Const.ActiveSkill.Space]
		return PlayerInput.new(_move_direction, _normal_attack, _charge_attack_strength, _look_at_direction, _q, _e, _r, _space)

var player_input = PlayerInput.get_default()


func check_master_network():
	var is_node_master = get_multiplayer_authority() == multiplayer.get_unique_id()
	print("is_node_master: ", is_node_master)
	print("multiplauuer auth: ", get_multiplayer_authority(), ", uniq id: ", multiplayer.get_unique_id())
	set_process(is_node_master)


func _ready():
	check_master_network()
#	set_process(false)	
#	await get_tree().create_timer(2).timeout
	
#	if is_node_master:
#		print("can set master for node: ", get_multiplayer_authority())
	# Only process for the local player
#	print("auth: ", get_multiplayer_authority(), "unique id: ", multiplayer.get_unique_id())


# player input: client hit keyboard -> call server to sync input

# server call this function to sync data for all client of this node
@rpc("call_local", "any_peer")
func sync_player_input(data_dict : Dictionary):
	player_input = PlayerInput.parse_from_dict(data_dict)
	

# server: process -> notify other client
@rpc("call_local")
func request_host_sync_input(data_dict : Dictionary):
	# server sync data for all client
	sync_player_input.rpc(data_dict)
	

# process by the node master
func _process(delta):
	# Get the input move_direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var _move_direction = Input.get_vector("move_left", "move_right", "move_up", "move_down")
	var _normal_attack = Input.is_action_pressed("normal_attack")
	var _charge_attack_strength = Input.get_action_strength("charge_attack")
	var _look_at_direction = get_local_mouse_position().normalized()
	var skill_q_press = Input.is_action_pressed("skill_q")
	var _q = Input.is_action_pressed("skill_q")
	var _e = Input.is_action_pressed("skill_e")
	var _r = Input.is_action_pressed("skill_r")
	var _space = Input.is_action_pressed("skill_space")
	var _player_input = PlayerInput.new(_move_direction, _normal_attack, _charge_attack_strength, _look_at_direction, _q, _e, _r, _space)
	
	var _player_input_dict = _player_input.to_dict()
	# avoid jitter
	player_input = _player_input
	# call server to compute
	request_host_sync_input.rpc_id(1, _player_input_dict)
	
#	if direction != Vector2.ZERO:
#		print("node want to move: ", get_multiplayer_authority())
