class_name RandomAudioData
extends Resource

@export var sounds: Array[AudioStream]
@export_range(0, 0.99) var pitch_variant := 0.2
