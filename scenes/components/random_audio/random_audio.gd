class_name RandomAudio
extends AudioStreamPlayer2D

@export var data: RandomAudioData

var _base_pitch_scale: float

func _ready() -> void:
	_base_pitch_scale = pitch_scale

func play_random() -> void:
	if !data or data.sounds.is_empty(): return
	stream = data.sounds.pick_random()
	pitch_scale = randf_range(
			_base_pitch_scale * (1.0 - data.pitch_variant),
			_base_pitch_scale * (1.0 + data.pitch_variant))
	play()
