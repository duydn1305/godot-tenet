extends Node2D
class_name BaseAbilityControl
@onready var character_sound_componet = $"../CharacterSoundComponet"

var current_behavior : Const.Behave

func _ready():
	current_behavior = Const.Behave.FREE

# listen to level up signal to unlock passive/active skill

func behaving(new_state : Const.Behave):
	if current_behavior == new_state:
		return
	if new_state == Const.Behave.PERFORMING_NORMAL_ATTACK:
		character_sound_componet.play_sound.rpc("attack")
	if new_state == Const.Behave.PERFORMING_CHARGE_ATTACK:
		character_sound_componet.play_sound.rpc("charge_attack")
	if new_state == Const.Behave.SKILL:
		print("change state roi")
		character_sound_componet.play_sound.rpc("q")
		
#	behavior_change.emit(current_behavior, new_state)
	current_behavior = new_state
	
# may place this logic in another file: AbilityManager: crud skill
# ability manager can receive user GUI input: esc, mouse select item
func remove_artifact():
	# remove from node Passives
	# update ui
	pass

# can be artifact, upgrade
func add_new_passive():
	# add scene to node Passives
	# update ui	
	pass
	
# add new space skill():
func worship_statue():
	# remove old skill
	# add new skill
	# update ui
	pass

# ???????????????????
# use artifact logic: user press ecs -> inventory tab -> click item -> reassign
# authorize user to use UI, click, F to interact with item, portal, item

# todo: level up manager
# achievement manager
# autoload to get/save meta progression data -> resource id
# ability -> resource id
# abilti by character -> reousce id
# need a place to check current max hp, current damage of player_id -> StatsManager
# need a place to check attribute of upgrade_id -> UpgradeManager (AbilityManager), REousefcManager

# data flow: client connect -> request server to store his meta data (StatsManager)
# host convert reousce id -> ability

# use case: lv up, deplete hp, increease damage
# lv up -> ExpManager store player_id exp, lv, signal level_up -> update up, increaes hp, activate artifact
# deplete/inc current hp: occur in hurtbox -> hp_component
# lvup -> increase max_hp: update first in StatsManager -> update UI, update hp component (use signal)
# increase damage: update first in StatsManager -> broadcast signal dmg change (for who) -> 
# 1. BaseAbilityControl listen to it, call function child
# 2. BaseNormalAtk, BaseChargeAtk, BaseSkill... listen from signal then update damage for boxcast

# when new passive is added (e.x increase exp gain): emit signal exp_gain_change
