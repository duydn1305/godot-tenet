extends Node2D
@onready var respawn = $Respawn
@onready var die = $Die

func _ready():
	GameEvents.player_die.connect(handle_player_die)
	GameEvents.player_respawn.connect(handle_player_respawn)
 
# test
#func _unhandled_input(event):
#	if event is InputEventMouse:
#		if event.is_action("normal_attack"):
#			handle_player_die(1)
#
#		if event.is_action("charge_attack"):
#			handle_player_respawn(1)

func handle_player_die(die_player_id):
#	print("handle_player_die")
	if owner.player_id == die_player_id:
		owner.remove_from_group("player_alive")
		owner.add_to_group("player_died")
		die.play_random()
		owner.visible = false
		
func handle_player_respawn(respawn_player_id):
	if owner.player_id == respawn_player_id:
		owner.remove_from_group("player_died")
		owner.add_to_group("player_alive")
		owner.visible = true
		if is_inside_tree():
			await get_tree().create_timer(1).timeout	
			respawn.play_random()
			animate_ray_respawn()

func animate_ray_respawn():
	$Ray.visible = true
	var t = create_tween() as Tween
	($Ray.material as ShaderMaterial).set_shader_parameter("cutoff", 0.3)
	t.tween_property($Ray.material, "shader_parameter/cutoff", 0.13, 2).set_ease(Tween.EASE_OUT).set_trans(Tween.TRANS_CUBIC)
	t.tween_interval(2)
	t.tween_callback(func(): $Ray.visible = false)
