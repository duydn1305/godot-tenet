extends Area2D
class_name  BaseHurtbox

# hurt box for living objects
# seeking for hitbox area: hero attack/bullet - enemies attack/bullet

@export var health_component: HealthComponent

var object_id : int

var ammor : float = 0:
	set(new_value):
		ammor = new_value
		if new_value < 0:
			ammor = 0
			
var evade = 0.0 # 0 to 1

func _ready():
	if !multiplayer.is_server():
		return
	area_entered.connect(on_area_entered)
	if owner.basic_data != null:
		ammor = (owner.basic_data as BasicStats).AMMOR
		evade = (owner.basic_data as BasicStats).EVADE
		(owner.basic_data as BasicStats).changed.connect(handle_stats_change)

func handle_stats_change():
	evade = (owner.basic_data as BasicStats).EVADE
	ammor = (owner.basic_data as BasicStats).AMMOR

func on_area_entered(other_area: Area2D):	
	var hitbox_component = other_area as BaseHitbox
	if hitbox_component == null:
		return
	if hitbox_component.has_method("object_collide"):
		if !(hitbox_component.tween as Tween).is_running():
			return
		hitbox_component.object_collide()
#	if not other_area is BaseHitbox:
#		return
	if randf() < evade:
		GameEvents.spawn_floating_text_str.rpc("evade", global_position, Color.BLUE_VIOLET) # just spawm for client
		return	
	var charge_attack_ratio = hitbox_component.charge_attack_ratio
	var final_damage = hitbox_component.damage
	# if charge_atk
	var is_crit = false
	if randf() < hitbox_component.crit_rate:
		final_damage = final_damage*(1+hitbox_component.crit_damage)
		is_crit = true
	if charge_attack_ratio > 0:
		# has charge ratio
		final_damage = final_damage*(1+charge_attack_ratio)
	# ammor
	if randf() < hitbox_component.penetrate_ammor_rate:
		pass
	else: final_damage = final_damage*(100.0/(100.0 + ammor))
	# after compute tons of equation
	var killer = 1
#	if hitbox_component.owner.get_node("HealthComponent")
	health_component.hp_change(-final_damage, hitbox_component.object_id) # procees by host
	if is_crit: GameEvents.spawn_floating_text.rpc(final_damage, global_position, Color.CRIMSON) # just spawm for client
	else: GameEvents.spawn_floating_text.rpc(final_damage, global_position) # just spawm for client
	
