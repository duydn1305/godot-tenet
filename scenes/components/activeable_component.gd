extends Node
class_name BaseActiveAbility

# meta data info: set by resource
# if positive then has cooldown
#var cooldown_seconds = 5

# if positive then has activating time
#var lasting_seconds = 0

# if positive then has time to perform animation
#var time_to_perform = 0

#var mp_cost = 10.0

var time_remain = 0

@export var active_by_key : Const.ActiveSkill


@onready var ability_control : BaseAbilityControl = $".."
@onready var velocity_component : VelocityComponent = $"../../VelocityComponent"
@onready var mana_component : ManaComponent = $"../../ManaComponent"
#@onready var player_input_node : Node2D = owner.get_node("PlayerInput")
@onready var player_input_node  = $"../../PlayerInput"

# need to store player_id of this node

#var key_mapping = {
#	Const.ActiveSkill.Q: "skill_q",
#	Const.ActiveSkill.E: "skill_e",
#	Const.ActiveSkill.R: "skill_r",
#	Const.ActiveSkill.Space: "skill_f",
#}
#
#@onready var active_by_key_mapping = key_mapping[active_by_key]
var skill_is_refilled = true
var player_id : int
var skill_data : BasicStats

func can_use_skill():
#	return skill_is_refilled && mana_component.current_mana >= mp_cost
	return skill_data != null &&skill_is_refilled && mana_component.current_mana >= skill_data.MP_COST

func is_skill_press():
	return player_input_node.skill_press[active_by_key]
	
func _ready():
	set_physics_process(multiplayer.is_server())
	if !multiplayer.is_server():
		return 
	$Cooldown.connect("timeout", handle_count_down.bind(1))
	player_id = owner.player_id
	if owner.basic_data != null:
		if active_by_key == Const.ActiveSkill.Q:
			skill_data = owner.basic_data.skill_q
		if active_by_key == Const.ActiveSkill.E:
			skill_data = owner.basic_data.skill_e
		if active_by_key == Const.ActiveSkill.R:
			skill_data = owner.basic_data.skill_r
	extend_ready()

func extend_ready():
	pass

func _physics_process(delta):
	if can_use_skill() && ability_control.current_behavior == Const.Behave.FREE && is_skill_press():
		handle_ability()
		start_timer_counting()
		return

# client also similate
func handle_ability():
	skill_is_refilled = false
	mana_component.mp_change(-skill_data.MP_COST)
	ability_control.behaving(Const.Behave.SKILL)
	# 1. perform
	await perform_ability_logic()
	# 2. release behavior for ot her action
	ability_control.behaving(Const.Behave.FREE)
	
	# 3. start to cooldown for all client
#	$Cooldown.start(cooldown_seconds)
#	await $Cooldown.timeout

# called by host
func start_timer_counting():
#	time_remain = cooldown_seconds
	time_remain = skill_data.COOLDOWN
	handle_count_down(0)
	$Cooldown.start(1)

# host control
# called by timer
# called by artifact: -0.5 cooldown when attack
func handle_count_down(s : float):
	time_remain -= s
	# call ui to update
	# update ui privately
	# if is host
	GameEvents.emit_player_skill_cooldown(player_id, active_by_key, skill_data.COOLDOWN, time_remain)
	if time_remain <= 0:
		$Cooldown.stop()
		skill_is_refilled = true
	

func perform_ability_logic():
	pass
#
## abstract
#func handle_level_up(which_level):
#	if !can_use_skill:
#		return
#
#
## abstract
#func handle_out_of_combat():
#	if !can_use_skill:
#		return
#
## abstract
#func handle_hp_lost():
#	if !can_use_skill:
#		return
#	can_use_skill = false
#	if cooldown_seconds > 0:
#		$Timer.start(cooldown_seconds)
#		await $Timer.timeout
#	can_use_skill = true	

# abstract: 


# teleport
# animation
# position = get new position

# dash
# get move component:
# boost speed in 0.2
