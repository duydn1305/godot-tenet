extends BaseNormalAttack

class_name RangeAttack
@export var shootable_component : ShootableComponent
@export var fire_out_point_a : Marker2D
@export var fire_out_point_b : Marker2D

func extend_ready(): # call by parent
	if owner.basic_data != null:
		shootable_component.damage = (owner.basic_data as BasicStats).DMG # get data from basic_data
		shootable_component.projectile_decay = (owner.basic_data as BasicStats).BULLET_DECAY # get data from basic_data
		shootable_component.penetration = (owner.basic_data as BasicStats).BULLET_PENETRATION # get data from basic_data
		shootable_component.bullet_speed = (owner.basic_data as BasicStats).BULLET_SPEED # get data from basic_data
		shootable_component.damage = damage
		shootable_component.crit_damage = crit_damage
		shootable_component.crit_rate = crit_rate
	shootable_component.object_onwer_id = owner.player_id

func extend_handle_stats_change():
	shootable_component.damage = damage
	shootable_component.crit_damage = crit_damage
	shootable_component.crit_rate = crit_rate
	

# animation call box cast and ranged attack
func character_perform_action():
	if !multiplayer.is_server():
		return
	show_an_arrow()
#	shootable_component.shoot_projectiles(init_pos, mv_vector, 3, deg_to_rad(90))
#	shootable_component.shoot_projectiles(init_pos, mv_vector, 10, deg_to_rad(90), 0)

func show_an_arrow():
	var mv_vector = (fire_out_point_b.global_position-fire_out_point_a.global_position).normalized()
	var init_pos = owner.position
	if fire_out_point_b != null: init_pos = fire_out_point_b.global_position
	shootable_component.shoot_a_projectile(init_pos, mv_vector)

func shot_n_arrow(n : int, deg : float):
	var mv_vector = (fire_out_point_b.global_position-fire_out_point_a.global_position).normalized()
	var init_pos = owner.position
	if fire_out_point_b != null: init_pos = fire_out_point_b.global_position
	shootable_component.shoot_projectiles(init_pos, mv_vector, n, deg_to_rad(deg))
	
