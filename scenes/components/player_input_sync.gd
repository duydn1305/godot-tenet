extends MultiplayerSynchronizer

@export var move_direction = Vector2()
@export var normal_attack : bool
@export var charge_attack_strength = 0.0
@export var look_at_direction = Vector2()
@export var skill_press = {
	Const.ActiveSkill.Q: false,
	Const.ActiveSkill.E: false,
	Const.ActiveSkill.R: false,
	Const.ActiveSkill.Space: false,
}
@export var interact : bool
@export var hp_componennt : HealthComponent
var network_player_id
func check_master_network():
	var is_node_master = get_multiplayer_authority() == multiplayer.get_unique_id()
#	print("is_node_master: ", is_node_master)
#	print("multiplauuer auth: ", get_multiplayer_authority(), ", uniq id: ", multiplayer.get_unique_id())
	set_process(is_node_master)
	set_process_unhandled_input(is_node_master)
	if is_node_master:
		GameEvents.player_die.connect(handle_died)
		GameEvents.player_respawn.connect(handle_respawn)
		network_player_id = multiplayer.get_unique_id()

func handle_died(player_id):
	if player_id == network_player_id:
		set_process(false)
		set_process_unhandled_input(false)
	
	
func handle_respawn(player_id):
	if player_id == network_player_id:
		set_process(true)
		set_process_unhandled_input(true)
	

func _unhandled_input(event):
	
	if event is InputEventKey:
		skill_press[Const.ActiveSkill.Q] = event.is_action_pressed("skill_q")
		skill_press[Const.ActiveSkill.E] = event.is_action_pressed("skill_e")
		skill_press[Const.ActiveSkill.R] = event.is_action_pressed("skill_r")
		skill_press[Const.ActiveSkill.Space] = event.is_action_pressed("skill_space")
		interact = event.is_action_pressed("interact_with_object")
	if event is InputEventMouse:
		if event.is_action("normal_attack"):
			normal_attack = event.is_action_pressed("normal_attack")
	
			
func _ready():
	check_master_network()

# process by the node master
func _process(_delta):
	# Get the input move_direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	move_direction = Input.get_vector("move_left", "move_right", "move_up", "move_down")
#	var _normal_attack = Input.is_action_pressed("normal_attack")
	charge_attack_strength = Input.get_action_strength("charge_attack")
	look_at_direction = (get_parent() as Node2D).get_local_mouse_position()#.normalized()
#	var _q = Input.is_action_pressed("skill_q")
	# avoid jitter
