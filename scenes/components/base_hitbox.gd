extends Area2D
class_name BaseHitbox

# in case bullet, init outside onwer!, it will be set stats by its spawner
# bullet may be a new class FlyingHitBox -> can fly in a direction (arrow, bullet, ball, enemy bullet)
# attack box: normal attack, charge attack, bullet, area damage
# stats for this attack box: 
var damage = 0
var crit_rate = 0
var crit_damage = 0  # if crit, final damage = damage * (1+crit_damage)
var stun_chance = 0.1 
var charge_attack_ratio = 0.0

var damage_on_loss_hp = 0.2 # if hp loss 70% -> new damage = damage * (1 + 0.7 * 0.2)
var penetrate_ammor_rate = 0.0 # rate
var object_id = 1
# when hit target by charge attack/normal attack/skill with box: heal or +mp
# if hp_remain <= 0, target die 
signal hit_target(type, final_damage, hp_remain) # may public this event. this event is own by each computer (same level as multiplayer event)

# auto load multiplayer event: connect, disconnect, peer_connected, ... 

# A. spawn enemy: host init position + stats -> sync rpc spawn -> host control enemies
# hurt box enemy: host control
# B. box cast: host control -> grpc for all puppet enemies -> hurt box of client recieved

# C. how if player hit in multi player
# hurt box of player collide enemy hit box (enemy hit character)
# client control player
# hurt box player: client control
# player hp component deplete -> sync remote private signal
# update hp when player lost in all computer


# D. how player know they hit target: auto load event hit_target
# host/client: hurtbox enemy will call rpc_id to emit this signal for the owner of the attack


# E. passive ability component
# deal extra damage base on missing health of target
# deal extra damage base on ammor point
# marked enemies to recieve more damage from all source
# deal more damage to a specific target's class

# F. elemental
# slow, stun, fire, cursed ammor, cursed weapon


# G. hitbox
# 1. normal may has more than 1 hit box
# 2. charge attack may reuse these hit box
# 3. bullet, arrow init it own hit box
# need a way to precompute bullet data -> faster init
# 4. each skill may has a unique hit box
# hitbox must be created in editor
