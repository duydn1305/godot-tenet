extends Node

var charging = false

signal begin_charging()
signal stop_charging()
var progress_value_tween : Tween

@onready var progress_bar = $ProgressBar

func _physics_process(delta):
	var strength = Input.get_action_strength("charge_attack")
	if not charging:
		if strength > 0.5:
			charging = true
			begin_charging.emit()
	else:
		if strength < 0.5:
			charging = false
			stop_charging.emit()


func _ready():
	begin_charging.connect(handle_begin_charging)
	stop_charging.connect(handle_stop_charging)
	
func handle_begin_charging():
	progress_value_tween = get_tree().create_tween().set_loops()
	progress_value_tween.tween_property(progress_bar, "value", 1, 2).set_ease(Tween.EASE_IN)
	progress_value_tween.tween_property(progress_bar, "value", 0, 2).set_ease(Tween.EASE_OUT)
	
func handle_stop_charging():
	progress_value_tween.kill()
	print(progress_bar.value)
	progress_bar.value = 0
