extends Node
class_name HealthComponent

# control by host

#signal died
@export var object_type : Const.ObjectTypes = Const.ObjectTypes.Creep
# when init player, data has been store by host before
#@onready var stats_manager : StatsManager = get_tree().get_first_node_in_group("stats_manager")
@export var object_id : int = 1
var max_health : float
var hp_ratio : float
var current_health : float : 
	set(v):
		v = min(v, max_health)
		v = max(0, v)
		current_health = v
		if max_health <= 0:
			hp_ratio = 0
		else: hp_ratio = min(current_health / max_health, 1)

var coin = 0
signal health_changed(current_value, amount, hp_ratio) # for chilrend of owner node
signal died # for chilrend of owner node
signal respawn # for chilrend of owner node
var died_roi = false
# enemy stast
# own by host
func _ready():
	if !multiplayer.is_server():
		return
	
	# player is special, id is generated from multiplayer
	if object_type == Const.ObjectTypes.Player:
		object_id = $"../".name.to_int()
		GameEvents.player_level_up.connect(handle_player_level_up)
		GameEvents.player_respawn.connect(handle_player_respawn)
		
#	if stats_manager != null:
#		max_health = stats_manager.get_stats(object_id, object_type, Const.Stats.HP)
#		current_health = max_health
#	await get_tree().create_timer(1).timeout
	if owner.basic_data != null:
		max_health = (owner.basic_data as BasicStats).HP
		coin = (owner.basic_data as BasicStats).COIN
		current_health = max_health
		(owner.basic_data as BasicStats).changed.connect(handle_stats_change)

func handle_stats_change():
	max_health = (owner.basic_data as BasicStats).HP

# logic of hp: host control player, include hp
# hp player changed -> host emit signal hp_changed for that player to update his UI
# StatsManager
# call by host, control by host
func hp_change(damage_amount: float, killer_id : int = 0): # can be positive -> increase
	if !multiplayer.is_server() || died_roi:
		return
	current_health += damage_amount
	if object_type == Const.ObjectTypes.Player:
		GameEvents.emit_player_hp_change(object_id, current_health, hp_ratio, damage_amount)
		if damage_amount > 0:
			GameEvents.spawn_floating_text_str.rpc("+hp", owner.global_position + Vector2.UP*16, Color.FOREST_GREEN)
	# local logic for children
	health_changed.emit(current_health, damage_amount, hp_ratio)
	if current_health == 0:
		died_roi = true
		if object_type != Const.ObjectTypes.Player && object_type != Const.ObjectTypes.Statue && object_type != Const.ObjectTypes.Projectile:
			GameEvents.emit_player_killed_enemies(killer_id, object_id, coin)
		died.emit()
		# tmp
		if object_type != Const.ObjectTypes.Player && object_type != Const.ObjectTypes.Statue:
			owner.call_deferred("queue_free")
		if object_type == Const.ObjectTypes.Player:
			GameEvents.emit_player_died.rpc(object_id)

func handle_player_level_up(player_id, current_level):
	if player_id != object_id:
		return
	hp_change(max_health*0.1)

func handle_player_respawn(player_id):
	if player_id != object_id:
		return
	died_roi = false
	hp_change(max_health*0.5)
	print("handle_player_respawn")
	respawn.emit()
