@tool
class_name TrailPath2D
extends Path2D

signal finished

## Length of trail (percentage of the path)
@export_range(0.001, 1.0) var length := 0.2

## Time that the trail will finish travelling the path (in seconds)
@export_range(0.05, 10) var time := 1.0

## Minimum distance that will spawn a new point in the trail.
## [br]Should be lower when increase [member time]
@export var min_distance := 8.0:
	set(value):
		min_distance = value
		_min_dist = value**2

## negative value mean no loop (in seconds)
@export_range(0, 3600) var loop_delay := 1.0

## First delay before spawning
@export_range(0, 3600) var init_delay := 0.0

@export_group("Width")
## set with for the path
@export var width: Curve:
	set(value):
		width = value
		update_configuration_warnings()

## if true, width will be width of the trail
@export var width_is_local := false

@export_group("Color")
## set color for the path
@export var color: Gradient:
	set(value):
		color = value
		update_configuration_warnings()

## if true, color will be color of the trail
@export var color_is_local := false

## if set, trail will use this texture
@export var texture: Texture2D:
	set(value):
		texture = value
		if _line: _line.texture = value

var _ratio: Array[float] = []
var _time := 0.0
var _finish_elapsed := 0.0
var _time_visual_update := 0.0

@onready var _pf: PathFollow2D = $PathFollow2D
@onready var _line: Line2D = $Line2D
@onready var _min_dist := min_distance**2

func _ready() -> void:
	_line.texture = texture
	if color_is_local:
		_line.gradient = color

	if width_is_local:
		_line.width_curve = width
	else:
		_line.width_curve.min_value = width.min_value
		_line.width_curve.max_value = width.max_value

func _get_configuration_warnings() -> PackedStringArray:
	var err := []
	if width == null: err.append("Width value must be set")
	if color == null: err.append("Color value must be set")
	return err

func _process(delta: float) -> void:
	if Engine.is_editor_hint():
		return

	if init_delay > 0.0:
		init_delay -= delta
		return

	if _finish_elapsed > 0.0:
		_finish_elapsed -= delta
		if _finish_elapsed <= 0.0: _reset()
		return

	# trail pos should be global
	_line.global_position = Vector2.ZERO
	_line.global_rotation = 0
	
	var ratio := (1.0 + length)*_time/time
	_pf.progress_ratio = clamp(ratio, 0.0, 1.0)
	var pos := _pf.global_position
	if _ratio.size() == 0 or pos.distance_squared_to(_line.points[-1]) >= _min_dist:
		_ratio.append(_pf.progress_ratio)
		_line.add_point(pos)

	if _ratio[0] < ratio - length:
		_ratio.remove_at(0)
		_line.remove_point(0)

	if _ratio.size() >= 2 and _time_visual_update > 0.033:
		_update_visual()

	if _ratio.size() == 0:
		# Random delay 15%
		_finish_elapsed = randf_range(loop_delay*0.85, loop_delay*1.15)
		finished.emit()

	_time += delta
	_time_visual_update += delta

func _reset() -> void:
	_finish_elapsed = 0
	_time = 0

func _update_visual() -> void:
	_time_visual_update = 0;
	if !color_is_local:
		var p_size := _line.gradient.colors.size()
		var offset := (_ratio[-1] - _ratio[0]) / (p_size - 1)
		for i in p_size:
			_line.gradient.colors[i] = color.sample(_ratio[0] + offset*i)

	if !width_is_local:
		var p_size := _line.width_curve.point_count
		var offset := (_ratio[-1] - _ratio[0]) / (p_size - 1)
		for i in p_size:
			_line.width_curve.set_point_value(i, width.sample_baked(_ratio[0] + offset*i))
