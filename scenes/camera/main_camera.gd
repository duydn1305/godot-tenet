extends Camera2D

var target_node : Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	make_current()
	if has_node("Timer"): $Timer.timeout.connect(on_timer_timeout)

@rpc("any_peer", "call_local")
func update_camera_zoom(new_zoom):
	zoom = new_zoom

func on_timer_timeout():
	target_node = get_me()
#	print("get me: ", target_node)
	if target_node == null:
		target_node = get_first_player_alive()
#		print("get othre: ", target_node)	
	can_change_player = true
		

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if target_node == null:
		target_node = get_me()
		if target_node == null:
			target_node = get_first_player_alive()

	if target_node != null:
		global_position = global_position.lerp(target_node.global_position, 1 - exp(-delta * 10))


func get_first_player_alive():
	var list = get_tree().get_nodes_in_group("player_alive")
	if len(list) == 0:
		return null
	click_count = click_count % len(list)
	var player_node = list[click_count]
	return player_node
	
func get_me():
	if !multiplayer.has_multiplayer_peer():
		print("not has multiplayer peer")
		return get_tree().get_first_node_in_group("player_alive")
	var me = get_tree().get_nodes_in_group("player_alive").filter(func(player): return player.player_id == multiplayer.get_unique_id())
	if len(me) > 0:
		return me[0]
	return null
	
var click_count = 0
var is_died = false
var can_change_player = true
func _unhandled_input(event):
	if !is_died || !can_change_player:
		return
	# check if is death
	if event is InputEventKey:
		if event.pressed and event.keycode == KEY_LEFT:
			print("muliplayer left: ", multiplayer.get_unique_id())
			
			click_count -= 1
			if click_count < 0:
				click_count = 0
			can_change_player = false
			print("tab: ", click_count)
		if event.pressed and event.keycode == KEY_RIGHT:
			print("muliplayer right: ", multiplayer.get_unique_id())			
			click_count += 1
			can_change_player = false
			print("tab: ", click_count)
