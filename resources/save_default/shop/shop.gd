class_name SaveShop
extends Resource

const SAVE_PATH := "user://portable/shop.res"

@export var characters: SaveShopCharacters

func save(path="") -> void:
	ResourceSaver.save(self, SAVE_PATH if path.is_empty() else path)

static func load_and_create() -> SaveShop:
	if !FileAccess.file_exists(SAVE_PATH):
		var default := load("res://resources/save_default/data/shop.tres")
		ResourceSaver.save(default.duplicate(true), SAVE_PATH)
	return ResourceLoader.load(SAVE_PATH) as SaveShop
