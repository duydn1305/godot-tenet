class_name SaveShopCharacters
extends Resource

@export var data := {}

func get_value(key: String) -> bool:
	if !data.has(key): return false
	return data[key]

func set_value(key: String, new_value: bool) -> void:
	data[key] = new_value
