class_name SaveMetaProgression
extends Resource

const SAVE_PATH := "user://portable/meta_progression.res"

@export var _data := {}

func get_value(key: String) -> int:
	if !_data.has(key): return 0
	return _data[key]

func set_value(key: String, new_level: int) -> void:
	_data[key] = new_level

func get_keys() -> Array[String]:
	var res: Array[String] = []
	for key in _data.keys():
		if get_value(key) > 0: res.append(key)
	return res

func save(path="") -> void:
	ResourceSaver.save(self, SAVE_PATH if path.is_empty() else path)

static func load_and_create() -> SaveMetaProgression:
	if !FileAccess.file_exists(SAVE_PATH):
		var default := load("res://resources/save_default/data/meta_progression.tres")
		ResourceSaver.save(default.duplicate(true), SAVE_PATH)
	return ResourceLoader.load(SAVE_PATH) as SaveMetaProgression
