class_name SaveGeneral
extends Resource

const SAVE_PATH := "user://portable/general.res"

@export var player_name: String
@export var last_character_id: String
@export var coin: int

func save(path="") -> void:
	ResourceSaver.save(self, SAVE_PATH if path.is_empty() else path)

static func load_and_create() -> SaveGeneral:
	if !FileAccess.file_exists(SAVE_PATH):
		var default := load("res://resources/save_default/data/general.tres")
		ResourceSaver.save(default.duplicate(true), SAVE_PATH)
	return ResourceLoader.load(SAVE_PATH) as SaveGeneral
