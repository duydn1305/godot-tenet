extends Resource

class_name GameTilemap

@export var id : int
@export var env : Const.MapEnv = Const.MapEnv.JUNGLE
@export var scene : PackedScene
