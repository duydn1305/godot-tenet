class_name MetaProgRequirement
extends Resource

@export var coin: Array[int]
@export var achievement: Array[String]
@export var meta_progression: Array[String]
