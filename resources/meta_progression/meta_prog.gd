class_name MetaProgResource
extends Resource

@export_placeholder("Must be unique") var key: String
@export var name: String
@export var icon: Texture2D
@export var description: MetaProgDescription
@export var require: MetaProgRequirement
