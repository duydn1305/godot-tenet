extends Resource

class_name BasicStats

# common
@export var id : int
@export var object_name : String
@export var object_description : String
@export var stats_type : Const.StatsType
@export var scene : PackedScene
@export var icon : Texture2D
@export var thumbnail: Texture2D

# value
@export var HP : float = 105
@export var AMMOR : float = 0 # decresase 10% damage recieve
@export var MP : float = 100
@export var MP_REFILL : float = 5
@export var MP_REFILL_INTERVAL : float = 3
@export var DMG : float = 5
@export var COOLDOWN : float = 10
@export var ACTIVATE_TIME : float = 2
@export var AREA : float = 10
@export var CRIT_DMG : float = 0.5
@export var CRIT_RATE : float = 1.05
@export var MP_COST : float = 10
@export var EXP_MULTIPLIER : float = 1
@export var SPEED_MULTIPLIER : float = 1
@export var FROZEN_RATE : float = 0.05
@export var STUN_RATE : float  = 0.05
@export var SET_FIRE_RATE : float = 0.05
@export var SLOW_FIRE_RATE : float = 0.05
@export var PENETRATE_AMMOR_RATE : float = 0.05
@export var ATTACK_SPEED : float = 1.5
@export var BULLET_SPEED : float = 50
@export var BULLET_PER_SHOT : int = 1
@export var BULLET_DECAY : float = 1
@export var BULLET_PENETRATION : int = 1
@export var SHOOT_ANGLE : float
@export var EVADE : float = 0.01
@export var MOVE_SPEED : float = 50
@export var ACCELEBRATION : float = 15
@export var HP_RECEIVED_MULTILPIER : float = 0

@export var EXP : float = 5
@export var COIN : float = 5
@export var COIN_MULTIPLIER : float = 1
@export var PRICE : float = 1000
@export var CAMERA_ZOOM_OUT : float = 0

@export var EXTRA_DMG_TO_JUNGLE_ENEMIES : float = 0
@export var REDUCE_DMG_FROM_JUNGLE_ENEMIES : float = 0


@export var EXTRA_DMG_TO_DESERT_ENEMIES : float = 0
@export var REDUCE_DMG_FROM_DESERT_ENEMIES : float = 0


@export var EXTRA_DMG_TO_LAVA_ENEMIES : float = 0
@export var REDUCE_DMG_FROM_LAVA_ENEMIES : float = 0


# skill
@export var skill_q : BasicStats
@export var skill_e : BasicStats
@export var skill_r : BasicStats
@export var passive : Array[BasicStats]
@export var level_up : BasicStats

func to_dict() -> Dictionary:
	var dict = {
		'id' :id,
		'object_name' :object_name,
		'object_description': object_description,
		'stats_type' :stats_type,
		'stats_by_enum': {
			 Const.Stats.HP: HP,
			 Const.Stats.AMMOR: AMMOR, # decresase 10% damage recieve
			 Const.Stats.MP: MP,
			 Const.Stats.MP_REFILL: MP_REFILL,
			 Const.Stats.MP_REFILL_INTERVAL: MP_REFILL_INTERVAL,
			 Const.Stats.DMG: DMG,
			 Const.Stats.COOLDOWN: COOLDOWN,
			 Const.Stats.ACTIVATE_TIME: ACTIVATE_TIME,
			 Const.Stats.AREA: AREA,
			 Const.Stats.CRIT_DMG: CRIT_DMG,
			 Const.Stats.CRIT_RATE: CRIT_RATE,
			 Const.Stats.MP_COST: MP_COST,
			 Const.Stats.EXP_MULTIPLIER: EXP_MULTIPLIER,
			 Const.Stats.SPEED_MULTIPLIER: SPEED_MULTIPLIER,
			 Const.Stats.FROZEN_RATE: FROZEN_RATE,
			 Const.Stats.STUN_RATE: STUN_RATE,
			 Const.Stats.SET_FIRE_RATE: SET_FIRE_RATE,
			 Const.Stats.SLOW_FIRE_RATE: SLOW_FIRE_RATE,
			 Const.Stats.PENETRATE_AMMOR_RATE: PENETRATE_AMMOR_RATE,
			 Const.Stats.ATTACK_SPEED: ATTACK_SPEED,
			 Const.Stats.BULLET_SPEED: BULLET_SPEED,
			 Const.Stats.BULLET_PER_SHOT: BULLET_PER_SHOT,
			 Const.Stats.BULLET_DECAY: BULLET_DECAY,
			 Const.Stats.BULLET_PENETRATION: BULLET_PENETRATION,
			 Const.Stats.EVADE: EVADE,
			 Const.Stats.MOVE_SPEED: MOVE_SPEED,
			 Const.Stats.ACCELEBRATION: ACCELEBRATION,
			 Const.Stats.EXP: EXP,
			 Const.Stats.COIN: COIN,
		}
	}
	if skill_q != null: dict["skill_q"] = skill_q.to_dict()
	if skill_e != null: dict["skill_e"] = skill_e.to_dict()
	if skill_r != null: dict["skill_r"] = skill_r.to_dict()
	if level_up != null: dict["level_up"] = level_up.to_dict()
	if len(passive) > 0:
		var arr : Array[BasicStats]
		for p in passive:
			arr.append(p.to_dict())
		dict["passive"] = arr
	return dict

#@export var env : Const.MapEnv = Const.MapEnv.JUNGLE
#func get_stats(stats : Const.Stats):
#	match stats:
#		Const.Stats.HP: return HP
#		Const.Stats.AMMOR: return AMMOR 
#		Const.Stats.MP: return MP 
#		Const.Stats.DMG: return DMG 
#		Const.Stats.COOLDOWN: return COOLDOWN 
#		Const.Stats.ACTIVATE_TIME: return ACTIVATE_TIME 
#		Const.Stats.AREA: return AREA 
#		Const.Stats.CRIT_DMG: return CRIT_DMG 
#		Const.Stats.CRIT_RATE: return CRIT_RATE 
#		Const.Stats.MP_COST: return MP_COST 
#		Const.Stats.EXP_MULTIPLIER: return EXP_MULTIPLIER 
#		Const.Stats.SPEED_MULTIPLIER: return SPEED_MULTIPLIER 
#		Const.Stats.FROZEN_RATE: return FROZEN_RATE 
#		Const.Stats.STUN_RATE: return STUN_RATE 
#		Const.Stats.SET_FIRE_RATE: return SET_FIRE_RATE 
#		Const.Stats.SLOW_FIRE_RATE: return SLOW_FIRE_RATE 
#		Const.Stats.PENETRATE_AMMOR_RATE: return PENETRATE_AMMOR_RATE 
#		Const.Stats.ATTACK_SPEED: return ATTACK_SPEED
#		Const.Stats.BULLET_SPEED: return BULLET_SPEED
#		Const.Stats.BULLET_PER_SHOT: return BULLET_PER_SHOT
#		Const.Stats.EVADE: return EVADE
#		Const.Stats.MOVE_SPEED: return MOVE_SPEED
#		Const.Stats.ACCELEBRATION: return ACCELEBRATION
#		_: return 0
			
