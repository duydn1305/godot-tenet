class_name Achievement
extends Resource

@export_placeholder("Must be unique") var key: String
@export var title: String
@export_multiline var description: String
@export var total_require: int
@export var display_number: bool
